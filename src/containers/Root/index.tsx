import React from 'react';
import { SafeAreaView } from 'react-native';
import { Store } from 'redux';
import { Provider } from 'react-redux';
import { ThemeContext } from 'react-native-material-ui';
import { I18nextProvider } from 'react-i18next';

import { AppState } from 'src/types/redux';
import { COLORS } from 'src/constants/colors';
import { setStore } from 'src/redux/store';
import { MuiTheme } from 'src/mui-theme';
import { Navigation } from 'src/navigation';
import { i18next } from 'src/i18n';

const store: Store<AppState> = setStore();

export const RootContainer: React.SFC = () => (
  <Provider store={store}>
    <I18nextProvider i18n={i18next}>
      <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.navy }}>
        <ThemeContext.Provider value={MuiTheme}>
          <Navigation />
        </ThemeContext.Provider>
      </SafeAreaView>
    </I18nextProvider>
  </Provider>
);
