export const Routes = {
  // Non-logged in routes
  Splash: 'Splash',
  Auth: 'AuthStackNavigator',
  Welcome: 'Welcome',
  HowItWorks: 'HowItWorks',
  Login: 'Login',
  Register: 'Register',
  ForgotPassword: 'ForgotPassword',
  // Logged in routes
  Drawer: 'MainDrawerNavigator',
  Application: 'TabsAndDrawer',
  Tabs: 'MainTabsNavigator',
  HomeTab: 'HomeTab',
  HomeTabIndex: 'HomeTabIndex',
  WalletTab: 'WalletTab',
  WalletTabIndex: 'WalletTabIndex',
  ScanTab: 'ScanTab',
  ScanTabIndex: 'ScanTabIndex',
  RewardsTab: 'RewardsTab',
  RewardsTabIndex: 'RewardsTabIndex',
  NetworkTab: 'NetworkTab',
  NetworkTabIndex: 'NetworkTabIndex',
  ProductForConsumer: 'ProductForConsumer',
  ProductAfterScan: 'ProductAfterScan',
  ProductAfterScanUnAuth: 'ProductAfterScanUnAuth',
  ScanUnAuth: 'ScanUnAuth',
  ProfileSettings: 'ProfileSettings',
};
