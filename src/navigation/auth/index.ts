import { createStackNavigator } from 'react-navigation';

import { WelcomeScreen } from 'src/screens/Welcome';
import { LoginScreen } from 'src/screens/Login';
import { HowItWorksScreen } from 'src/screens/HowItWorks';
import { RegisterScreen } from 'src/screens/Register';
import { ForgotPasswordScreen } from 'src/screens/ForgotPassword';
import { Product } from 'src/screens/Product';
import { ScanTabScreen } from 'src/screens/ScanTab';
import { Routes } from '../route-names';

export const AuthNavigation = createStackNavigator(
  {
    [Routes.Welcome]: WelcomeScreen,
    [Routes.Login]: LoginScreen,
    [Routes.HowItWorks]: HowItWorksScreen,
    [Routes.Register]: RegisterScreen,
    [Routes.ForgotPassword]: ForgotPasswordScreen,
    [Routes.ProductAfterScanUnAuth]: Product,
    [Routes.ScanUnAuth]: ScanTabScreen,
  },
  {
    initialRouteName: Routes.Welcome,
    navigationOptions: { header: null },
  },
);
