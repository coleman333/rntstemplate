import React from 'react';
import { Linking } from 'react-native';
import { Store } from 'redux';
import { PublicScanParams } from 'src/models/entities/ScanParams';
import { NavigationContainerComponent } from 'react-navigation';
import { getStore } from 'src/redux/store';
import { Navigation as NavigationService } from 'src/services/Navigation';
import { Routes } from 'src/navigation/route-names';
import { AUTH_SET_INITIAL, AUTH_SET_DEEP_LINK } from 'src/constants/ActionTypes/Auth';
import NativeSplashScreen from 'react-native-splash-screen';

import { RootNavigation } from './root';

export class Navigation extends React.PureComponent {

  public static store: Store<any>;
  public static authorized: boolean = false;
  // public static scanParams?: PublicScanParams;
  public static outsideURL?: string = "";

  constructor(props: any) {
    super(props);
    Navigation.store = getStore();
    Navigation.store.subscribe(() => {
      let state = Navigation.store.getState();
      // Navigation.scanParams = state.auth.scan_parameters;
      // Navigation.authorized = state.auth.authorized;
      // "state.auth.initial" means that "login" / "logout" happened
      // if (state.auth.initial) {                                           // if authenticated
      //   Navigation.store.dispatch({type: AUTH_SET_INITIAL, payload: {initial: false}});    //
      //   if ( Navigation.outsideURL ) {
      //     Navigation.store.dispatch({type: AUTH_SET_DEEP_LINK, payload: {deeplink: Navigation.outsideURL}});
      //   }
      //   if (Navigation.authorized) {
      //     NativeSplashScreen.hide();
      //     if (Navigation.outsideURL.length) {
            NavigationService.navigateTo({routeName: Routes.ScanTab});
          // } else {
          //   NavigationService.navigateTo({routeName: Routes.HomeTab, params: { initial: true }});
          // }
        // } else {
        //   if (Navigation.outsideURL.length) {
        //     NavigationService.navigateTo({routeName: Routes.ScanUnAuth});
        //     NativeSplashScreen.hide();
          // } else {
          //   NavigationService.navigateTo({ routeName: Routes.Welcome, params: { animate: true } });
          // }
        // }
        // Navigation.outsideURL = "";
      // }
    });

  }

  private initializeNavigationService = (reference: NavigationContainerComponent): void => {
    NavigationService.init(reference);
  };

  // Linking

  private appWokeUp = (event: any): void => {
    // this handles the use case where the app is running in the background
    // and is activated by the listener...
    this.handleOutsideNavigation(event.url);
  };

  private handleOutsideNavigation = (url: string): void => {
    // Do whatever you need to do within your app to redirect users to the proper route
    Navigation.outsideURL = url;
  };

  public componentDidMount() {
    // this handles the case where the app is closed
    // and is launched via Universal Linking.

    Linking.getInitialURL()
      .then((url: string) => {
        if (url) {
          this.handleOutsideNavigation(url);
        }
      })
      .catch((e) => {
        console.log(e);
      });

    // this listener handles the case where the app is woken up
    // from the Universal or Deep Linking

    Linking.addEventListener('url', this.appWokeUp);
  }

  public componentWillUnmount(){
    Linking.removeEventListener('url', this.appWokeUp);
  }

  // RENDER

  public render(): React.ReactNode {
    return <RootNavigation ref={this.initializeNavigationService} />;
  }
}
