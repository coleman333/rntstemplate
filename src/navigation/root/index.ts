import { createSwitchNavigator } from 'react-navigation';

import { SplashScreen } from 'src/screens/Splash';
import { Routes } from '../route-names';
import { AuthNavigation } from '../auth';
import { ApplicationNavigation } from '../application';

export const RootNavigation = createSwitchNavigator(
  {
    [Routes.Splash]: SplashScreen,
    [Routes.Auth]: AuthNavigation,
    [Routes.Application]: ApplicationNavigation,
  },
  {
    initialRouteName: Routes.Splash,
  },
);
