import {StyleSheet} from "react-native";
import {COLORS} from "../../../constants";

export const styles = StyleSheet.create({

  image: {
    height: 30,
    marginTop: 2,
    marginBottom: 7,
  },

});