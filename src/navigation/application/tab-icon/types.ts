export interface Props {
  tab: 'home' | 'wallet' | 'scan' | 'rewards' | 'network' | 'logout';
  focused: boolean;
}
