import React from 'react';
import { createStackNavigator } from 'react-navigation';

import { Routes } from 'src/navigation/route-names';
import { WalletTabScreen } from 'src/screens/WalletTab';
import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const WalletTab = {
  screen: createStackNavigator(
    {
      [Routes.WalletTabIndex]: WalletTabScreen,
      // other screens inside home tab
    },
    {
      initialRouteName: Routes.WalletTabIndex,
      navigationOptions: {
        header: <Header theme="dark" align="right" />,
      },
    },
  ),
  navigationOptions: {
    tabBarLabel: 'Wallet',
    tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="wallet" focused = {focused} />,
  },
};
