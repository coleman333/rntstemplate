import React from 'react';
import { createStackNavigator } from 'react-navigation';

import { Routes } from 'src/navigation/route-names';
import { RewardsTabScreen } from 'src/screens/RewardsTab';
import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const RewardsTab = {
  screen: createStackNavigator(
    {
      [Routes.RewardsTabIndex]: RewardsTabScreen,
      // other screens inside home tab
    },
    {
      initialRouteName: Routes.RewardsTabIndex,
      navigationOptions: {
        header: <Header theme="light" align="right" />,
      },
    },
  ),
  navigationOptions: {
    tabBarLabel: 'Rewards',
    tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="rewards" focused = {focused} />,
  },
};
