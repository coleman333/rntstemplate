import React from 'react';
import { createStackNavigator } from 'react-navigation';

import { Routes } from 'src/navigation/route-names';
import { ScanTabScreen } from 'src/screens/ScanTab';
import { TabIcon } from '../tab-icon';
import {Header} from "../header";

export const ScanTab = {
  screen: createStackNavigator(
    {
      [Routes.ScanTabIndex]: ScanTabScreen,
    },
    {
      initialRouteName: Routes.ScanTabIndex,
      navigationOptions: {
        header: <Header theme="light" align="right" />,
      },
    },
  ),
  navigationOptions: {
    tabBarLabel: 'Scan',
    tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="scan" focused = {focused} />,
  },
};