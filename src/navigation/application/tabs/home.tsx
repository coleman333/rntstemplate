import React from 'react';
import { createStackNavigator } from 'react-navigation';

import { Routes } from 'src/navigation/route-names';
import { HomeTabScreen } from 'src/screens/HomeTab';
import { Product } from 'src/screens/Product';
import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const HomeTab = {
  screen: createStackNavigator(
    {
      [Routes.HomeTabIndex]: HomeTabScreen,
      [Routes.ProductForConsumer]: Product,
      // other screens inside home tab
    },
    {
      initialRouteName: Routes.HomeTabIndex,
      navigationOptions: {
        header: <Header theme="light" align="right" />,
      },
    },
  ),
  navigationOptions: {
    tabBarLabel: 'Home',
    tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="home" focused = {focused} />,
  },
};
