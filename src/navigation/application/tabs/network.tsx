import React from 'react';
import { createStackNavigator } from 'react-navigation';

import { Routes } from 'src/navigation/route-names';
import { NetworkTabScreen } from 'src/screens/NetworkTab';
import { TabIcon } from '../tab-icon';
import { Header } from "../header";
import { ProfileSettingsScreen } from "../../../screens/ProfileSettings";

export const NetworkTab = {
  screen: createStackNavigator(
    {
      [Routes.NetworkTabIndex]: NetworkTabScreen,
      [Routes.ProfileSettings]: ProfileSettingsScreen,
      // other screens inside home tab
    },
    {
      initialRouteName: Routes.NetworkTabIndex,
      navigationOptions: {
        header: <Header theme="light" align="right" />,
      },
    },
  ),
  navigationOptions: {
    tabBarLabel: 'Profile',
    tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="network" focused = {focused} />,
  },
};
