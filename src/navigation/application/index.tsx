import { Dimensions } from "react-native";
import {createMaterialTopTabNavigator, createDrawerNavigator, createStackNavigator} from 'react-navigation';

import { COLORS } from 'src/constants';
import { Routes } from '../route-names';
import { HomeTab } from './tabs/home';
import { WalletTab } from './tabs/wallet';
import { ScanTab } from './tabs/scan';
import { RewardsTab } from './tabs/rewards';
import { NetworkTab } from './tabs/network';
import { Menu } from 'src/components/drawer-menu';
import { Product } from 'src/screens/Product';
import React from "react";

const {width} = Dimensions.get('screen');
const DRAWER_WIDTH = width - (width / 5);

export const ApplicationTabs = createMaterialTopTabNavigator(
  {
    [Routes.HomeTab]: HomeTab,
    [Routes.WalletTab]: WalletTab,
    [Routes.ScanTab]: ScanTab,
    [Routes.RewardsTab]: RewardsTab,
    [Routes.NetworkTab]: NetworkTab,
  },
  {
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    animationEnabled: true,
    lazy: true,
    initialRouteName: Routes.HomeTab,
    tabBarOptions: {
      activeTintColor: COLORS.mauve,
      inactiveTintColor: COLORS.inactive,
      showIcon: true,
      showLabel: true,
      upperCaseLabel: false,
      pressColor: 'transparent',
      pressOpacity: 0,
      // scrollEnabled
      // tabStyle
      indicatorStyle: {
        backgroundColor: 'transparent',
      },
      labelStyle: {
        fontSize: 14,
        //color: COLORS.mauve,
        margin: 0,
      },
      iconStyle: {
        margin: 0,
      },
      style: {
        backgroundColor: COLORS.navy,
      },
      allowFontScaling: false,
    },
  },
);

export const ApplicationDrawer = createDrawerNavigator(
  {
    [Routes.Tabs]: ApplicationTabs,
    [Routes.ProductAfterScan]: Product,

  },
  {
    drawerPosition: 'left',
    drawerWidth: DRAWER_WIDTH,
    contentComponent: ({ navigation }) => <Menu navigation = {navigation} />,
  }
);

export const ApplicationNavigation = createStackNavigator(
  {
    [Routes.Drawer]: ApplicationDrawer,
  }, {
    navigationOptions: {
      header: null,
    },
  }
);

