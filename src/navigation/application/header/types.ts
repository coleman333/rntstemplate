export interface Props {
  theme: 'light' | 'dark' | 'product';
  align?: 'center' | 'right' | 'left';
}
