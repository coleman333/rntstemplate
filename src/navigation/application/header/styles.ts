import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: '3.125%',
  },

  left: { justifyContent: 'flex-start' },
  center: { justifyContent: 'center' },
  right: { justifyContent: 'flex-end' },

  light: {
    backgroundColor: COLORS.white,
    color: COLORS.navy,
  },

  dark: {
    backgroundColor: COLORS.navy,
    color: COLORS.white,
  },

  product: {
    backgroundColor: COLORS.navy,
    color: COLORS.white,
  },
});
