import React from 'react';
import { Image, View, TouchableHighlight } from 'react-native';

import { Props } from './types';
import { hamburger } from './variables';
import { styles } from './styles';
import { State } from "src/components/dark-top/types";
import { Navigation } from 'src/services/Navigation';

export class Header extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    theme: 'dark',
    align: 'center',
  };

  private onPress = () : void => {
    Navigation.toggleDrawer();
  };

  public render(): React.ReactNode {
    const { theme, align } = this.props;
    return (
      <View style={[styles.container, styles[theme], styles[align]]}>
        <TouchableHighlight
          underlayColor={'transparent'}
          onPress = {this.onPress}
        >
          <Image source={hamburger[theme]} />
        </TouchableHighlight>
      </View>
    );
  }

}

Header.defaultProps = {
  align: 'center',
};
