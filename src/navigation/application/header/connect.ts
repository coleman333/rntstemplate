import { bindActionCreators, Dispatch } from 'redux';

import { reduxify } from 'src/utils/reduxify';
import { logout } from "src/actions/Auth";

const mapStateToProps = (state: any) => ({
  auth: state.auth,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    logout:     bindActionCreators(logout, dispatch),
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
