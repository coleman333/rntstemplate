import {
  PAGE_PRODUCT_DETAILS_ADD_REVIEW_REQUEST,
  PAGE_PRODUCT_DETAILS_GET_PRODUCT_REQUEST,
  PAGE_PRODUCT_DETAILS_SCAN_REQUEST,
  PAGE_PRODUCT_DETAILS_RESET_PRODUCT_REQUEST,
  COPY_PRODUCT_DETAILS,
  GET_PRODUCT_BY_PHOTO_REQUEST,
} from 'src/constants/ActionTypes/Products';
import { IReduxState } from 'src/types/redux';
import {Product, ProductForConsumer} from 'src/models/entities/Product';
import { SET_REVIEW } from '../../../constants/ActionTypes/Rating';

export interface IProductDetailsState extends IReduxState {
  product?: Product | ProductForConsumer;
  matches?: Array<ProductForConsumer>;
  textIdentifier?: string;
}

export const initialState: IProductDetailsState = {
  pending: false,
};

export const product = function(
  state: IProductDetailsState = initialState,
  action: any,
): IProductDetailsState {
  switch (action.type) {
    case COPY_PRODUCT_DETAILS:
      return {
        ...state,
        product: {
          ...action.payload,
        },
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${GET_PRODUCT_BY_PHOTO_REQUEST}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
        product: null,
      };
    case `${GET_PRODUCT_BY_PHOTO_REQUEST}_REJECTED`:
      return {
        ...state,
        pending: false,
        fulfilled: false,
        rejected: true,
        product: null,
      };
    case `${GET_PRODUCT_BY_PHOTO_REQUEST}_FULFILLED`:
      return {
        ...state,
        pending: false,
        fulfilled: true,
        rejected: false,
        product: {
          ...action.payload.product,
        },
        matches: [
          ...action.payload.matches,
        ],
      };
    case `${PAGE_PRODUCT_DETAILS_ADD_REVIEW_REQUEST}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${PAGE_PRODUCT_DETAILS_ADD_REVIEW_REQUEST}_FULFILLED`:
      return {
        ...state,
        product: {
          ...state.product,
          ...action.payload,
        },
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${PAGE_PRODUCT_DETAILS_ADD_REVIEW_REQUEST}_REJECTED`:
      return {
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    case `${PAGE_PRODUCT_DETAILS_GET_PRODUCT_REQUEST}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${PAGE_PRODUCT_DETAILS_GET_PRODUCT_REQUEST}_FULFILLED`:
      return {
        ...state,
        product: {
          ...action.payload,
        },
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${PAGE_PRODUCT_DETAILS_GET_PRODUCT_REQUEST}_REJECTED`:
      return {
        ...state,
        product: null,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    case `${PAGE_PRODUCT_DETAILS_SCAN_REQUEST}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${PAGE_PRODUCT_DETAILS_SCAN_REQUEST}_FULFILLED`:
      return {
        ...state,
        product: {
          ...action.payload.product,
          user_reviews: action.payload.user_reviews,
        },
        textIdentifier: action.payload.text_identifier,
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${PAGE_PRODUCT_DETAILS_SCAN_REQUEST}_REJECTED`:
      return {
        ...state,
        product: null,
        textIdentifier: null,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    case `${PAGE_PRODUCT_DETAILS_RESET_PRODUCT_REQUEST}_FULFILLED`:
      return {
        ...state,
        product: null,
        textIdentifier: null,
        pending: false,
        fulfilled: false,
        rejected: false,
      };

    case `${SET_REVIEW}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${SET_REVIEW}_FULFILLED`:
      console.log(`${SET_REVIEW}_FULFILLED`, action.payload)
      if(action.payload.addReview){
        return {
          ...state,
          product: {
            ...state.product,
            user_rating: action.payload.addReview.reviewee_rating.rating,
            user_review:
              {
                id: action.payload.addReview.id,
                reviewee: action.payload.addReview.reviewee,
                reviewer: action.payload.addReview.reviewer,
                numerical_rating: action.payload.addReview.numerical_rating,
                review_date: action.payload.addReview.review_date,
                description: action.payload.addReview.description,
              },
          },
          pending: false,
          fulfilled: true,
          rejected: false,
        };
      }
      else{
        return {
          ...state,
          product: {
            ...state.product,
            user_rating: action.payload.updateReview.reviewee_rating.rating,
            user_review:
              {
                id: action.payload.updateReview.id,
                reviewee: action.payload.updateReview.reviewee,
                reviewer: action.payload.updateReview.reviewer,
                numerical_rating: action.payload.updateReview.numerical_rating,
                review_date: action.payload.updateReview.review_date,
                description: action.payload.updateReview.description,
              },
          },
          pending: false,
          fulfilled: true,
          rejected: false,
        };
      }


    case `${SET_REVIEW}_REJECTED`:
      return {
        ...state,
        product: null,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };

    default:
      return state;
  }
};
