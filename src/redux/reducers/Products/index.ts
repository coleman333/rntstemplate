import { product } from './details';
import { products } from './list';

export { product, products };