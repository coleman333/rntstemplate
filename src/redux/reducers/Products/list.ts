import { GET_PRODUCTS_REQUEST } from 'src/constants/ActionTypes/Products';
import { IReduxState } from 'src/types/redux';
import { Product } from 'src/models/entities/Product';
import { PublicScanParams } from 'src/models/entities/ScanParams';

export interface IProductsDetailState extends IReduxState {
  products?: any;
}

export interface ScanResponse {
  product: Product;
  scan_parameters: PublicScanParams;
}

export const initialState: IProductsDetailState = {
  pending: false,
};

export const products = (
  state: IProductsDetailState = initialState,
  action: any,
): IProductsDetailState => {
  switch (action.type) {
    case `${GET_PRODUCTS_REQUEST}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${GET_PRODUCTS_REQUEST}_FULFILLED`:
      return {
        ...state,
        ...action.payload,
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${GET_PRODUCTS_REQUEST}_REJECTED`:
      return {
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    default:
      return state;
  }
};
