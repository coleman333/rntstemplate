import { PAGE_REWARDS_GET_DATA } from 'src/constants/ActionTypes/Rewards';
import { IReduxState } from 'src/types/redux';
import { UserCoupon } from "src/models/entities/Coupon";

export interface IRewardsState extends IReduxState {
  pending?: boolean;
  coupons?: Array<UserCoupon>
}

export const initialState: IRewardsState = {
  pending: false,
};

export const rewardsReducer = function(
  state: IRewardsState = initialState,
  action: any,
): IRewardsState {
  switch (action.type) {
    case `${PAGE_REWARDS_GET_DATA}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${PAGE_REWARDS_GET_DATA}_FULFILLED`:
      return {
        ...state,
        ...action.payload,
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${PAGE_REWARDS_GET_DATA}_REJECTED`:
      return {
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    default:
      return state;
  }
};
