import {
  GET_BALANCE
} from 'src/constants/ActionTypes/Wallet';
import {IReduxState} from 'src/types/redux';
import { IWallet } from 'src/interfaces/Wallet';

export interface IWalletState extends IReduxState, IWallet {}

export const initialState: IWalletState = {
  initial: true,
  pending: false,
  fulfilled: false,
  rejected: false,
  conversationRate: 0,
  balanceOMZ: 0,
  balanceCurrency: 0
};

export const wallet = (state: IWalletState = initialState, action: any): IWalletState => {
  switch (action.type) {
    case `${GET_BALANCE}`:
      return {
        ...state,
       ...action.payload,
      };

    case `${GET_BALANCE}_PENDING`:
      return {
        ...state,
        initial: false,
        pending: true,
        fulfilled: false,
        rejected: false
      };
    case `${GET_BALANCE}_FULFILLED`:
      return {
        ...state,
        initial: false,
        pending: false,
        fulfilled: true,
        rejected: false,
        ...action.payload
      };

    case `${GET_BALANCE}_REJECTED`:
      return {
        ...state,
        initial: false,
        pending: false,
        fulfilled: false,
        rejected: true
      };

    default:
      return {...state};
  }
};
