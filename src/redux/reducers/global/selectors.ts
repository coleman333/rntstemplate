import { IAuthState } from 'src/redux/reducers/Auth';

export const auth = (state: IAuthState): IAuthState => state;
