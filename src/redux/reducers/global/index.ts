import { createReducer } from 'src/utils/createReducer';
import { ActionTypes } from './action-types';
import { State } from './types';

const initialState: State = {
  authorized: false,
};

export const globalReducer = createReducer(initialState)({
  [ActionTypes.MARK_USER__AUTHORIZED]: (state: State): State => ({ ...state, authorized: true }),
});
