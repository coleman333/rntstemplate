import { Action, ActionCreator } from 'redux';
import { ActionTypes } from './action-types';

export const markUserAsAuthorized: ActionCreator<Action> = () => ({
  type: ActionTypes.MARK_USER__AUTHORIZED,
});
