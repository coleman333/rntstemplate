import { SCAN_HISTORY_GET } from 'src/constants/ActionTypes/Scan';
import { IReduxState } from 'src/types/redux';
import { UserCoupon } from "src/models/entities/Coupon";
import { ProductForConsumer } from "src/models/entities/Product";

export interface IHistoryState extends IReduxState {
  pending?: boolean;
  coupons?: Array<UserCoupon>
  products?: Array<ProductForConsumer>
}

export const initialState: IHistoryState = {
  pending: false,
};

export const profileReducer = function(state: IHistoryState = initialState, action: any): IHistoryState {
  switch (action.type) {
    case `${SCAN_HISTORY_GET}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${SCAN_HISTORY_GET}_FULFILLED`:
      return {
        ...state,
        ...action.payload,
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${SCAN_HISTORY_GET}_REJECTED`:
      return {
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    default:
      return state;
  }
};
