### Reducer folder structure scheme

- `index.ts` contains the reducer itself with it's initial state
- `types.ts` contains all reducer related types
- `action-types.ts` contains all reducer action types constants
- `actions.ts` contains all action creators related to the reducer
- `selectors.ts` _[optional]_ contains all selectors related to the reducer
- `variables.ts` _[optional]_ contains different variables related to the reducer
- `helpers.ts` _[optional]_ contains different helper functions related to the reducer

```text
┗ reducers
  ┣ global
  ┣ request-errors
  ┣ request-statuses
  ┣ {other_reducers}
  ┗ {your_reducer_name}
    ┣ action-types.ts
    ┣ actions.ts
    ┣ index.ts
    ┣ selectors.ts
    ┣ types.ts
    ┣ variables.ts
    ┗ helpers.ts
```
