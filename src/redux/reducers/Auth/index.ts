import {
  USER_PROFILE_GET_PROFILE_REQUEST,
  USER_PROFILE_UPDATE_PROFILE_REQUEST,
  LOGOUT_REQUEST,
  DELETE_REQUEST,
  SOCIAL_LOGIN_REQUEST,
  GET_SOCIAL_IDS_REQUEST,
  USER_PROFILE_GET_INITIAL_REQUEST,
  AUTH_SET_INITIAL,
  AUTH_SET_DEEP_LINK,
} from 'src/constants/ActionTypes/Auth';
import {IReduxState} from 'src/types/redux';

export interface IAuthState extends IReduxState {
  socialIds?: {
    socialGoogleId: string;
    socialFacebookId: string;
  };
  authToken?: boolean;
  isUserProfileFulfilled: boolean;
  isSocialIdFulfilled: boolean;
  userProfile?: {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    birth_date: number;
    country: string;
    image_id: string;
    tokens_amount: number;
    organization_id: number;
    age_range_from: number;
    age_range_to: number;
    gender: Object;
    location: Object;
  };
  deeplink: string;
}

export const initialState: IAuthState = {
  authorized: false,
  initial: false,
  pending: false,
  isUserProfileFulfilled: false,
  authToken: false,
  isSocialIdFulfilled: false,
  deeplink: '',
};

export const auth = (state: IAuthState = initialState, action: any): IAuthState => {
  switch (action.type) {
    case `${AUTH_SET_INITIAL}`:
      return {
        ...state,
        initial: action.payload.initial,
      };
    case `${AUTH_SET_DEEP_LINK}`:
      return {
        ...state,
        deeplink: action.payload.deeplink,
      };
    case `${SOCIAL_LOGIN_REQUEST}_PENDING`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        authorized: false,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${SOCIAL_LOGIN_REQUEST}_FULFILLED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        authorized: true,
        authToken: action.payload.auth_token,
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${SOCIAL_LOGIN_REQUEST}_REJECTED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        authorized: false,
        error: action.error,
        pending: false,
        authToken: false,
        fulfilled: false,
        rejected: true,
      };
    case `${USER_PROFILE_GET_INITIAL_REQUEST}_PENDING`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
        isUserProfileFulfilled: false,
      };
    case `${USER_PROFILE_GET_INITIAL_REQUEST}_FULFILLED`:
      return {
        // TODO: don't do that, use immer!
        ...state,

        // two following options should reflected by authorization subscription
        initial:    true,
        authorized: typeof action.payload.email === 'undefined' ? false : true,

        userProfile: {
          ...action.payload,
        },
        pending: false,
        fulfilled: true,
        rejected: false,
        isUserProfileFulfilled: true,
      };
    case `${USER_PROFILE_GET_INITIAL_REQUEST}_REJECTED`:
      return {
        // TODO: don't do that, use immer!
        ...state,

        // two following options should reflected by authorization subscription
        initial: true,
        authorized: false,

        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
        isUserProfileFulfilled: true,
      };
    case `${USER_PROFILE_GET_PROFILE_REQUEST}_PENDING`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        initial: false,
        pending: true,
        fulfilled: false,
        rejected: false,
        isUserProfileFulfilled: false,
      };
    case `${USER_PROFILE_GET_PROFILE_REQUEST}_FULFILLED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        // TODO: set expected type of received data using "as"
        userProfile: {
          ...action.payload,
        },
        initial: false,
        pending: false,
        fulfilled: true,
        rejected: false,
        isUserProfileFulfilled: true,
      };
    case `${USER_PROFILE_GET_PROFILE_REQUEST}_REJECTED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
        isUserProfileFulfilled: true,
      };
    case `${GET_SOCIAL_IDS_REQUEST}_PENDING`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
        isSocialIdFulfilled: false,
      };
    case `${GET_SOCIAL_IDS_REQUEST}_FULFILLED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        // TODO: set expected type of received data using "as"
        socialIds: {
          ...action.payload,
        },
        pending: false,
        fulfilled: true,
        rejected: false,
        isSocialIdFulfilled: true,
      };
    case `${GET_SOCIAL_IDS_REQUEST}_REJECTED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
        isSocialIdFulfilled: true,
      };
    case `${USER_PROFILE_UPDATE_PROFILE_REQUEST}_PENDING`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${USER_PROFILE_UPDATE_PROFILE_REQUEST}_FULFILLED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        // TODO: set expected type of received data using "as"
        ...action.payload,
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${USER_PROFILE_UPDATE_PROFILE_REQUEST}_REJECTED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    case `${LOGOUT_REQUEST}_PENDING`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${LOGOUT_REQUEST}_FULFILLED`:
      return {
        ...initialState,

        // two following options should reflected by authorization subscription
        initial:    true,
        authorized: false,

        isUserProfileFulfilled: false,
      };
    case `${LOGOUT_REQUEST}_REJECTED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    case `${DELETE_REQUEST}_PENDING`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${DELETE_REQUEST}_REJECTED`:
      return {
        // TODO: don't do that, use immer!
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    default:
      return state;
  }
};
