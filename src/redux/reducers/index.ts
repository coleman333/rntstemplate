import { ReducersMapObject } from 'redux';
import { auth } from './Auth';
import { product, products } from './Products';
import { wallet } from './Wallet';
import { scanParams } from './Scan';
import { homeState } from './Home';
import { rewardsReducer } from './Rewards';
import { profileReducer } from './Profile';

export const reducers: ReducersMapObject<any> = {
  auth: auth,
  scan: scanParams,
  details: product,
  products: products,
  wallet: wallet,
  home: homeState,
  rewards: rewardsReducer,
  profile: profileReducer,
};
