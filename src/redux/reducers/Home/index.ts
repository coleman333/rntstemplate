import { ACTION_HOME_GET_DATA } from 'src/constants/ActionTypes/Home';
import { IReduxState } from 'src/types/redux';
import { UserCoupon } from "src/models/entities/Coupon";
import { ProductForConsumer } from "src/models/entities/Product";

export interface IHomeState extends IReduxState {
  pending?: boolean;
  coupons?: Array<UserCoupon>
  products?: Array<ProductForConsumer>
}

export const initialState: IHomeState = {
  pending: false,
};

export const homeState = function(state: IHomeState = initialState, action: any): IHomeState {
  switch (action.type) {
    case `${ACTION_HOME_GET_DATA}_PENDING`:
      return {
        ...state,
        pending: true,
        fulfilled: false,
        rejected: false,
      };
    case `${ACTION_HOME_GET_DATA}_FULFILLED`:
      return {
        ...state,
        ...action.payload,
        pending: false,
        fulfilled: true,
        rejected: false,
      };
    case `${ACTION_HOME_GET_DATA}_REJECTED`:
      return {
        ...state,
        error: action.error,
        pending: false,
        fulfilled: false,
        rejected: true,
      };
    default:
      return state;
  }
};
