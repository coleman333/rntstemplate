import {
  SCAN_PARAMS_SAVE,
  SCAN_PARAMS_CLEAR,
} from 'src/constants/ActionTypes/Scan';
import { ScanParamsObject } from 'src/screens/ScanTab/types';

export interface IScanState extends ScanParamsObject {}

export const initialState: IScanState = {
  n: '',
  i: '',
  r: '',
  c: ''
};

export const scanParams = (state: IScanState = initialState, action: any): IScanState => {
  switch (action.type) {
    case `${SCAN_PARAMS_SAVE}`:
      return {
        n: action.payload.n,
        i: action.payload.i,
        r: action.payload.r,
        c: action.payload.c,
      };
    case `${SCAN_PARAMS_CLEAR}`:
      return {
        n: initialState.n,
        i: initialState.i,
        r: initialState.r,
        c: initialState.c,
      };
    default:
      return state;
  }
};
