import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore,
  Middleware,
  Store,
  StoreEnhancer,
} from 'redux';

import { AppState } from 'src/types/redux';
import { reducers } from './reducers';

let store:Store<any>;

export const configureStore = (): Store<AppState> => {
  /*
  |────────────────────────────────────────────────────────────────────────────────
  | Collect all middlewares
  |────────────────────────────────────────────────────────────────────────────────
  | NOTE:
  | If you are using Redux Logger then it must be the last middleware in the
  | chain, otherwise it will log thunk/sagas and promises, not actual actions.
  | Source: https://github.com/evgenyrodionov/redux-logger/issues/20
  |────────────────────────────────────────────────────────────────────────────────
  */
  const allMiddlewares: Middleware[] = [thunk, logger];

  // Compose all middlewares
  const middleware: StoreEnhancer<any, {}> = __DEV__
    ? composeWithDevTools(applyMiddleware(...allMiddlewares))
    : compose(applyMiddleware(...allMiddlewares));

  return createStore(combineReducers({ ...reducers }), undefined, middleware);
};

export const setStore = function(): Store<any> {
  store = configureStore();
  return store;
};

export const getStore = function(): Store<any> {
  return store;
};