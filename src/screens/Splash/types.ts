import { IAuthState } from 'src/redux/reducers/Auth'

export interface Props {
  auth: IAuthState;
  getSocialIds: Function;
  getProfile(initial: boolean): Function;
  logout: Function;
}
