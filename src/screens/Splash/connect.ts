import { bindActionCreators, Dispatch } from 'redux';
import { reduxify } from 'src/utils/reduxify';

import { getSocialIdsAction, getProfile, logout } from 'src/actions/Auth';

const mapStateToProps = (state: any) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getSocialIds: bindActionCreators(getSocialIdsAction, dispatch),
    getProfile:   bindActionCreators(getProfile, dispatch),
    logout:       bindActionCreators(logout, dispatch),
  }
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
