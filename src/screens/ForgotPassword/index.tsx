import React from 'react';
import { Text } from 'react-native';

import { FullScreenContainer } from 'src/components/full-screen-contaner';

export class ForgotPasswordScreen extends React.Component {
  // RENDER

  public render(): React.ReactNode {
    return (
      <FullScreenContainer>
        <Text>Forgot Password screen</Text>
      </FullScreenContainer>
    );
  }
}
