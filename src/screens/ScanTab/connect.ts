import { bindActionCreators, Dispatch } from 'redux';
import { scanAction, getProductByPhoto, copyProductDetails } from "src/actions/Products";
import { saveScanParams } from "src/actions/Scan";
import { setDeepLink } from "src/actions/Auth";
import { reduxify } from 'src/utils/reduxify';

const mapStateToProps = (state: any) => ({
  auth: state.auth,
  details: state.details,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    scanAction:           bindActionCreators(scanAction,          dispatch),
    saveScanParams:       bindActionCreators(saveScanParams,      dispatch),
    getProductByPhoto:    bindActionCreators(getProductByPhoto,   dispatch),
    setDeepLink:          bindActionCreators(setDeepLink,         dispatch),
    copyProductDetails:   bindActionCreators(copyProductDetails,  dispatch),
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
