import { Navigation, WithTranslation } from 'src/types/react';
import { IProductDetailsState } from 'src/redux/reducers/Products/details';
import { IAuthState } from "src/redux/reducers/Auth";
import { ProductForConsumer } from '../../models/entities/Product';

export interface Props extends WithTranslation {
  navigation: Navigation;
  scanAction: Function;
  saveScanParams(params: ScanParamsObject): Function;
  getProductByPhoto(data: UploadedImageApollo): Function;
  clearScanParams: Function;
  details: IProductDetailsState;
  auth: IAuthState;
  closestProducts: Array<ProductForConsumer>;
  setDeepLink(link: string): Function;
  copyProductDetails(data: ProductForConsumer): Function;
}

export interface State {
  scanType: '' | 'photo' | 'qr' | 'nfc';
  displayError: boolean;
  displayLabelError: boolean;
  displayClosest: boolean;
  loader: boolean;
}

export interface ParsedLink {
  [key: string]: string;
  namespace: string;
  i:  string;  //identity (final)
  s?: string; //identity QR
  m?: string; //identity NFC
  n?: string;  //random number
  c:  string;  //access counter
}

export interface ScanParamsObject {
  n: string; //namespace
  i: string; //identity
  r: string; //random number
  c: string; //access counter
}

export interface CameraImageObject {
  uri:      string;
  base64?:  string;
  width:    number;
  height:   number;
}

export interface UploadedImageApollo {
  uri:      string;
  name:     string;
  type:     string;
}
