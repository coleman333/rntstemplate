import React from 'react';
import { View, Text } from 'react-native';

import { ScanMenu } from 'src/components/scan/navigation-menu/menu';

import { Props, State, ParsedLink, ScanParamsObject, CameraImageObject } from './types';
import { connect } from './connect';
import { styles } from './styles';
import { withNamespaces } from "react-i18next";
import { Navigation } from 'src/services/Navigation';
import { Routes } from 'src/navigation/route-names';

import { ErrorDialog } from "src/components/error-dialog";
import { Loader } from 'src/components/loader';
import { FullScreenContainer } from 'src/components/full-screen-contaner';
import { LabelScan } from 'src/components/scan/label';
import { ErrorLabelDialog } from 'src/components/scan/label/error';
import { ScanQR } from 'src/components/scan/qr';
import { ScanNFC } from 'src/components/scan/nfc';
import { ReactNativeFile } from'apollo-upload-client';
import { CommonBack } from "src/components/common-back";
import { ClosestMatches } from 'src/components/closest-matches-dialog';
import {ProductForConsumer} from "../../models/entities/Product";

const SCAN_TYPE_NONE  = '';
const SCAN_TYPE_PHOTO = 'photo';
const SCAN_TYPE_QR    = 'qr';
const SCAN_TYPE_NFC   = 'nfc';

@connect
export class ScanScreen extends React.Component<Props, State> {

  static deepLink: string = "";

  public static getDerivedStateFromProps(props: Props, state: State): State {
    if (props.auth.deeplink.length && !ScanScreen.deepLink.length) {
      state.scanType      = SCAN_TYPE_NONE;
      ScanScreen.deepLink = props.auth.deeplink;
    }
    return state;
  }

  public state: State = {
    scanType: SCAN_TYPE_NFC,
    displayError: false,
    displayLabelError: false,
    displayClosest: false,
    loader: false,
  };

  public componentDidMount() {
    if (this.props.auth.deeplink.length) {
      const link = this.props.auth.deeplink;
      this.props.setDeepLink("");
      this.showLoader();
      this.onProductLinkReceived(link);
    }
  }

  public componentDidUpdate (prevProps: Readonly<Props>): void {
    if (!prevProps.details.fulfilled && this.props.details.fulfilled) {
      this.hideLoader();
      if (typeof this.props.details.product !== 'undefined' && typeof this.props.details.product.id !== 'undefined') {
        this.onChangeScanType(SCAN_TYPE_NONE);
        let routeName = this.props.auth.authorized ? Routes.ProductForConsumer : Routes.ProductAfterScanUnAuth;
        let hideHeader = this.props.auth.authorized === false;
        Navigation.navigateTo({routeName: routeName, params: {scanned: true, hideHeader: hideHeader}});
      } else {
        if (typeof this.props.details.matches !== 'undefined' && this.props.details.matches.length) {
          this.showClosest();
        } else {
          this.showErrorDialog();
        }
      }
    }
    if (prevProps.details.rejected === false && this.props.details.rejected) {
      this.showErrorDialog();
    }
  }

  private onBackPressed = (): void => {
    Navigation.navigateBack();
  };

  private onClosestTapped = (data: ProductForConsumer): void => {
    this.props.copyProductDetails(data);
    this.hideClosest(() => {
      Navigation.navigateTo({
        routeName: Routes.ProductForConsumer,
        params: {
          scanned: true,
          hideHeader: false,
        }
      });
    });
  };

  private parseProductLink = (link: string): ScanParamsObject => {

    //console.log('LINK', link);
    let parsed: ParsedLink = {
      namespace: '',
      i: '', //identity (final)
      s: '', //identity QR
      m: '', //identity NFC
      n: '', //random number
      c: '', //access counter
    };

    let scanParamsObject: ScanParamsObject = {
      n: '',
      i: '',
      r: '',
      c: ''
    };

    let linkArray = link.split('/');
    let index = linkArray.length -1;

    try {
      if (typeof linkArray[index] !== 'undefined') {
        linkArray = linkArray[index].split('?');
        if (typeof linkArray[0] !== 'undefined') {
          parsed.namespace = linkArray[0];
        }
        if (typeof linkArray[1] !== 'undefined') {
          linkArray = linkArray[1].split('&');
          for (let i = 0; i < linkArray.length; i++) {
            let pair = linkArray[i].split('=');
            parsed[ pair[0] ] = pair[1];
          }
        }
      }
    } catch (e) {

    }

    if (parsed.s.length) {
      parsed.i = parsed.s;
    }
    if (parsed.m.length && parsed.m.indexOf('x') !== -1) {
      linkArray = parsed.m.split('x');
      parsed.i = linkArray[0];
      parsed.c = linkArray[1];
    }

    scanParamsObject.n = parsed.namespace;
    scanParamsObject.i = parsed.i;
    scanParamsObject.r = parsed.n;
    scanParamsObject.c = parsed.c;

    return scanParamsObject;
  };

  private onProductLinkReceived = (link: string): void => {
    let params : ScanParamsObject = this.parseProductLink(link);
    console.log('SCAN PARAMS', params);
    if (params.i) {
      this.props.saveScanParams(params);
      this.props.scanAction(params);
    } else {
      this.showErrorDialog();
    }
  };

  private onImageReceived = (data: CameraImageObject): void => {
    this.showLoader();
    const file = new ReactNativeFile({
      uri: data.uri,
      name: 'uploaded-photo.jpg',
      type: 'image/jpeg'
    });
    this.props.getProductByPhoto(file);
  };

  private onChangeScanType = (type: any): void => {
    this.setState({scanType: type});
  };

  private closeErrorDialog = (): void => {
    this.setState({displayError: false, displayLabelError: false});
  };

  private showErrorDialog  = (): void => {
    if (this.state.scanType === SCAN_TYPE_PHOTO) {
      this.setState({displayLabelError: true, loader: false, scanType: ''});
    } else {
      this.setState({displayError: true, loader: false, scanType: ''});
    }
  };

  private showClosest = (): void => {
    this.setState({
      displayClosest: true, loader: false, scanType: '',
    });
  };

  private hideClosest = (callback?: Function): void => {
    if (typeof callback !== 'undefined') {
      this.setState({ displayClosest: false }, () => {
        callback();
      });
    } else {
      this.setState({ displayClosest: false });
    }
  };

  private showLoader = (): void => {
    this.setState({loader: true});
  };

  private hideLoader = (): void => {
    this.setState({loader: false});
  };

  private resetToDefaults = (): void => {
    this.onChangeScanType(SCAN_TYPE_NONE);
  };

  private renderScanComponent = (): Array<React.ReactNode> => {
    const { t } = this.props;
    let JSX: Array<React.ReactNode> = [];
    switch (this.state.scanType) {
      case SCAN_TYPE_PHOTO:
        JSX.push(
          <LabelScan
            t = {t}
            key = {'label-scan'}
            onSuccess = {this.onImageReceived}
            showLoader = {this.showLoader}
            hideLoader = {this.hideLoader}
          />
        );
        break;
      case SCAN_TYPE_QR:
        JSX.push(
          <ScanQR
            key = {'scan-qr'}
            onError = {this.showErrorDialog}
            onSuccess = {this.onProductLinkReceived}
            showLoader = {this.showLoader}
            overlayText = {'Place QR code within the frame and hold it steady'}
          />
        );
        break;
      case SCAN_TYPE_NFC:
        JSX.push(
          <ScanNFC
            key = {'scan-nfc'}
            t = {this.props.t}
            onSuccess = {this.onProductLinkReceived}
            onCancel = {this.resetToDefaults}
          />
        );
        break;
      default:
        JSX.push(
          <View style = {styles.default} key = {'scan-default-screen'}>
            <Text style = {styles.message}>{t('Scan:scan_method')}</Text>
          </View>
        );
        break;
    }
    return JSX;
  };

  public render(): React.ReactNode {

    const { t, auth, details } = this.props;
    const { scanType } = this.state;
    const matches: Array<ProductForConsumer> = details.matches ? details.matches : [];


    return (
      <FullScreenContainer style={styles.container}>
        {auth.authorized === false &&
        <View style = {styles.headerArea}>
          <CommonBack title={t('Welcome:title')} theme={'light'} onPress={this.onBackPressed} />
        </View>
        }
        <View style = {auth.authorized ? styles.scanArea : styles.scanAreaUnAuth}>
          {this.renderScanComponent()}
        </View>
        <View style = {[styles.buttonsArea, auth.authorized ? {} : styles.buttonsAreaUnAuth]}>
          <ScanMenu
            t = {t}
            active = {scanType}
            onChange = {this.onChangeScanType}
          />
        </View>
        <ErrorDialog
          title   = {t('error.title')}
          message = {t('error.message')}
          email   = {t('error.email')}
          visible = {this.state.displayError}
          onPress = {this.closeErrorDialog}
        />
        <ErrorLabelDialog
          title   = {t('Scan:error.titleLabel')}
          message = {t('Scan:error.messageLabel')}
          buttonText = {t('Scan:error.buttonLabel')}
          visible = {this.state.displayLabelError}
          onPress = {this.closeErrorDialog}
        />
        <ClosestMatches
          title   = {t('Scan:closest.title')}
          message = {t('Scan:closest.message')}
          visible = {this.state.displayClosest}
          data    = { matches }
          onClose = {this.hideClosest}
          onTap   = {this.onClosestTapped}
        />
        <Loader visible={this.state.loader} />
      </FullScreenContainer>
    );
  }

}

export const ScanTabScreen = withNamespaces(['common','Scan', 'Welcome'])(ScanScreen);
