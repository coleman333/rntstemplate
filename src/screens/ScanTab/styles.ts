import { StyleSheet } from 'react-native';
import {COLORS} from "../../constants";

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'flex-start',
    backgroundColor: COLORS.OMNIAZ_submenu,
    flex: 1,
  },

  default: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: '20%',
  },

  message: {
    fontFamily: 'Poppins',
    fontSize: 13.5,
    color: COLORS.white,
    textAlign: 'center',
  },

  headerArea: {
    width: '100%',
    height: 50,
    backgroundColor: COLORS.navy,
  },

  scanArea: {
    width: '100%',
    height: '90%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  scanAreaUnAuth: {
    width: '100%',
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonsArea: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: COLORS.OMNIAZ_submenu,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  buttonsAreaUnAuth: {
    backgroundColor: COLORS.navy,
  },


});
