import React from 'react';
import { ScrollView, View } from 'react-native';
import { withNamespaces } from 'react-i18next';
import { Navigation } from 'src/services/Navigation';
import { Loader } from 'src/components/loader';
import { ProfileSettings as ProfileSettingsComponent } from 'src/components/profile/profileSettings'
import { Props, State } from "./types";
import { styles } from "./styles";
import { connect } from './connect';
import { DarkTop } from "../../components/dark-top";
import { HeaderBack } from "../../components/header-back";
import ProfileTermsAndConditions from "../../components/profile/profileTermsAndConditions";

@connect
class ProfileSettings extends React.Component<Props, State> {

  static navigationOptions = ({navigation} : {navigation: any}): any => {
    return {
      header: null,
    };
  };

  public state: State = {
    loader: false
  };

  private onBackPressed = (): void => {
    Navigation.navigateBack();
  };

  public render(): React.ReactNode {
    const { t, user } = this.props;
    console.log( 'user', user );
    return (
      <View style = {styles.container}>
        <DarkTop animate={false} rounded={true} style={styles.header}>
          <HeaderBack title={t('ProfileSettings:title')} onPress={this.onBackPressed} />
        </DarkTop>
        <ScrollView style = {styles.content}>
          {typeof user !== 'undefined' && typeof user.first_name !== 'undefined' &&
            <ProfileSettingsComponent
              t = { t }
              user = { user }
              onLogout = { this.props.logout }
              onDelete = { this.props.delete }
              onSave = { (userData) => {console.log('on save', userData);} }
            />
          }
          <View style = {styles.terms}>
            <ProfileTermsAndConditions t = { t } />
          </View>
        </ScrollView>
        {this.state.loader && <Loader visible = {this.state.loader} />}
      </View>
    );
  }

}

export const ProfileSettingsScreen = withNamespaces(['Profile', 'ProfileSettings'])(ProfileSettings);