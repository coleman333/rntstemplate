import { Navigation, WithTranslation } from 'src/types/react';
import { IAuthState } from "src/redux/reducers/Auth";
import { TranslationFunction } from "i18next";

export interface Props extends WithTranslation {
  t: TranslationFunction;
  navigation: Navigation<{ initial?: boolean }>;
  auth: IAuthState;
  user: any;
  logout: ()=>{};
  delete: ()=>{};
}

export interface State {
  loader: boolean;
}