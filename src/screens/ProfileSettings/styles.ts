import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants';

export const styles = StyleSheet.create({

  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.grayBackground,
  },

  header: {
    width: '100%',
    height: '25%',
  },

  content: {
    width: '100%',
    height: '75%',
  },

  profile: {
    width: '100%',
    height: '65%',
  },

  terms: {
    width: '100%',
    height: '10%',
  },


});
