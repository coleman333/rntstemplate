import { bindActionCreators, Dispatch } from 'redux';
import { logout, deleteProfileAction } from "../../actions/Auth";
import { reduxify } from 'src/utils/reduxify';

const mapStateToProps = (state: any) => ({
  auth:     state.auth,
  user:     state.auth.userProfile,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    logout:     bindActionCreators(logout, dispatch),
    //save:     bindActionCreators(save, dispatch),
    delete:     bindActionCreators(deleteProfileAction, dispatch),
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
