import React from 'react';
import { Props, State } from './types';
import { connect } from './connect';
import { styles } from './styles';
import { withNamespaces } from 'react-i18next';
import { FullScreenContainer } from 'src/components/full-screen-contaner';
import { ProfileForm } from 'src/components/profile/form';
import { User } from 'src/components/user';
import { Loader } from 'src/components/loader';
import { ScansTable } from 'src/components/scans-table';
import { ScansListHeader, ScansListBody } from 'src/components/scans-list';
import ModalDeleteAccount from 'src/screens/NetworkTab/ModalDeleteAccount';
import { Button, Icon, ViewStyle } from 'react-native-material-ui';
import { COLORS } from 'src/constants';
import { View, Text, TouchableOpacity, Linking, Keyboard ,ScrollView, RefreshControl} from 'react-native';
import { Bubble } from 'src/components/bubble';
import { HorizontalScroll } from 'src/components/horizontal-scroll';
import { ProductListItem } from "src/components/product/list-item";
import Product, { ProductForConsumer } from 'src/models/entities/Product';

@connect
export class ProfileScreen extends React.Component<Props, State> {

  static getDerivedStateFromProps(props: Props, state: State): State {
    if (typeof props.user !== 'undefined') {
      if (typeof props.user.first_name !== 'undefined') {
        state.first_name = props.user.first_name ? props.user.first_name : '';
      }
      if (typeof props.user.last_name !== 'undefined') {
        state.last_name = props.user.last_name ? props.user.last_name : '';
      }
      if (typeof props.user.email !== 'undefined') {
        state.email = props.user.email ? props.user.email : '';
      }
      if (typeof props.user.country !== 'undefined') {
        state.country = props.user.country ? props.user.country : '';
      }
    }

    state.isUpdating = false;

    return state;
  }

  componentDidMount() {
    this.update();
  }

  static navigationOptions = ({navigation} : {navigation: any}): any => {
    return {
      header: null,
    };
  };

  public state: State = {
    first_name: '',
    last_name: '',
    email: '',
    country: '',
    hideButton: false,
    displayDeleteModal: false,
    pageIndex: 0,
    isUpdating: true,
  };

  public constructor(props: Props) {
    super(props);
  }

  private setIndex(pageIndex: number) :void {
    this.setState({pageIndex});
  }

  private update() : void {
    // this.setState({isUpdating: true});
    this.props.getHistory({});
  }

  private renderBubbles(): Array<React.ReactNode> {
    let bubbles = new Array<React.ReactNode>();

    bubbles.push(<Bubble 
      key={'buuble1'}
      left={-100}
      top={-100}
      size={160}
      zIndex={-1}
    />);
    bubbles.push(<Bubble 
      key={'buuble2'}
      left={380}
      top={110}
      size={55}
      zIndex={-1}
    />);
    bubbles.push(<Bubble 
      key={'buuble3'}
      left={375}
      top={130}
      size={116}
      zIndex={-1}
    />);

    return bubbles;
  }
  
  private renderPage(category: string) : React.ReactNode {
    const { scans, t } = this.props;
    const page = new Array<React.ReactNode>();

    page.push(<TouchableOpacity 
      style={styles.viewMoreBackground}
      onPress={null}>
      <Text style={styles.viewMoreText}>
      {t('viewmore')}</Text>
    </TouchableOpacity>);
    
    for(const year in scans && scans[category]) {
      for(const month in scans[category][year]) {
        const row = new Array<React.ReactNode>();
        for(const product of scans[category][year][month]) {
          row.push(<ProductListItem
            onPress = {() => null}
            product = {product?.product}
            key = {'product-list-item-' + Math.random()}
          />);
          page.push(<HorizontalScroll
            key={`bootle-scan-list-${Math.random()}`}
          >
            {row}
          </HorizontalScroll>);
          page.push(<Text
            key={`scans-label-${Math.random()}`}
            style={styles.textDivider}>
              {`${t(`Profile:monthNames:${month}`)} ${year}`}
          </Text>);
        }
      }
    }

    if(page.length < 2) 
      return <View style={styles.noScansContainer}>
        <Text style={styles.noScansText}
        >{t('noscans')}</Text>
    </View> 

    return <View
      style={{ flexDirection: 'column-reverse' }}
    >{page}</View>;
  }

  public render(): React.ReactNode {
    const {t, user} = this.props;
    return (
      <View style={styles.mainContainer}>
        <ScrollView 
          refreshControl={<RefreshControl 
            onRefresh={() => {
              this.update();
              this.setState({isUpdating: true});
            }}
            colors={[COLORS.pink200]}
            refreshing={this.state.isUpdating}
          />}
          contentContainerStyle={styles.innerContainer}>
          <View style={styles.upperElevatedBlock} >
            <User 
              name={`${user.first_name} ${user.last_name}`}
              location={user.country ? user.country : '❌ unknown location'} // '🇸🇬 Singapoure'
              dateSince=' unknown date'
              netTab
            />
            {this.renderBubbles()}
            <ScansTable />
            <ScansListHeader 
              pagesNames={[t('Profile:tabs:wine'),
                t('Profile:tabs:beer'),
                t('Profile:tabs:spirits')]}
              pageIndex={this.state.pageIndex}
              setPageIndex={this.setIndex.bind(this)} />
          </View>
          <ScansListBody
            pages={[
              this.renderPage('Wine'),
              this.renderPage('Beer'),
              this.renderPage('Spirits'),
            ]}
            pageIndex={this.state.pageIndex}
          />
        </ScrollView>
      </View>
    );
  }
}

export const NetworkTabScreen = withNamespaces(['Profile'])(ProfileScreen);
