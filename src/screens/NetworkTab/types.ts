import { Navigation, WithTranslation } from 'src/types/react';
import { IGetUser } from 'src/interfaces/Auth'

export interface Props extends WithTranslation {
  user: any;
  logout: ()=>{};
  delete: ()=>{};
  getHistory: Function;
  scans: Array<Object>;
  keyboardDidShowListener:any;
}

export interface State {
  [key:string]: any,
  first_name: string;
  last_name: string;
  email: string;
  country: string;
  isUpdating: boolean;
}
