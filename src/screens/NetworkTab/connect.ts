import { bindActionCreators, Dispatch } from 'redux';

import { reduxify } from 'src/utils/reduxify';
import { logout, deleteProfileAction, updateUserProfile } from "../../actions/Auth";
import { loadScanHistory } from "../../actions/Scan";

const mapStateToProps = (state: any) => ({
  user:     state.auth.userProfile,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    logout:     bindActionCreators(logout, dispatch),
    //save:     bindActionCreators(updateUserProfile, dispatch),
    delete:     bindActionCreators(deleteProfileAction, dispatch),
    getHistory: bindActionCreators(loadScanHistory, dispatch),
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
