import { StyleSheet } from 'react-native';
import {COLORS} from "../../constants";

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-start',
    backgroundColor: '#e3e3e3',
    padding: 20,
  },

  iconArea: {
    width: '100%',
    height: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  formArea: {
    height: '70%',
    width: '100%'
  },

  buttonsArea: {
    width: '100%',
    height: 50,
    justifyContent: 'space-between',
    marginTop:'8%',
    flexDirection: 'row',
  },

  viewMoreText: {
    color: '#e497ac',
    textAlign: 'center',
    fontWeight: '600',
    backgroundColor: '#00253c',
    padding: 10,
    borderRadius: 3,
    elevation: 5,
    width: '40%',
  },

  viewMoreBackground: {
      margin: 20,
      alignItems: 'center',
      justifyContent: 'center',
  },

  image: {
    borderWidth: 1,
    width: 90,
    height: 90,
    borderRadius: 45,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white',
    marginTop: -200
  },

  buttonContainer: {
    marginRight: 10,
    width: 100,
    borderRadius: 5,
    backgroundColor: COLORS.navy,
  },

  textDivider: {
    marginTop: 18,
    marginBottom: 8,
    fontWeight: '500',
  },

  noScansContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },

  upperElevatedBlock: {
    marginBottom: 1,
    backgroundColor: COLORS.white,
    shadowOffset:{  width: 1,  height: 1 },
    shadowColor: COLORS.black,
    shadowOpacity: 0.7,
    elevation: 5,
  },

  noScansText: {
    color: 'rgba(0,0,0,.5)'
  },

  buttonText: {
    color: COLORS.lightMauve,
  },

  mainContainer:{
    width:'100%',
    backgroundColor: '#f2f2f2', 
    flex: 1,
  },
  innerContainer:{
    flexDirection: 'column',
  },
  termsAndPolicyContainer:{
    // flex: 1,
    height:'6%',
    flexDirection:'row',
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor:'#e3e3e3'
  },
  textLinkStyle:{
    color: 'black',
    textDecorationLine:'underline'
  }
});
