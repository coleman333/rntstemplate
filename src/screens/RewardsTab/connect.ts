import { bindActionCreators, Dispatch } from 'redux';

import { reduxify } from 'src/utils/reduxify';
import { getRewards } from "src/actions/Rewards";

const mapStateToProps = (state: any) => ({
  auth:     state.auth,
  rewards:  state.rewards,
  coupons:  state.rewards.coupons,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getRewards: bindActionCreators(getRewards, dispatch),
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
