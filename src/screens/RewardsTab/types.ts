import { Navigation, WithTranslation } from 'src/types/react';
import { IAuthState } from "src/redux/reducers/Auth";
import { IRewardsState } from "src/redux/reducers/Rewards";
import { UserCoupon } from "src/models/entities/Coupon";
import { TranslationFunction } from "i18next";

export interface Props extends WithTranslation {
  t: TranslationFunction;
  navigation: Navigation<{ initial?: boolean }>;
  auth: IAuthState;
  rewards: IRewardsState;
  coupons: Array<UserCoupon>;
  getRewards(payload: object): void;
}

export interface State {
  loader: boolean;
}