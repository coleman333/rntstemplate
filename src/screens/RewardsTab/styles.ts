import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({

  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.white,
    padding: 10,
  },

  scrollview: {
    flexGrow: 1,

  },

  wrapper: {
    width: '100%',
    height: 210,
  },

  item: {
    marginBottom: 10,
  },

  separator: {
    width: '100%',
    height: '1%',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.gray,
  },

  titleHolder: {
    marginBottom: 10,
  },

  title: {
    fontFamily: 'Poppins',
    fontSize: 14,
    color: COLORS.navy,
  },

});
