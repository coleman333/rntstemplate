import React from 'react';
import { ScrollView, View, Text } from 'react-native';
import { withNamespaces } from 'react-i18next';
import { Coupon } from 'src/components/coupon';
import { NoCoupons } from 'src/components/coupon/no-coupons';
import { Loader } from 'src/components/loader';
import { Props, State } from "./types";
import { styles } from "./styles";
import { connect } from './connect';
import { UserCoupon } from "src/models/entities/Coupon";
import { EventService } from "src/services/Events";

@connect
class RewardsScreen extends React.Component<Props, State> {

  public state: State = {
    loader: false
  };

  public componentDidMount(): void {
    this.props.getRewards({
      userCoupons: {},
    });
  }

  private onCouponEvent = (data: object): void => {
    EventService.addEvent({...data});
  };

  private renderCoupons = (): Array<React.ReactNode> => {
    const { coupons, t } = this.props;

    let JSX: Array<React.ReactNode> = [];
    if (typeof coupons !== 'undefined' && coupons.length > 0) {
      coupons.map((data: UserCoupon, i: number) => {
        JSX.push(
          <View style = {styles.item} key = {'coupon_' + i}>
            <View style = {styles.wrapper}>
              <Coupon
                t = {t}
                data = {data}
                single = {true}
                authorized = {true}
                couponEvent = {this.onCouponEvent}
              />
            </View>
            <View style = {styles.separator} />
          </View>
        );
      });
    } else {
      JSX.push(
        <NoCoupons text = {t('Coupons:noCoupons')} key = {'no-coupons'} />
      );
    }
    return JSX;
  };

  public render(): React.ReactNode {
    const { t } = this.props;
    return (
      <View style = {styles.container}>
        <View style = {styles.titleHolder}>
          <Text style = {styles.title}>{t('Coupons:title')}</Text>
        </View>
        <ScrollView bounces = {false} style = {styles.scrollview}>
          {this.renderCoupons()}
        </ScrollView>
        <Loader visible = {this.state.loader} />
      </View>
    );
  }

}

export const RewardsTabScreen = withNamespaces(['Coupon', 'Coupons'])(RewardsScreen);