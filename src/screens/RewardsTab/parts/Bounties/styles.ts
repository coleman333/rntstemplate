import { Dimensions, StyleSheet } from 'react-native';
import { COLORS } from 'src/constants';

export const styles = StyleSheet.create({
  rewardsWrapper: {
    marginHorizontal: 10,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: COLORS.lightBorderColor,
    backgroundColor: '#f5f5f5',
  },

  rewardsList: {
    height: 295,
  },

  rewardsContainer: {
    padding: 10,
  },

  marginBottom: {
    marginBottom: 10,
  },

  fakeItem: {
    height: Dimensions.get('window').height,
  },
});
