import React from 'react';
import { ScrollView, View } from 'react-native';
import { withNamespaces } from 'react-i18next';

import { AnyFunction } from 'src/types/common';
import { Card } from 'src/components/card';
import { CollapsibleReward } from 'src/components/collapsible-reward';
import { Props, State } from './types';
import { styles } from './styles';

const PLACEHOLDER_DATA: string[] = [
  'Facebook',
  'Instagram',
  'Twitter',
  'Facebook',
  'Instagram',
  'Twitter',
];

class BountiesComponent extends React.PureComponent<Props, State> {
  public state: State = {
    scrollEnabled: true,
    showFakeItem: false,
  };

  // Component variables

  private ScrollViewReference: ScrollView;

  // Setters

  private setScrollViewReference = (reference: ScrollView): void => {
    this.ScrollViewReference = reference;
  };

  // Event handlers

  private onExpandStart = (topOffset: number): void => {
    this.setState(
      { showFakeItem: true, scrollEnabled: false },
      (): void => {
        setTimeout(() => {
          // 10 is the margin between list elements
          this.ScrollViewReference.scrollTo({ x: 0, y: topOffset - 10, animated: true });
        }, 0);
      },
    );
  };

  private onExpandEnd = (): void => {
    if (this.state.showFakeItem) {
      this.setState({ showFakeItem: false });
    }
  };

  private onCollapseEnd = (): void => {
    this.setState({ scrollEnabled: true });
  };

  private onVerify = (index: number): AnyFunction => (value: string): void => {
    // TODO: replace index usage with reward ID from the server
    console.log(`item #${index + 1} --- value: ${value}`);
  };

  // RENDER

  public render(): React.ReactNode {
    const { t } = this.props;
    const { showFakeItem, scrollEnabled } = this.state;

    return (
      <Card title={t('Bounties:title')}>
        <View style={styles.rewardsWrapper}>
          <ScrollView
            nestedScrollEnabled
            style={styles.rewardsList}
            scrollEnabled={scrollEnabled}
            ref={this.setScrollViewReference}
          >
            <View style={styles.rewardsContainer}>
              {PLACEHOLDER_DATA.map(
                (type: string, index): React.ReactElement<View> => (
                  <CollapsibleReward
                    key={index}
                    style={index + 1 !== PLACEHOLDER_DATA.length ? styles.marginBottom : {}}
                    title={t(`${type}Reward:title`)}
                    onVerify={this.onVerify(index)}
                    onExpandStart={this.onExpandStart}
                    onExpandEnd={this.onExpandEnd}
                    onCollapseEnd={this.onCollapseEnd}
                    amount={100}
                    steps={[
                      t(`${type}Reward:follow us`),
                      t(`${type}Reward:like & share`),
                      t(`${type}Reward:enter URL`),
                    ]}
                  />
                ),
              )}
              {showFakeItem && <View style={styles.fakeItem} />}
            </View>
          </ScrollView>
        </View>
      </Card>
    );
  }
}

export const Bounties = withNamespaces([
  'common',
  'Bounties',
  'FacebookReward',
  'InstagramReward',
  'TwitterReward',
])(BountiesComponent);
