import { WithTranslation } from 'src/types/react';

export interface Props extends WithTranslation {}

export interface State {
  scrollEnabled: boolean;
  showFakeItem: boolean;
}
