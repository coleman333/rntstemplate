import React from 'react';
import { ScrollView, View } from 'react-native';
import { withNamespaces } from 'react-i18next';
import { Recommended } from './parts/Recommended';
import { Coupon } from 'src/components/coupon';
import { NoCoupons } from 'src/components/coupon/no-coupons';
import { Loader } from 'src/components/loader';
import { Props, State } from "./types";
import { styles } from "./styles";
import { connect } from './connect';
import { Navigation } from 'src/services/Navigation';
import { Routes } from "src/navigation/route-names";
import {EventService} from "../../services/Events";

@connect
class HomeScreen extends React.Component<Props, State> {

  static getDerivedStateFromProps(props: Props, state: State): State {
    if (state.loader) {
      if (props.home.fulfilled || props.home.rejected) {
        state.loader = false;
      }
    } else {
      if (props.home.pending) {
        state.loader = true;
      }
    }
    return state;
  }

  public state: State = {
    loader: false
  };

  public componentDidMount(): void {
    this.props.getHomeData({
      userCoupons: {},
      productList: {
        page_size: 10,
        featured_only: true,
      },
    });
  }

  private openProductDetails = (id: number) : void => {
    this.props.getProductAction({product_id: id});
    Navigation.navigateTo({routeName: Routes.ProductForConsumer, params: {direct: true}});
  };

  private onCouponEvent = (data: object): void => {
    EventService.addEvent({...data});
  };

  public render(): React.ReactNode {
    const { products, coupons, t } = this.props;
    return (
      <View style = {styles.container}>
        <ScrollView bounces={false} style = {styles.container}>
          <Recommended
            products = {products}
            onProductPress = {this.openProductDetails}
          />
          <View style = {styles.separator}></View>
          {typeof coupons !== 'undefined' &&
          <View style = {styles.couponArea}>
            {coupons.length > 0 &&
            <Coupon
              single = {true}
              t = {t}
              authorized = {true}
              data = {coupons[0]}
              couponEvent = {this.onCouponEvent}
            />
            }
            {coupons.length === 0 &&
            <NoCoupons text = {t('Coupons:noCoupons')} />
            }
          </View>
          }
        </ScrollView>
        <Loader visible = {this.state.loader} />
      </View>
    );
  }

}

export const HomeTabScreen = withNamespaces(['Coupon', 'Coupons'])(HomeScreen);