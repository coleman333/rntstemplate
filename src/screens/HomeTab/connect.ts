import { bindActionCreators, Dispatch } from 'redux';

import { reduxify } from 'src/utils/reduxify';
import { getHomeData } from "src/actions/Home";
import { getProductAction } from 'src/actions/Products';

const mapStateToProps = (state: any) => ({
  auth:     state.auth,
  home:     state.home,
  coupons:  state.home.coupons,
  products: state.home.products,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getHomeData:          bindActionCreators(getHomeData,             dispatch),
    getProductAction:     bindActionCreators(getProductAction,      dispatch),
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
