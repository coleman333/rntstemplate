import { Navigation, WithTranslation } from 'src/types/react';
import { IAuthState } from "src/redux/reducers/Auth";
import { IHomeState } from "src/redux/reducers/Home";
import { ProductForConsumer } from 'src/models/entities/Product';
import { UserCoupon } from "src/models/entities/Coupon";

export interface Props extends WithTranslation {
  navigation: Navigation<{ initial?: boolean }>;
  auth: IAuthState;
  home: IHomeState,
  products: Array<ProductForConsumer>;
  coupons: Array<UserCoupon>;
  getHomeData(data: any): Function;
  getProductAction(data: any): Function;
}

export interface State {
  loader: boolean;
}