import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({
  item: {
    justifyContent: 'space-between',
    width: 120,
    height: 200,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: COLORS.lightBorderColor,
    marginHorizontal: 5,
    paddingHorizontal: 6,
    paddingVertical: 12,
  },

  imageContainer: {
    width: '100%',
    alignItems: 'center',
  },

  image: {
    width: 63,
    height: 113,
  },

  text: {
    fontSize: 12.1,
    fontFamily: getFontFamily(),
    lineHeight: 16,
    letterSpacing: 0.4,
    color: COLORS.lightTextColor,
  },
});
