import { WithTranslation } from 'src/types/react';
import { ProductForConsumer } from 'src/models/entities/Product';

export interface Props extends WithTranslation {
  products: Array<ProductForConsumer>;
  onProductPress(id: number): void;
}
