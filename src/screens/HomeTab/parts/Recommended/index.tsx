import React from 'react';
import { View } from 'react-native';
import { withNamespaces } from 'react-i18next';
import { Props } from './types';

import { Card } from 'src/components/card';
import { HorizontalScroll } from 'src/components/horizontal-scroll';
import { ProductListItem } from "src/components/product/list-item";
import { ProductForConsumer } from 'src/models/entities/Product';


export class RecommendedComponent extends React.Component<Props> {

  private onProductPress = (id: number): void => {
    this.props.onProductPress(id);
  };

  public render(): React.ReactNode {
    const { t, products } = this.props;
    return (
      <Card title={t('title')}>
          <HorizontalScroll>
            {products && products.map(
              (item: ProductForConsumer, index: number): React.ReactElement<View> => (
                <ProductListItem
                  onPress = {this.onProductPress}
                  product = {item} key = {'product-list-item-' + index}
                />
              ),
            )}
          </HorizontalScroll>
      </Card>
    );
  }

}

export const Recommended = withNamespaces(['Recommended'])(RecommendedComponent);


/*
export const Recommended = withNamespaces('Recommended')((props: Props) => (
  <Card title={props.t('title')}>
    <HorizontalScroll>
      {createEmptyArray(5).map(
        (item, index): React.ReactElement<View> => (
          <View key={index} style={styles.item}>
            <View style={styles.imageContainer}>
              <Image style={styles.image as ImageStyle} source={BOTTLE_IMAGE} />
            </View>
            <View>
              <Text style={styles.text}>Amisfield</Text>
              <Text style={styles.text}>Pinot Noir 2015</Text>
            </View>
            <StarRating value={4.6} />
          </View>
        ),
      )}
    </HorizontalScroll>
  </Card>
));
*/