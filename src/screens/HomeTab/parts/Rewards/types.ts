import { WithTranslation } from 'src/types/react';

export interface Props extends WithTranslation {}
