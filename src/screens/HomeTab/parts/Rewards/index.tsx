import React from 'react';
import { withNamespaces } from 'react-i18next';

import { Card } from 'src/components/card';
import { CollapsibleReward } from 'src/components/collapsible-reward';
import { Coupon } from 'src/components/coupon';
import { Props } from './types';

const AD_IMAGE = require('./assets/ad_image.png');

export const Rewards = withNamespaces(['common', 'Rewards', 'FacebookReward'])((props: Props) => (
  <Card title={props.t('Rewards:title')}>
    <CollapsibleReward
      amount={100}
      title={props.t('FacebookReward:title')}
      onVerify={() => {}}
      style={{ marginHorizontal: 10 }}
      steps={[
        props.t('FacebookReward:follow us'),
        props.t('FacebookReward:like & share'),
        props.t('FacebookReward:enter URL'),
      ]}
    />

    <Coupon
      image={AD_IMAGE}
      deal={`20% ${props.t('common:off all purchases')}`}
      code="WC2018OMZ"
      secondaryText={`${props.t('common:valid until')} 31/12/2018`}
    />
  </Card>
));
