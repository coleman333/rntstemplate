import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import FeatherIcon from 'react-native-vector-icons/Feather';

import { Props } from './types';
import { styles } from './styles';

export const Filters: React.SFC<Props> = props => (
  <View style={styles.container}>
    {props.names.map(
      (name: string): React.ReactElement<TouchableOpacity> => {
        const selected: boolean = props.selected.indexOf(name) > -1;
        return (
          <TouchableOpacity
            key={name}
            onPress={props.onFilterPress(name)}
            style={[styles.button, selected && styles.selected]}
          >
            {selected && <FeatherIcon style={styles.icon} name="check" size={18} />}
            <Text style={styles.label}>{name}</Text>
          </TouchableOpacity>
        );
      },
    )}
  </View>
);
