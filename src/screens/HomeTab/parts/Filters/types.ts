import { AnyFunction } from 'src/types/common';

export interface Props {
  names: string[];
  selected: string[];
  onFilterPress: AnyFunction;
}
