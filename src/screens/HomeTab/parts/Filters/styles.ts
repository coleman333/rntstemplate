import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    marginVertical: 15,
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 90,
    height: 32,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.lightBorderColor,
    marginHorizontal: 8.5,
  },

  icon: {
    marginHorizontal: 8,
    color: 'rgba(0, 0, 0, 0.54)',
  },

  label: {
    fontSize: 14.1,
    fontFamily: getFontFamily(),
    lineHeight: 20,
    letterSpacing: 0.3,
    color: COLORS.lightTextColor,
  },

  selected: {
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(0, 0, 0, 0.08)',
  },
});
