import React from 'react';
import { Image, Text, View } from 'react-native';
import { withNamespaces } from 'react-i18next';

import { createEmptyArray } from 'src/utils/createEmptyArray';
import { Card } from 'src/components/card';
import { HorizontalScroll } from 'src/components/horizontal-scroll';
import { StarRating } from 'src/components/star-rating';
import { Props } from './types';
import { styles } from './styles';

const RETAILER_IMAGE = require('./assets/retailer.png');
const LOCATION_PIN = require('./assets/location-pin.png');

export const Retailers = withNamespaces(['common', 'Retailers'])((props: Props) => (
  <Card title={props.t('Retailers:title')}>
    <HorizontalScroll>
      {createEmptyArray(3).map(
        (item, index): React.ReactElement<View> => (
          <View key={index} style={styles.container}>
            <View style={styles.retailer}>
              <Image source={RETAILER_IMAGE} style={{ width: '100%', height: '100%' }} />
            </View>
            <View style={styles.infoContainer}>
              <View>
                <View style={styles.info}>
                  <Text style={styles.ratingValue}>4.7</Text>
                  <Text style={styles.ratingAmount}>12223 {props.t('common:ratings')}</Text>
                </View>
                <StarRating value={4.6} />
              </View>
              <View style={styles.info}>
                <Text style={styles.distance}>1.2KM</Text>
                <Image source={LOCATION_PIN} />
              </View>
            </View>
          </View>
        ),
      )}
    </HorizontalScroll>
  </Card>
));
