import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({
  container: {
    width: 300,
    height: 175,
    marginHorizontal: 5,
  },

  retailer: {
    width: '100%',
    height: 100,
    borderTopStartRadius: 4,
    borderTopEndRadius: 4,
    overflow: 'hidden',
  },

  infoContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    padding: 16,
    borderBottomStartRadius: 4,
    borderBottomEndRadius: 4,
    borderWidth: 1,
    borderTopWidth: 0,
    borderColor: COLORS.lightBorderColor,
  },

  info: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },

  ratingValue: {
    fontSize: 23.7,
    fontFamily: getFontFamily('semi-bold'),
    color: COLORS.navy,
    marginRight: 3,
  },

  ratingAmount: {
    fontSize: 12.1,
    fontFamily: getFontFamily(),
    lineHeight: 16,
    letterSpacing: 0.4,
    color: COLORS.navy,
    marginBottom: 3,
  },

  distance: {
    fontSize: 14,
    fontFamily: getFontFamily('medium'),
    lineHeight: 16,
    letterSpacing: 1.3,
    color: COLORS.navy,
  },
});
