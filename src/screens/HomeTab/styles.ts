import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.white,
  },

  separator: {
    width: '100%',
    height: '1%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.gray,
  },

  couponsSeparator: {
    marginLeft: 10,
  },

  couponsArea: {
    width: '100%',
    height: 200,
    padding: 10,
  },

  couponArea: {
    width: '100%',
    height: 230,
    padding: 10,
  },

});
