import { StyleSheet } from 'react-native';
import {COLORS} from "../../constants";

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'flex-start',
    backgroundColor: COLORS.white,
    width: '100%',
    height: '100%',
  },

  header: {
    width: '100%',
    height: 45,
    flexDirection: 'row',
  },

  left: {
    width: '50%',
    height: '100%',
    backgroundColor: COLORS.white,
  },

  right: {
    width: '50%',
    height: '100%',
    backgroundColor: COLORS.navy,
  },

  emptyBlock: {
    width: '100%',
    height: 20,
  },


});
