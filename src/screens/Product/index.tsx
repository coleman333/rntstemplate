import React from 'react';
import { ScrollView, View } from 'react-native';

import { Props, State, TraceabilityData, Org } from './types';
import { connect } from './connect';
import { styles } from './styles';
import { withNamespaces } from "react-i18next";
import { Navigation } from 'src/services/Navigation';
import { Routes } from "src/navigation/route-names";

import { Loader } from 'src/components/loader';
import { BottleDetails } from 'src/components/product/bottle-details';
import { TextBlock } from 'src/components/product/text-block';
import { Rewards } from 'src/components/product/rewards';
import { Traceability } from "src/components/product/traceability";
import { Organization } from 'src/models/entities/Organization';
import TraceabilityInfo from 'src/components/product/traceability/assets/tracebility';

import { FullScreenContainer } from 'src/components/full-screen-contaner';
import { Header } from "src/navigation/application/header";

@connect
export class ProductDetails extends React.Component<Props, State> {

  static navigationOptions = ({navigation} : {navigation: any}) => {
    const hidden = navigation.getParam('hideHeader', false);
    if (hidden) return null;
    return {
      header: (
        <View style = {styles.header}>
          <View style = {styles.left}></View>
          <View style = {styles.right}>
            <Header theme="product" align="right"/>
          </View>
        </View>
      )
    };
  };

  public state: State = {
  };

  private onBackPressed = (): void => {
    Navigation.navigateBack();
  };

  private setRating (rating: number): void {
    // this.setState({ starCount:rating } );
    if (
      this.props.details.product.user_review &&
      this.props.details.product.user_review.id &&
      this.props.auth.authToken
    ) {
      this.props.setRating({
        id: this.props.details.product.user_review.id,
        numerical_rating: rating,
        reviewee: {
          reviewee_id: this.props.details.product.id,
          reviewee_type: 'REVIEWEE_PRODUCT',
        },
        reviewer: {
          reviewer_type: 'REVIEWER_USER',
        },
      });
    } else {
        this.props.setRating({
        numerical_rating: rating,
        reviewee: {
          reviewee_id: this.props.details.product.id,
          reviewee_type: 'REVIEWEE_PRODUCT',
        },
        reviewer: {
          reviewer_type: 'REVIEWER_USER',
        },
      });
    }
    //console.log(this.props.details.product.user_review);
  }

  private onSignupBannerPressed = (): void => {
    Navigation.navigateTo(Routes.Login);
  };

  private getAwards = (awards: Array<any>): string => {
    return awards.map((item) => {
      return item.text;
    }).join(', ');
  };

  private getAromas = (aromas: Array<any>): string => {
    return aromas.map((item) => {
      return item.tag_text + '('+ item.count +')';
    }).join(', ');
  };

  private available(entity: any): boolean {
    return typeof entity !== 'undefined' && entity !== null;
  }

  private getOrganization = (type: string, data: Array<Organization>): Org => {
    let result: Org = { title: '', text: ''};
    if ( typeof data !== 'undefined' && data.length ) {
      data.map( (item: Organization) => {
        if (item.role_type.toUpperCase() === type.toUpperCase()) {
          item.organization_translations.map((t) => {
            if (t.language.trim() === 'en') { //TODO: change to actual language from props
              result.title  = t.display_name;
              result.text   = t.description;
            }
          });
        }
      });
    }
    return result;
  };

  private getTraceability = (data: any): TraceabilityData => {

    const { details } = this.props;

    let result : TraceabilityData;
    let SKU: string;

    if (typeof details.product !== 'undefined') {
      if (typeof details.product.product_organizations !== 'undefined' && details.product.product_organizations.length) {
        try {
          SKU     = details.product.product_organizations[0].SKU;
          result  = data[ SKU ];
        } catch (e) {
          console.log(e.message);
        }
      }
    }

    console.log('traceability', result);
    return result;
  };


  public render(): React.ReactNode {

    const { t, details, auth }  = this.props;
    const DIRECT                = this.props.navigation.getParam('direct', false);

    if (typeof details.product === 'undefined' || details.product === null || details.fulfilled === false) {
      return (<Loader visible = {true} />);
    }

    if (details.rejected) {
      console.log('TODO: Product.details add error handling here...');
      return null;
    }

    let traceability = this.getTraceability(TraceabilityInfo);

    const PRODUCER: Org = this.getOrganization('PRODUCER', details.product.organizations);
    const RETAILER: Org = this.getOrganization('RETAILER', details.product.organizations);

    return (
      <FullScreenContainer style={styles.container}>
        <ScrollView>
          <BottleDetails
            t         = {t}
            auth      = {auth}
            verified  = {!DIRECT}
            details   = {details}
            onBackPressed={this.onBackPressed}
            onRate={this.setRating.bind(this)}
            labels    = {{
              price_label:  t('Product:price_label'),
              buy_text:     t('Product:buy_text'),
              omniaz_id:    t('Product:omniaz_id')
            }}
          />
          {auth.authorized === false &&
            <Rewards
              title   = {'Rewards'}
              labels  = {{
                earned: t('Product:banner.earned'),
                signup: t('Product:banner.signup')
              }}
              onBannerPressed={this.onSignupBannerPressed}
            />
          }
          {this.available(details.product.label_notes) === true &&
          <TextBlock
            title   = {t('Product:description')}
            text    = {details.product.label_notes}
          />
          }
          {this.available(details.product.awards) === true &&
          <TextBlock
            title   = {t('Product:awards')}
            text    = {this.getAwards(details.product.awards)}
          />
          }
          {this.available(details.product.aromas) === true &&
          <TextBlock
            title   = {t('Product:aromas')}
            text    = {this.getAromas(details.product.aromas)}
          />
          }
          {this.available(details.product.food_pairings) === true &&
          <TextBlock
            title   = {t('Product:food pairings')}
            text    = {this.getAromas(details.product.food_pairings)}
          />
          }
          {this.available(details.product.tasting_note) === true &&
          <TextBlock
            title   = {t('Product:tasting notes')}
            text    = {details.product.tasting_note}
          />
          }
          {auth.authorized === true && PRODUCER.text !== '' &&
          <TextBlock
            title   = {t('Product:producer') + (PRODUCER.title ? "\n" + PRODUCER.title : '')}
            text    = {PRODUCER.text}
          />
          }
          {auth.authorized === true && RETAILER.text !== '' &&
          <TextBlock
            title   = {t('Product:retailer') + (RETAILER.title ? "\n" + RETAILER.title : '')}
            text    = {RETAILER.text}
          />
          }
          {DIRECT === false && traceability !== undefined &&
            <Traceability data={this.getTraceability(TraceabilityInfo)} />
          }
          <View style = {styles.emptyBlock} />
        </ScrollView>
      </FullScreenContainer>
    );
  }
}

export const Product = withNamespaces(['common','Product'])(ProductDetails);
