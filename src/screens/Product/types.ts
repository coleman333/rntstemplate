import { Navigation, WithTranslation } from 'src/types/react';
import { IProductDetailsState } from 'src/redux/reducers/Products/details';
import { IAuthState } from "src/redux/reducers/Auth";

export interface Props extends WithTranslation {
  navigation: Navigation;
  details: IProductDetailsState;
  auth: IAuthState;
  scanned?: boolean;
  setRating: (rating:any) => {};
}

export interface State {
  
}

export interface TraceabilityData {
  [key: string]: TraceabilityItem;
}

export interface TraceabilityItem {
  Country:  string;
  Address:  string;
  Date:     string;
}

export interface Org {
  title: string;
  text: string;
}
