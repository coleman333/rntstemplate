import { bindActionCreators, Dispatch } from 'redux';
import { reduxify } from 'src/utils/reduxify';
import { reviewAction } from 'src/actions/Rating';

const mapStateToProps = (state: any) => ({
  details: state.details,
  auth: state.auth,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setRating: bindActionCreators(reviewAction, dispatch)
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
