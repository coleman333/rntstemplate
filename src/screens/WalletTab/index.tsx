import React from 'react';
import { withNamespaces } from 'react-i18next';
import { Image, ImageStyle, Text, View } from 'react-native';
import { Button } from 'react-native-material-ui';

import { FullScreenContainer } from 'src/components/full-screen-contaner';
import { Props } from './types';
import { styles } from './styles';
import { connect } from'./connect';

const OMZLogo = require('../../../assets/images/omz.png');

@connect
export class WalletTabScreenComponent extends React.PureComponent<Props> {
  public componentDidMount (): void {
    const { id } = this.props.userProfile;
    this.props.getBalance( {
      balanceInput: {user_id: id},
      currencyInput: 'USD',
    });
  };

  private static calculateAmount(value: number): string {
    const tmp = value.toFixed(2);
    return tmp.slice(tmp.length - 2) === '99' ? (value + 0.01).toFixed(2) : tmp;
  }

  public render(): React.ReactNode {
    const { t, balanceCurrency = 0, balanceOMZ = 0, conversationRate = 0 } = this.props;
    return (
      <FullScreenContainer>
        <View style={[styles.block, styles.top]}>
          <View style={styles.logoContainer}>
            <Image style={styles.logo as ImageStyle} source={OMZLogo} />
            <View style={styles.logoTitleContainer}>
              <Text style={styles.amount}>OMZ</Text>
            </View>
          </View>
          <Text style={styles.YourBalanceStyle}>Your Balance</Text>
          <Text style={styles.amount}>{balanceOMZ}</Text>

          <View style={styles.priceContainer}>
            <Text style={styles.priceText}>
              (~$ {WalletTabScreenComponent.calculateAmount(balanceCurrency)} USD)
            </Text>
            <Text style={styles.priceText}>1 OMZ = {conversationRate.toFixed(4)} USD</Text>
          </View>
        </View>

        <View style={[styles.block, styles.bottom]}>
          <Text style={styles.note}>{t('WalletTab:Get more credits')}</Text>
        </View>
      </FullScreenContainer>
    );
  }
}

export const WalletTabScreen = withNamespaces(['common', 'WalletTab'])(WalletTabScreenComponent);
