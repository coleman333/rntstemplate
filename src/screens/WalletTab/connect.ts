import { bindActionCreators, Dispatch } from 'redux';
import { reduxify } from 'src/utils/reduxify';

import { getBalance } from 'src/actions/Wallet';

const mapStateToProps = (state: any) =>{
  return {
    balanceOMZ: state.wallet.balanceOMZ,
    balanceCurrency: state.wallet.balanceCurrency,
    conversationRate: state.wallet.conversationRate,
    userProfile: state.auth.userProfile,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getBalance: bindActionCreators(getBalance, dispatch),
  }
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
