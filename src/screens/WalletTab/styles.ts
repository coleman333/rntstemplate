import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({
  block: {
    width: '100%',
    alignItems: 'center',

  },
  top: {
    backgroundColor: COLORS.navy,
    // justifyContent: 'space-between',
    flex:6
  },
  bottom: {
    flex: 4,
    // justifyContent: 'center',
  },
  logoContainer:{
    flexDirection: 'row',
    marginTop:'10%'
  },
  logo: {
    marginTop: 6,
    width: 72,
    height: 82
  },
  YourBalanceStyle:{
    marginTop:'9%',
    fontFamily: getFontFamily(),
    fontSize: 14,
    lineHeight: 12 * 1.32,
    letterSpacing: 0.4,
    color: COLORS.white,
  },
  amount: {
    marginTop:'5%',
    fontSize: 48.3,
    fontFamily: getFontFamily('bold'),
    color: COLORS.white,
  },
  logoTitleContainer:{
    marginLeft:"3%",
    justifyContent:'center',
    alignItems: "center"
  },

  priceContainer: {
    // width: 234,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  priceText: {
    fontSize: 12,
    fontFamily: getFontFamily(),
    lineHeight: 12 * 1.32,
    letterSpacing: 0.4,
    color: COLORS.white,
  },

  note: {
    width: 180,
    fontSize: 14,
    fontFamily: getFontFamily(),
    lineHeight: 14 * 1.32,
    letterSpacing: 0.4,
    color: COLORS.navy,
    marginTop:'15%'
    // textAlign: 'center',
    // justifyContent:'center'
  },

  button: {
    width: 128,
    height: 36,
    borderRadius: 4,
    backgroundColor: COLORS.navy,
    marginVertical: 16,
  },
  buttonLabel: {
    fontSize: 14,
    fontFamily: getFontFamily('medium'),
    lineHeight: 14 * 1.15,
    letterSpacing: 1.3,
    color: COLORS.white,
  },
});
