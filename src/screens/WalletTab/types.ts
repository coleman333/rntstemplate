import { WithTranslation } from 'src/types/react';
import { IWallet } from 'src/interfaces/Wallet'
import { IGetUser } from 'src/interfaces/Auth'

export interface Props extends WithTranslation, IWallet {
  getBalance: Function;
  userProfile: IGetUser;
}
