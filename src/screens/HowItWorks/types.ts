import { Navigation, WithTranslation } from 'src/types/react';

export interface Props extends WithTranslation {
  navigation: Navigation;
}

export interface State {
  index: number;
}
