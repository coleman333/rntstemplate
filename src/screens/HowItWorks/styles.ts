import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    backgroundColor: COLORS.gray,
  },

  topArea: {
    width: '100%',
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.navy,
  },

  svgArea: {
    width: '100%',
    height: 45,
  },

  contentArea: {
    position: 'absolute',
    top: '15%',
    left: 0,
    height: '75%',
    width: '100%',
    zIndex: 9999,
  },

  buttonArea: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
    flexGrow: 1,
    paddingBottom: '2%',
  },

  nextButtonContainer: {
    padding: 15,
  },

  nextButton: {
    opacity: 0.9,
    color: COLORS.black,
    fontSize: 18,
    fontWeight: '500',
  },

  darkTopContent: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: '40%',
  },

  swiperPagination: {
    paddingBottom: 0,
  },

  swiperPaginationDot: {
    width: 12,
    height: 12,
    borderRadius: 6,
    borderWidth: 1,
    backgroundColor: COLORS.navy,
    borderColor: COLORS.navy,
  },

  swiperPaginationDot_Active: {
    width: 12,
    height: 12,
    borderRadius: 6,
    borderWidth: 1,
    backgroundColor: COLORS.lightMauve,
    borderColor: COLORS.lightMauve,
  },

  slide: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
  },

  slideHeader: {
    width: '100%',
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  slideContent: {
    paddingTop: 20,
    width: '100%',
    height: '35%',
  },

  slideHeaderText: {
    color: COLORS.white,
    fontSize: 24,
    fontWeight: '500',
    marginTop: '-10%',
    marginBottom: 30,
  },

  slideContentText: {
    textAlign: 'center',
    flexWrap: 'wrap',
    color: COLORS.navy,
    fontSize: 20,
    fontWeight: '500',
  },

  slideIcon_Slide1: {
    width: 24,
    height: 68,
  },

  slideIcon_Slide2: {
    width: 41,
    height: 35,
  },

  slideIcon_Slide3: {
    width: 123,
    height: 75,
  },

  slideIcon_Slide4: {
    width: 50,
    height: 45,
  },
});
