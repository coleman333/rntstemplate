import React from 'react';
import { withNamespaces } from 'react-i18next';
import { Image, ImageStyle, Text, TouchableHighlight, View, ViewStyle } from 'react-native';
import Swiper from 'react-native-swiper';

import { Navigation } from 'src/services/Navigation';
import { FullScreenContainer } from 'src/components/full-screen-contaner';
import { DarkTop } from 'src/components/dark-top';
import { HeaderBack } from 'src/components/header-back';
import { Props, State } from './types';
import { styles } from './styles';

class HowItWorksScreenComponent extends React.Component<Props, State> {
  public state: State = {
    index: 0,
  };

  // Component variables

  private swiper: any;

  // Setters

  private setSwiperReference = (reference: any): void => {
    this.swiper = reference;
  };

  // Event handlers

  private onBackPressed = (): void => {
    Navigation.navigateBack();
  };

  private onNextPressed = (): void => {
    if (this.swiper) {
      this.swiper.scrollBy(1);
    }
  };

  private onSwiperIndexChanged = (index: number): void => {
    this.setState({ index: index });
  };

  // RENDER

  public render(): React.ReactNode {
    const { t } = this.props;

    return (
      <FullScreenContainer style={styles.container}>
        <DarkTop animate={false} style={styles.darkTopContent}>
          <HeaderBack title={t('How it works')} onPress={this.onBackPressed} />
        </DarkTop>
        <View style={styles.contentArea}>
          <Swiper
            ref={this.setSwiperReference}
            showsButtons={false}
            showsPagination={true}
            loop={false}
            paginationStyle={styles.swiperPagination as ViewStyle}
            dotStyle={styles.swiperPaginationDot as ViewStyle}
            activeDotStyle={styles.swiperPaginationDot_Active as ViewStyle}
            onIndexChanged={this.onSwiperIndexChanged}
          >
            <View style={styles.slide}>
              <View style={styles.slideHeader}>
                <Text style={styles.slideHeaderText}>{t('step1')}</Text>
                <Image
                  style={styles.slideIcon_Slide1 as ImageStyle}
                  source={require('./assets/step1.png')}
                  resizeMode={'contain'}
                />
              </View>
              <View style={styles.slideContent}>
                <Text style={styles.slideContentText}>{t('slide1')}</Text>
              </View>
            </View>

            <View style={styles.slide}>
              <View style={styles.slideHeader}>
                <Text style={styles.slideHeaderText}>{t('step2')}</Text>
                <Image
                  style={styles.slideIcon_Slide2 as ImageStyle}
                  source={require('./assets/step2.png')}
                  resizeMode={'contain'}
                />
              </View>
              <View style={styles.slideContent}>
                <Text style={styles.slideContentText}>{t('slide2')}</Text>
              </View>
            </View>

            <View style={styles.slide}>
              <View style={styles.slideHeader}>
                <Text style={styles.slideHeaderText}>{t('step3')}</Text>
                <Image
                  style={styles.slideIcon_Slide3 as ImageStyle}
                  source={require('./assets/step3.png')}
                  resizeMode={'contain'}
                />
              </View>
              <View style={styles.slideContent}>
                <Text style={styles.slideContentText}>{t('slide3')}</Text>
              </View>
            </View>

            <View style={styles.slide}>
              <View style={styles.slideHeader}>
                <Text style={styles.slideHeaderText}>{t('step4')}</Text>
                <Image
                  style={styles.slideIcon_Slide4 as ImageStyle}
                  source={require('./assets/step4.png')}
                  resizeMode={'contain'}
                />
              </View>
              <View style={styles.slideContent}>
                <Text style={styles.slideContentText}>{t('slide4')}</Text>
              </View>
            </View>
          </Swiper>
        </View>
        {this.state.index !== 3 && (
          <View style={styles.buttonArea}>
            <TouchableHighlight
              style={styles.nextButtonContainer}
              underlayColor={'transparent'}
              onPress={this.onNextPressed}
            >
              <Text style={styles.nextButton}>{t('next')}</Text>
            </TouchableHighlight>
          </View>
        )}
      </FullScreenContainer>
    );
  }
}

export const HowItWorksScreen = withNamespaces('HowItWorks')(HowItWorksScreenComponent);
