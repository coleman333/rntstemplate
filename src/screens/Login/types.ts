import { Navigation, WithTranslation } from 'src/types/react';
import { IAuthState } from "src/redux/reducers/Auth";
import { IScanState } from "src/redux/reducers/Scan";

export interface Props extends WithTranslation {
  auth: IAuthState;
  scan: IScanState;
  navigation: Navigation;
  login(platform: any): Function;
  getProfile(): Function;
  getSocialIdsAction(): Function;
  clearScanParams(): Function;
}

export interface State {
  loader: boolean;
}

export interface LoginPayload {
  [key: string]: any;
}