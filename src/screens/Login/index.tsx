import React from 'react';
import {Text, View, Image, ImageStyle, Platform} from 'react-native';
import { Button } from 'react-native-material-ui';

import { FullScreenContainer } from 'src/components/full-screen-contaner';
import { DarkTop } from 'src/components/dark-top';
import { HeaderBack } from 'src/components/header-back';
import { Bubble } from "src/components/bubble";
import { Navigation } from 'src/services/Navigation';
import { Routes } from 'src/navigation/route-names';
import { Props, State, LoginPayload } from './types';
import { connect } from './connect';
import { styles } from './styles';
import { withNamespaces } from "react-i18next";
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSigninSingleton as GoogleSignIn } from 'react-native-google-signin/src/GoogleSignin';
import { Loader } from 'src/components/loader';
import { RNToasty } from 'react-native-toasty'
import { config } from "src/config";

import FB_PERMISSIONS from './fb-permissions-list.json';
import GOOGLE from './google-client.json';

const LOGO = require('../Welcome/assets/drnk.png');

@connect
class LoginScreenComponent extends React.Component<Props, State> {

  public state: State = {
    loader: false
  };

  public componentDidUpdate(prevProps: Readonly<Props>) : void {
    if (prevProps.auth.authToken === false && this.props.auth.authToken) {
      //got auth token (login complete)
      this.props.getProfile();
    }
    if (prevProps.auth.isUserProfileFulfilled === false && this.props.auth.isUserProfileFulfilled) {
      //got profile
      this.redirectToHome();
    }
  }

  private redirectToHome = (): void => {
    Navigation.navigateTo(Routes.Application);
  };

  private onBackPressed = (): void => {
    Navigation.navigateBack();
  };

  private fbLogin = async (): Promise<any> => {

    this.setState({loader: true});

    let result, token;
    try {
      //LoginManager.logOut(); //TODO: https://github.com/facebook/facebook-swift-sdk/issues/215#issuecomment-441261944
      if ( Platform.OS === 'android' ) {
        LoginManager.setLoginBehavior('web_only');
        LoginManager.logOut();
      } else {
        LoginManager.setLoginBehavior('web');
        LoginManager.logOut();
      }
      result = await LoginManager.logInWithReadPermissions(FB_PERMISSIONS);
      console.log('FB LOGIN:', result);
    } catch (e) {
      console.log('FB LOGIN:', e.message);
      RNToasty.Error({
        title: e.message,
        titleSize: 12,
      });
    }

    if (result && result.grantedPermissions && result.grantedPermissions.length) {
      token = await AccessToken.getCurrentAccessToken();
      if (token.accessToken) {
        let payload: LoginPayload = {
          oauth_token: token.accessToken,
          platform: 'FACEBOOK',
        };
        if (this.props.scan.i) {
          payload.scan_parameters = this.props.scan;
        }
        this.props.login(payload);
      }
    } else {
      this.setState({loader: false});
    }

  };

  private googleLogin = async () : Promise<any> => {

    this.setState({loader: true});

    let userInfo;
    try {
      if ( Platform.OS === 'android' ) {
        await GoogleSignIn.configure({
          scopes: GOOGLE.clientID.android_scopes,
          offlineAccess: false,
        });
      } else {
        if (config.PRODUCTION) {
          await GoogleSignIn.configure({
            iosClientId: GOOGLE.clientID.ios.prod,
          });
        } else {
          await GoogleSignIn.configure({
            iosClientId: GOOGLE.clientID.ios.dev,
          });
        }
      }
      let already = GoogleSignIn.isSignedIn();
      if (already) {
        await GoogleSignIn.signOut();
      }
      userInfo = await GoogleSignIn.signIn();
    } catch (error) {
      this.setState({loader: false});
      console.log(error);
      RNToasty.Error({
        title: error.message,
        titleSize: 12,
      });
    }

    console.log('userInfo', userInfo);

    if (typeof userInfo.accessToken !== 'undefined')  {
      let payload: LoginPayload = {
        oauth_token: userInfo.accessToken,
        platform: 'GOOGLE',
      };
      if (this.props.scan.i) {
        payload.scan_parameters = this.props.scan;
      }
      this.props.login(payload);
    } else {
      this.setState({loader: false});
    }

  };

  // RENDER

  public render(): React.ReactNode {
    const { t } = this.props;
    return (
      <FullScreenContainer style={styles.container}>
        <DarkTop animate={false} rounded={true} style={styles.darkTopContent}>
          <HeaderBack title={t('title')} onPress={this.onBackPressed} icon = {<Image source={LOGO} />}/>
        </DarkTop>
        <View style={styles.contentArea}>
          <Button
            style={{ container: styles.buttonContainer, text: styles.buttonText }}
            upperCase={false}
            text={t('loginWithFB')}
            icon={<Image source={require('./assets/fb.png')} style={styles.iconFB as ImageStyle}/>}
            onPress={this.fbLogin}
          />
          <Button
            style={{ container: styles.buttonContainer, text: styles.buttonText }}
            upperCase={false}
            text={t('loginWithGoogle')}
            icon={<Image source={require('./assets/google.png')} style={styles.iconFB as ImageStyle}/>}
            onPress={this.googleLogin}
          />
          <Bubble
            size    = {200}
            left    = {'85%'}
            top     = {'50%'}
            zIndex  = {9}
          />
          <Bubble
            size    = {190}
            left    = {-100}
            top     = {-20}
            zIndex  = {10}
          />
          <Bubble
            size    = {80}
            left    = {'12%'}
            top     = {'15%'}
            zIndex  = {11}
          />
        </View>
        <Loader visible = {this.state.loader} />
      </FullScreenContainer>
    );
  }
}

export const LoginScreen = withNamespaces(['Login'])(LoginScreenComponent);
