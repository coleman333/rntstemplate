import { bindActionCreators, Dispatch } from 'redux';

import { reduxify } from 'src/utils/reduxify';
import { socialLoginAction, getProfile, getSocialIdsAction } from "src/actions/Auth";
import { clearScanParams } from "src/actions/Scan";

const mapStateToProps = (state: any) => ({
  auth: state.auth,
  scan: state.scan,
});


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    login:              bindActionCreators(socialLoginAction,     dispatch),
    getProfile:         bindActionCreators(getProfile,            dispatch),
    getSocialIdsAction: bindActionCreators(getSocialIdsAction,    dispatch),
    clearScanParams:    bindActionCreators(clearScanParams,       dispatch),
  };
};

export const connect = reduxify(mapStateToProps, mapDispatchToProps);
