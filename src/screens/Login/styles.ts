import { StyleSheet } from 'react-native';
import {COLORS} from "../../constants";

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'flex-start',
    backgroundColor: COLORS.gray,
  },

  contentArea: {
    width: '100%',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonContainer: {
    margin: 10,
    width: 150,
    height: 31,
    borderRadius: 15,
    backgroundColor: COLORS.white,
    shadowOffset:{  width: 1,  height: 1 },
    shadowColor: COLORS.black,
    shadowOpacity: 0.7,
    elevation: 3,
  },

  buttonText: {
    color: COLORS.navy,
    fontSize: 16,
    fontWeight: '500',
  },

  darkTopContent: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: '30%',
  },

  labelText: {
    fontSize: 16,
    color: COLORS.navy,
    fontWeight: '500',
  },

  row: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  iconFB: {
    width: 20,
    height: 20,
    marginRight: 5,
  },

  iconGoogle: {
    width: 20,
    height: 20,
  },

  height50: {
    height: '50%',
  },

});
