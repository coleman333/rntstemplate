import React from 'react';
import { withNamespaces } from 'react-i18next';
import { Image, View, Text} from 'react-native';
import { Button } from 'react-native-material-ui';
import NativeSplashScreen from 'react-native-splash-screen';

import { FullScreenContainer } from 'src/components/full-screen-contaner';
import { DarkTop } from 'src/components/dark-top';
import { Bubble } from "src/components/bubble";
import { Navigation } from 'src/services/Navigation';
import { Routes } from 'src/navigation/route-names';
import { Props } from './types';
import { styles } from './styles';

const LOGO = require('./assets/drnk.png');

class WelcomeScreenComponent extends React.PureComponent<Props> {
  // Action handlers

  private hideSplashScreen = (): void => {
    NativeSplashScreen.hide();
  };

  private redirectToLogin = (): void => {
    Navigation.navigateTo(Routes.Login);
  };

  private redirectToHowItWorks = (): void => {
    Navigation.navigateTo(Routes.HowItWorks);
  };

  private redirectToScan = (): void => {
    Navigation.navigateTo(Routes.ScanUnAuth);
  };

  // RENDER

  public render(): React.ReactNode {
    const { navigation, t } = this.props;

    return (
      <FullScreenContainer style={styles.container}>
        <DarkTop
          animate={navigation.getParam('animate', false)}
          beforeAnimate={this.hideSplashScreen}
        >
          <Image source={LOGO} />
        </DarkTop>
        <View style={styles.contentArea}>
          <Text
            style={styles.welcomeText}
          >{t('hey')}</Text>
          <Button
            style={{ container: [styles.buttonContainer, styles.buttonContainerPink], text: styles.buttonText }}
            upperCase={false}
            text={t('Login')}
            onPress={this.redirectToLogin}
          />
          <Button
            style={{ container: styles.buttonContainer, text: styles.buttonText }}
            upperCase={false}
            text={t('Scan')}
            onPress={this.redirectToScan}
          />
          <Button
            style={{ container: styles.buttonContainer, text: styles.buttonText }}
            upperCase={false}
            text={t('How it works')}
            onPress={this.redirectToHowItWorks}
          />
          <Bubble
            size    = {200}
            left    = {'85%'}
            top     = {-20}
            zIndex  = {9}
          />
          <Bubble
            size    = {190}
            left    = {-100}
            top     = {'50%'}
            zIndex  = {10}
          />
          <Bubble
            size    = {80}
            left    = {'12%'}
            top     = {'72%'}
            zIndex  = {11}
          />
        </View>
      </FullScreenContainer>
    );
  }
}

export const WelcomeScreen = withNamespaces(['WelcomeScreen', 'buttons'])(WelcomeScreenComponent);
