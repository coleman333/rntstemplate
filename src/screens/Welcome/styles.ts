import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    backgroundColor: COLORS.gray,
  },

  topArea: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.navy,
  },

  welcomeText: {
    color: COLORS.navy,
    fontWeight: '600',
    fontSize: 20,
    marginBottom: 35,
  },

  svgArea: {
    width: '100%',
    height: 45,
  },

  contentArea: {
    width: '100%',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonContainer: {
    margin: 10,
    width: 150,
    height: 31,
    borderRadius: 15,
    backgroundColor: COLORS.white,
    shadowOffset:{  width: 1,  height: 1 },
    shadowColor: COLORS.black,
    shadowOpacity: 0.7,
    elevation: 5,

  },

  buttonContainerPink: {
    backgroundColor: COLORS.lightMauve,
  },

  buttonText: {
    color: COLORS.navy,
  },
});
