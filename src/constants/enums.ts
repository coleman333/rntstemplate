export enum SocialPlatform {
  OTHER = 'OTHER',
  FACEBOOK = 'FACEBOOK',
  TWITTER = 'TWITTER',
  TELEGRAM = 'TELEGRAM',
  INSTAGRAM = 'INSTAGRAM',
}

export enum SocialLoginPlatform {
  UNSPECIFIED = 'UNSPECIFIED',
  FACEBOOK = 'FACEBOOK',
  GOOGLE = 'GOOGLE',
}

export enum SocialActionType {
  UNKNOWN = 'UNKNOWN',
  LIKE = 'LIKE',
  SHARE = 'SHARE',
  FOLLOW = 'FOLLOW',
  RETWEET = 'RETWEET',
  JOIN_GROUP = 'JOIN_GROUP',
}

export enum UserSocialBountyStatus {
  DEFAULT = 'DEFAULT',
  AVAILABLE = 'AVAILABLE',
  CLAIMED = 'CLAIMED',
  CLICKED = 'CLICKED',
  CHECKING = 'CHECKING',
  CONFIRMED = 'CONFIRMED',
  FAILED = 'FAILED',
}

export enum DocumentType {
  DOCUMENT = 'DOCUMENT',
  FINANCIAL = 'FINANCIAL',
  LICENSE = 'LICENSE',
}

export enum LocationType {
  UNKNOWN = 'UNKNOWN',
  LOCAL = 'LOCAL',
  REGIONAL = 'REGIONAL',
  GLOBAL = 'GLOBAL',
}

export enum BusinessType {
  NONE = 'NONE',
}

export enum OrganizationImageType {
  UNSPECIFIED = 'UNSPECIFIED',
  LOGO = 'LOGO',
  OTHER = 'OTHER',
}

export enum OutletType {
  UNDISCLOSED = 'UNDISCLOSED',
  ONLINE = 'ONLINE',
  PHYSICAL_STORE = 'PHYSICAL_STORE',
  RESTAURANT_OR_BAR = 'RESTAURANT_OR_BAR',
}

export enum RoleType {
  EMPTY = 'EMPTY',
  PRODUCER = 'PRODUCER',
  RETAILER = 'RETAILER',
  IMPORTER = 'IMPORTER',
  DISTRIBUTOR = 'DISTRIBUTOR',
  AGENT_BROKER = 'AGENT_BROKER',
  RESTAURANT_BAR = 'RESTAURANT_BAR',
  OMNIAZ = 'RESTAURANT_BAR',
}

export enum Namespace {
  UNKNOWN = 'UNKNOWN',
  NHN0 = 'NHN0',
  NHQ0 = 'NHQ0',
}

export enum OwnerType {
  UNSPECIFIED = 'UNSPECIFIED',
  PRODUCER = 'PRODUCER',
  RETAILER = 'RETAILER',
}

export enum ProductImageType {
  OTHER = 'OTHER',
  FRONT = 'FRONT',
  BACK = 'BACK',
  LABEL = 'LABEL',
}

export enum ReviewerType {
  DEFAULT = 'DEFAULT',
  REVIEWER_USER = 'REVIEWER_USER',
  REVIEWER_ORGANIZATION = 'REVIEWER_ORGANIZATION',
}

export enum RevieweeType {
  UNKNOWN = 'UNKNOWN',
  REVIEWEE_PRODUCT = 'REVIEWEE_PRODUCT',
  REVIEWEE_ORGANIZATION = 'REVIEWEE_ORGANIZATION',
  REVIEWEE_USER = 'REVIEWEE_USER',
}

export enum CouponStatus {
  UNKNOWN = 'UNKNOWN',
  AVAILABLE = 'AVAILABLE',
  CLAIMED = 'CLAIMED',
}

export enum CouponType {
  DEFAULT = 'DEFAULT',
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
}

export enum Reason {
  UNKNOWN = 'UNKNOWN',
  BOUNTY = 'BOUNTY',
  CAMPAIGN = 'CAMPAIGN',
}

export enum Gender {
  UNKNOWN = 'UNKNOWN',
  MALE = 'MALE',
  FEMALE = 'FEMALE',
}

export enum Currency {
  UNSPECIFIED = 'UNSPECIFIED',
  USD = 'USD',
}

export enum DevicePlatform {
  NONE = "NONE",
  WEB = "WEB",
  IOS = "IOS",
  ANDROID = "ANDROID",
}