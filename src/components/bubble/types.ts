import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  size: number;
  left: number | string;
  top: number | string;
  color?: string;
  zIndex: number;
}

export interface BubbleStyles {
  width: number;
  height: number;
  left: number | string;
  top: number | string;
  borderRadius: number;
  backgroundColor: string;
  zIndex: number;
}
