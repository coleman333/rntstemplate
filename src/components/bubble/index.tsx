import React from 'react';
import { View } from 'react-native';
import { Props, BubbleStyles } from './types';
import { styles } from './styles';
import { COLORS } from "../../constants";

export class Bubble extends React.Component<Props> {

  public static defaultProps: Partial<Props> = {
    size: 10,
    left: 0,
    top: 0,
    color: COLORS.lightMauve,
    zIndex: 10,
  };

  private applySettings = (): BubbleStyles => {
    return {
      width: this.props.size,
      height: this.props.size,
      left: this.props.left,
      top: this.props.top,
      borderRadius: (this.props.size / 2),
      backgroundColor: this.props.color,
      zIndex: this.props.zIndex
    };
  };

  public render(): React.ReactNode {
    const settings: BubbleStyles = this.applySettings();
    return (
      <View style = {[styles.container, settings]} />
    );
  }

}