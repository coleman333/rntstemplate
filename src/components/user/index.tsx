import React from 'react';
import {Text, TextStyle, View, Image, ImageStyle} from 'react-native';
import { Icon } from 'react-native-material-ui';
import { Props } from './types';
import { styles } from './styles';

export class User extends React.Component<Props> {

  public static defaultProps: Partial<Props> = {
    image:  '',
    name:   '',
    email:  '',
    netTab: false,
    location: '',
    dateSince: '',
  };


  public render(): React.ReactNode {
    const {netTab, email, name, location, dateSince} = this.props;

    return (
      <View style={{
        width: '100%',
        paddingLeft: 16,
        paddingTop: 25,
        flexDirection: netTab ? 'row' : 'column'
      }}>
        <View
          style={{
            justifyContent: 'space-between',
          }}
        >
          {this.props.image !== '' ?
          <Image
            source = {{uri: this.props.image}}
            style = {netTab ? styles.bigImage as ImageStyle : styles.image as ImageStyle}
            resizeMode = {'contain'}
          /> :
          <Icon name="person" 
            style={netTab ? styles.bigImage : styles.image} 
            size={netTab ? 116 : 58}/>
          }
        </View>
        <View style={netTab ? styles.row : null}>
          <Text style = {styles.nameText as TextStyle}>{this.props.name}</Text>
          <Text style = {styles.emailText as TextStyle}>{ netTab ? location : email}</Text>
          {netTab ? <Text style = {styles.emailText as TextStyle}>{`DRNKer since ${dateSince}`}</Text> 
            : null}
        </View>
      </View>
    );
  }

}
