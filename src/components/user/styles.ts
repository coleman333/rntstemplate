import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  row: {
    paddingLeft: 16,
    paddingTop: 10,
    justifyContent: 'space-around',
  },

  nameText: {
    fontFamily: 'Montserrat',
    fontSize: 19,
    fontWeight: "500",
    color: COLORS.black,
    opacity: 0.9,
  },
  emailText: {
    fontFamily: 'Montserrat',
    fontSize: 14,
    fontWeight: "500",
    color: COLORS.black,
    opacity: 0.9,
  },

  image: {
    borderWidth: 1,
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },

  bigImage: {
    borderWidth: 1,
    width: 120,
    height: 120,
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
  }

});
