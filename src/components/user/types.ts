import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  image?: string;
  name: string;
  email: string;
  netTab: boolean;
  location: string;
  dateSince: string;
}
