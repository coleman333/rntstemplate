export interface Props {
  value: number;
  count?: number;
  size?: number;
  color?: string;
  margin?: number;
  backgroundColor?: string;
}
