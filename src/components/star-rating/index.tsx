import React from 'react';
import { Animated, Image, View, ViewStyle } from 'react-native';

import { createEmptyArray } from 'src/utils/createEmptyArray';
import { COLORS } from 'src/constants';
import { Props } from './types';

const STAR_IMAGE = require('./assets/star.png');

export class StarRating extends React.Component<Props> {
  public static defaultProps: Partial<Props> = {
    count: 5,
    size: 15,
    color: COLORS.lightMauve,
    margin: 5.2,
    backgroundColor: COLORS.white,
  };

  // Getters

  private getValue = (): number => {
    const { value, count } = this.props;
    switch (true) {
      case value > count:
        return count;
      case value < 0:
        return 0;
      default:
        return value;
    }
  };

  private getOverlayStyle = (): ViewStyle => {
    const { size, color, margin } = this.props;
    const value = this.getValue();

    return {
      width: value * size + Math.trunc(value) * margin,
      height: size,
      backgroundColor: color,
      position: 'absolute',
      top: 0,
      left: 0,
    };
  };

  // Render helpers

  private renderRatings = (): React.ReactNode => {
    const { size, count, margin, backgroundColor } = this.props;

    return createEmptyArray(count).map(
      (item: undefined, index: number): React.ReactNode => (
        <React.Fragment key={index}>
          {index ? <View style={{ width: margin, backgroundColor: backgroundColor }} /> : null}
          <Image source={STAR_IMAGE} style={{ width: size, height: size }} />
        </React.Fragment>
      ),
    );
  };

  // RENDER

  public render(): React.ReactNode {
    return (
      <View pointerEvents="none">
        <View style={{ flexDirection: 'row' }}>
          <Animated.View style={this.getOverlayStyle()} />
          {this.renderRatings()}
        </View>
      </View>
    );
  }
}
