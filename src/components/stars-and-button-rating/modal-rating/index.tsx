import React from 'react';
import { Modal, View, Text, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { Button } from 'react-native-material-ui';
import { Props, State } from './types';
import { styles } from './styles';
import { COLORS } from '../../../constants';
import StarRating from 'react-native-star-rating';

class ProductModalRating extends React.Component<Props, State> {

  public static defaultProps: Props = {
    visible: false,
    stars: 0,
    onRate: () => {},
    onClose: () => {},
    readonly:true,
    t: () => {},
  };


  public state: State = {
    stars: 0,
  };

  public constructor(props: Props) {
    super(props);
    this.state.stars =props.stars;
  }

  public componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any): void {
    if (this.props.visible && !prevProps.visible) {
      this.setState({ stars: this.props.stars });
    }
  }

  private onSelectedStar = ( stars: number): void => {
    this.setState({ stars });
  };

  public render(): React.ReactNode {
    let { onRate, onClose, visible, t} = this.props;
    let { stars } = this.state;

    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={onClose}
      >
        <TouchableOpacity
          activeOpacity={1.0}
          onPress={onClose}
          style={{ backgroundColor: 'rgba(0,0,0,.8)', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.container}>
              <TouchableWithoutFeedback>
                <View style={styles.window}>
                  <View style={styles.titleArea}>
                    <Text style={styles.titleModalText}>{ t('Product:stars_button_text:modalText') }</Text>
                  </View>
                  <View style={styles.starsContainer}>
                    <StarRating
                      maxStars={5}
                      rating={stars}
                      emptyStar={'ios-star-outline'}
                      fullStar={'ios-star'}
                      halfStar={'ios-star-half'}
                      iconSet={'Ionicons'}
                      fullStarColor={COLORS.lightMauve}
                      emptyStarColor={COLORS.lightMauve}
                      starSize={40}
                      starStyle={{ marginRight: 3 }}
                      selectedStar={this.onSelectedStar}
                    />
                  </View>

                  <View style={styles.buttonArea }>
                    <Button
                      style={{ container: styles.buttonContainer, text: styles.buttonText }}
                      upperCase={false}
                      text={t('Product:stars_button_text:buttonLabel')}
                      onPress={onRate.bind(null, stars)}
                    />
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
        </TouchableOpacity>
      </Modal>
    );
  }

}

export default ProductModalRating;
