import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    marginBottom:'20%',
    justifyContent: 'center',
  },

  window: {
    width: '70%',
    height: '30%',
    backgroundColor: COLORS.navy,
    borderRadius: 15,
  },

  buttonContainer: {
    margin: 10,
    marginTop:5,
    width: "43%",
    height:'100%',
    borderRadius: 5,
    backgroundColor: COLORS.white,
  },

  buttonText: {
    color: COLORS.navy,
    fontFamily: "Poppins"
  },

  titleModalText: {
    textAlign: 'center',
    color: COLORS.white,
    fontSize: 14,
    fontFamily: 'Poppins',
    fontWeight: '500',
    fontStyle: 'normal',
  },

  bodyText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 14,
    marginBottom: 15,
  },

  emailText: {
    textAlign: 'center',
    color: COLORS.mauve,
    fontWeight: '500',
    fontSize: 22,
  },

  titleArea: {
    lineHeight: 2,
    width: '100%',
    height: '35%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },

  bodyArea: {
    width: '100%',
    height: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonArea: {
    marginTop:'8%',
    width: '100%',
    height: '23%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  starsContainer:{
    width:'80%',
    marginLeft:'12%',
    justifyContent:'center',
  }
});
