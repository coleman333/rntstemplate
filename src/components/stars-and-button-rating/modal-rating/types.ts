import { TranslationFunction } from 'i18next';

export interface Props {
  visible: boolean;
  stars: number;
  onRate?(stars:number): void;
  onClose?(): void;
  readonly:boolean;
  t: TranslationFunction;
}

export interface State {
  stars: number;
}
