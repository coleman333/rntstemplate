import { TranslationFunction } from 'i18next';

export interface Props {
  stars: number;
  userRating: number;
  onClose? (): void;
  onRate?(stars:number): void;
  readonly:boolean,
  labelButton: string,
  modalText: string
  t: TranslationFunction;
}

export interface State {
  stars: number;
  displayRatingModal: boolean
}
