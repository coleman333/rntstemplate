import React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-material-ui';
import { Props, State } from './types';
import { styles } from './styles';
import { COLORS } from '../../constants';
import StarRating from 'react-native-star-rating';
import ProductModalRating from './modal-rating';

class StarsAndButtonRating extends React.Component<Props, State> {

  public static defaultProps: Props = {
    stars: 0,
    userRating: 0,
    onClose: () => {},
    onRate: () => {},
    readonly:false,
    labelButton: '',
    modalText: '',
    t: () => {}
  };


  public state: State = {
    stars: 0,
    displayRatingModal:false
  };

  private closeRatingDialog = (): void => {
    this.setState({ displayRatingModal: false });
  };

  public showRatingDialog =(): void  => {
    this.setState({ displayRatingModal: true });
  };


  public render(): React.ReactNode {
    let { onRate , stars , t } = this.props;

    return (
      <View style   = {styles.ratingHolder} >
        <View style={{flexDirection:'row'}}>
          <Text style = {styles.ratingText}>{stars.toFixed(2)}</Text>
          <View style={styles.buttonArea}>
            {this.props.readonly === false && <Button
              style={{ container: styles.buttonContainer, text: styles.buttonText }}
              upperCase={false}
              text={t('Product:stars_button_text:buttonLabel')}
              onPress={this.showRatingDialog.bind(this)}
            />}
          </View>
        </View>
        <View style={styles.starsContainer}>
          <StarRating
            maxStars  = {5}
            rating    = { stars }
            emptyStar = {'ios-star-outline'}
            fullStar  = {'ios-star'}
            halfStar  = {'ios-star-half'}
            iconSet   = {'Ionicons'}
            fullStarColor  = {COLORS.lightMauve}
            emptyStarColor = {COLORS.lightMauve}
            starSize  = {25}
            starStyle = {{marginRight: 3}}
            selectedStar={this.showRatingDialog}
          />
        </View>
        {this.props.readonly === false && this.state.displayRatingModal === true && <ProductModalRating
          t = {t}
          visible={true}
          stars={this.props.userRating}
          onClose={this.closeRatingDialog.bind(this)}
          onRate={(rating) => { this.closeRatingDialog(); onRate(rating); }}
       />
     }
      </View>
    );
  }

}

export default StarsAndButtonRating ;

