import React from 'react';
import {Text, TextStyle, View, Image, ImageStyle} from 'react-native';
import { Icon } from 'react-native-material-ui';
import { Props, listPart } from './types';
import { styles } from './styles';
import value from '*.json';

export class ScansTable extends React.Component<Props> {

  public static defaultProps: Partial<Props> = {
    tabs: [
    {
      title: 'NFC Scans',
      value: 0,
    } as listPart,
    {
      title: 'QR Scans',
      value: 0,
    } as listPart,
    {
      title: 'Credits',
      value: 0,
    } as listPart,
  ]
};


  public render(): React.ReactNode {

    const {tabs} = this.props;

    return (
      <View style={{
        width: '100%',
        padding: 16,
      }}>
      <View
        style={{
          position: 'relative',
        }}
      >
        <View style={{
          backgroundColor: '#416c91',
          height: 25,
          borderTopStartRadius: 5,
          borderTopEndRadius: 5,
        }} />
        <View style={{
          backgroundColor: '#e7e7e7',
          height: 35,
          borderBottomStartRadius: 5,
          borderBottomEndRadius: 5,
        }} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            position: 'absolute',
            top: 0,
            right: 0,
          }}
        >
          {tabs.map((v,i) => <View
              key={`scans-table-part-${i}`}
              style={{
                marginTop: 5,
                flex: 1,
                alignItems: 'center',
              }}
            >
              <Text style={{
                color: 'white',
              }}>{v.title}</Text>
              <Text style={{
                marginTop: 10,
                backgroundColor: '',
                color: '#00253c',
                margin: 0,
                fontWeight: '800',
              }}>{v.value}</Text>
            </View>)}
          </View>
        </View>
      </View>
    );
  }
}
