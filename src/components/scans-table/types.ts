import { ViewProps } from 'react-native';

export type listPart = {
  title: string;
  value: number;
}

export interface Props extends ViewProps {
  tabs: Array<listPart>;
}
