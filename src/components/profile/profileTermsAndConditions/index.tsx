import React from 'react';
import { View, Text, TouchableOpacity, Linking } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { Props, State } from './types';
import { styles } from './styles';

export class ProfileTermsAndConditions extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
  };

  public render(): React.ReactNode {
    let { t} = this.props;

    return (
      <View style={styles.termsAndPolicyContainer}>
        <TouchableOpacity onPress={() => Linking.openURL('https://omniaz.io/terms.html')}>
          <Text style={styles.textLinkStyle}>
            {t('Profile:Terms')}
          </Text>
        </TouchableOpacity>
        <Text style={styles.textStyle}>{t('Profile:conjunction')}</Text>
        <TouchableOpacity onPress={() => Linking.openURL('https://omniaz.io/privacy.html')}>
          <Text style={styles.textLinkStyle}>
            {t('Profile:Policy')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

}
export default ProfileTermsAndConditions;
