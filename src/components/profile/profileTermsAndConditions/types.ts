import { ViewProps } from 'react-native';
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  fields: any;
  t:TranslationFunction;
}

export interface State {
  country?: any
}
