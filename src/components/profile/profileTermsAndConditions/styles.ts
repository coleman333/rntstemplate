import { StyleSheet } from 'react-native';
import { COLORS } from '../../../constants';
export const styles = StyleSheet.create({

  container: {
    width: '100%',
  },

  nameGroupInputContainer: {
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    height: 35,
  },

  nameGroupInput: {
    backgroundColor: COLORS.white,
    color: COLORS.navy,
    width: '43%',
    borderColor: '#cbcbcb',
    height: 38,
    borderWidth: 1,
    borderRadius: 3,
    justifyContent: 'center',
  },

  nameGroupLabelContainer: {
    marginTop: '2.5%',
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    height: 25,
  },

  nameGroupLabel: {
    color: COLORS.navy,
    width: '43%',
    justifyContent: 'center',
  },
  emailContainer:{
    marginTop: '2%',
    width: '100%',

  },

  formGroupInput: {
    marginTop: '2%',
    backgroundColor: COLORS.white,
    color: COLORS.navy,
    width: '100%',
    borderColor: '#cbcbcb',
    height: 38,
    borderWidth: 1,
    justifyContent: 'center',
    borderRadius: 3,
  },

  selectIcon:{
    width:25,
    height:25,
    marginLeft: '-8%',
    marginTop: '2%'
  },

  termsAndPolicyContainer:{
    height:'100%',
    flexDirection:'row',
    justifyContent: 'center',
    alignItems:'center',
  },

  textLinkStyle:{
    fontFamily: 'Poppins',
    color: COLORS.black,
    opacity: 0.49,
    textDecorationLine:'underline'
  },

  textStyle:{
    fontFamily: 'Poppins',
    color: COLORS.black,
    opacity: 0.49,
  }
});
