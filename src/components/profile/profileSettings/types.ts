import { ViewProps } from 'react-native';
import { IUserData } from '../form/types';
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  user: any;
  t:TranslationFunction;
  onLogout(): void;
  onDelete(): void;
  onSave(data: IUserData): void;
  openDeleteModal(): void;
}

export interface State {
  [key:string]: any,

  hideButton:boolean;
  opened:boolean;
}
