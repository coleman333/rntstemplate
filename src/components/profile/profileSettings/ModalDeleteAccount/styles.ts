import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    marginBottom:'15%',
    justifyContent: 'center',
  },

  window: {
    width: '85%',
    height: '60%',
    backgroundColor: COLORS.white,
    borderRadius: 10,
  },

  deleteButtonContainer: {
    width: "72%",
    height: 36,
    borderRadius: 5,
    backgroundColor: COLORS.gray,
  },

  keepButtonContainer: {
    width: "72%",
    height: 36,
    borderRadius: 5,
    backgroundColor: COLORS.mauve,
    elevation:2,
  },

  buttonText: {
    color: COLORS.white,
    fontFamily: 'Poppins',
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 1.3,
    textAlign: 'center',
  },

  deleteButtonText: {
    color: COLORS.navy,
    fontFamily: 'Poppins',
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 1.3,
    textAlign: 'center',
  },

  titleText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontWeight: 'bold',
    fontSize: 23,
    letterSpacing:2,
  },

  titleArea: {
    marginTop: '10%',
    width: '100%',
    height: '10%',
  },

  secondTitleText: {
    fontFamily: 'Poppins',
    fontSize: 14,
    letterSpacing: 0.9,
    textAlign: 'center',
    color: COLORS.navy,
  },

  secondBlockTitleArea: {
    marginTop: '2%',
    width: '100%',
    height: '35%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },

  bodyArea: {
    width: '100%',
    height: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonArea: {
    marginTop:'9%',
    width: '100%',
    height: '25%',
    alignItems: 'center',
    justifyContent: 'center'
  },

  buttonMargin:{
    marginTop:"3%",
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }

});
