import React from 'react';
import { Modal, View, Text, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { Button } from 'react-native-material-ui';
import { Props, State } from './types';
import { styles } from './styles';

class ModalDeleteAccount extends React.Component<Props, State> {

  public static defaultProps: Props = {
    visible: false,
    stars: 0,
    onDelete: () => {
    },
    onClose: () => {
    },
    readonly: true,
    t: () => {
    },
  };


  public state: State = {
    stars: 0,
  };

  public constructor(props: Props) {
    super(props);
    this.state.stars = props.stars;
  }

  public componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any): void {
    if (this.props.visible && !prevProps.visible) {
      this.setState({ stars: this.props.stars });
    }
  }

  public render(): React.ReactNode {
    let { onDelete, onClose, visible, t } = this.props;

    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={onClose}
      >
        <TouchableOpacity
          activeOpacity={1.0}
          onPress={onClose}
          style={{ backgroundColor: 'rgba(0,0,0,.8)', flex: 1, alignItems: 'center', justifyContent: 'center' }}>

          <View style={styles.container}>
            <TouchableWithoutFeedback>
              <View style={styles.window}>
                <View style={styles.titleArea}>
                  <Text style={styles.titleText}>{t('Profile:deleteModal:title')}</Text>
                </View>
                <View style={styles.secondBlockTitleArea}>
                  <Text style={styles.secondTitleText}>{t('Profile:deleteModal:text')}</Text>
                </View>
                <View style={styles.buttonArea}>
                  <Button
                    style={{ container: styles.deleteButtonContainer, text: styles.buttonText }}
                    upperCase={false}
                    text={t('Profile:deleteModal:deleteButtonText')}
                    onPress={onDelete}
                  />
                  <View style={styles.buttonMargin}>
                    <Button
                      style={{ container: styles.keepButtonContainer, text: styles.deleteButtonText }}
                      upperCase={false}
                      text={t('Profile:deleteModal:keepButtonText')}
                      onPress={onClose}
                    />
                  </View>
                </View>

              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }

}

export default ModalDeleteAccount;
