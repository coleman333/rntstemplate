
export interface Props {
  visible: boolean;
  stars: number;
  onDelete?(): void;
  onClose?(): void;
  readonly:boolean;
  t: Function;
}

export interface State {
  stars: number;
}
