import { StyleSheet } from 'react-native';
import {COLORS} from "../../../constants";

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    //height: '100%',
  },

  iconArea: {
    width: '100%',
    height: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  formArea: {
    //height: '50%',
    width: '100%',
    paddingVertical: 30,
    paddingHorizontal: 20,
  },

  buttonLogoutContainer: {
    marginRight: 10,
    width: '28%',
    borderRadius: 5,
    backgroundColor: '#cbcbcb',
  },

  buttonSaveContainer: {
    marginRight: 10,
    width: 100,
    borderRadius: 5,
    backgroundColor: COLORS.navy,
  },

  buttonLogoutText: {
    color: COLORS.navy,
    fontSize: 11,
  },

  buttonSaveText: {
    color: COLORS.mauve,
    fontSize: 11,
  },

  buttonDeleteContainer:{

  },

  buttonDeleteText:{
    fontFamily: 'Poppins',
    fontSize: 11,
    letterSpacing: 0.2,
    textAlign: 'center',
    color: '#a4a4a4'

  },

  buttonsArea: {
    width: '100%',
    //height: 50,
    justifyContent: 'space-between',
    //marginTop:'8%',
    flexDirection: 'row',
    paddingHorizontal: 20,
  },

  image: {
    borderWidth: 1,
    width: 90,
    height: 90,
    borderRadius: 45,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white',
    marginTop: -200
  },

  mainContainer:{
    width:'100%',
    height:'100%',
    backgroundColor:'#e3e3e3'
  },
  innerContainer:{
     flexDirection: 'column'

  },
  termsAndPolicyContainer:{
    // flex: 1,
    height:'6%',
    flexDirection:'row',
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor:'#e3e3e3'
  },
  textLinkStyle:{
    color: 'black',
    textDecorationLine:'underline'
  }
});
