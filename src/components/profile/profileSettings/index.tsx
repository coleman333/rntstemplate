import React from 'react';
import { View, ScrollView } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { Props, State } from './types';
import { styles } from './styles';
import { FullScreenContainer } from '../../full-screen-contaner';
import { ProfileForm } from '../form';
import ModalDeleteAccount from './ModalDeleteAccount';
import Header from '../profileSettingsHeader';
import ProfileTermsAndConditions from '../profileTermsAndConditions';
import { Button } from 'react-native-material-ui';

const selectIcon = require('../../../../assets/images/selectIcon.png');


/*          заготовка под компонент

  <ProfileSettings
    t = { t }
    user = {this.props.user}
    onLogout={this.props.logout}
    onDelete={this.props.delete}
    onSave={(userData)=>{console.log('on save',userData)} }
  />
*/



export class ProfileSettings extends React.Component<Props, State> {

  private form: ProfileForm;

  public static defaultProps: Partial<Props> = {
    user: {},
  };

  public state: State = {
    hideButton: false,
    opened:  false,
  };

  private toggleModal = () : void => {
    this.setState({opened : !this.state.opened} );
  };

  private onSave = () : void => {
    this.setState({opened : false} );
    let form = this.form.getValues();
    this.props.onSave(form);
  };

  private onLogout = () : void => {
    this.setState({opened : false} );
    this.props.onLogout();
  };

  private openDeleteModal = () : void => {
    this.setState({opened : true} );
  };

  public render() {
    const { t, onLogout, onDelete} = this.props;

    return (
        <View style={styles.container}>
          <View style={styles.formArea}>
            <ProfileForm user={this.props.user} t = { t } ref = { (c) => { this.form = c } }/>
          </View>
          <View style={styles.buttonsArea}>
            {false &&
            <Button
              style={{ container: styles.buttonSaveContainer, text: styles.buttonSaveText }}
              upperCase={true}
              text={t('buttons.save')}
              onPress = {this.onSave}
            />
            }
            <Button
              style={{ container: styles.buttonLogoutContainer, text: styles.buttonLogoutText }}
              upperCase={true}
              text={t('buttons.logout')}
              onPress = {this.onLogout}
            />
            <Button
              style={{ container: styles.buttonDeleteContainer, text: styles.buttonDeleteText }}
              text={t('buttons.delete')}
              onPress={this.openDeleteModal}
              upperCase={true}
            />

            <ModalDeleteAccount
              t={t}
              visible={this.state.opened}
              onClose={this.toggleModal}
              onDelete={onDelete}
            />

          </View>
        </View>
    );
  }
}

export default ProfileSettings;
