import { StyleSheet } from 'react-native';
import { COLORS } from '../../../constants';
export const styles = StyleSheet.create({

  container: {
    width: '100%',
  },

  innerMasterContainer:{
    height: 300
  },

  innerSlaveContainer:{
    backgroundColor:COLORS.navy,
    borderRadius: 352,
    overflow:'hidden',
    marginTop: -500,
    marginLeft: '-36%',
    height:730,
    width :700
  },

  headerContainer:{
    alignItems:'center',
    marginTop: 600 ,
    flexDirection:'row',
    marginLeft: '25%'
    // marginLeft: 300
  },

  profileIcon:{
    marginTop: -130,
    height: 109 ,
    width: 109 ,
    marginLeft: '35%'
  },

  styleForImage: {
    height: 20 ,
    width: 12,

  },

  headerTextStyle: {
    color: COLORS.mauve,
    textAlign: 'center',
    marginLeft: '15%',
    fontFamily: 'Poppins',
    fontSize: 17,
    fontWeight: '600'
  }

});
