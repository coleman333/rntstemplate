import React from 'react';
import { View, Text, Image, TouchableOpacity ,ImageStyle } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { Props, State } from './types';
import { styles } from './styles';
const profileIcon = require('../../../../assets/images/profileIcon.png');
const headerBackIcon = require('../../../../assets/images/headerBackIcon.png');

export class Header extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    onBackPress:Function
  };

  private onBackPress = (): void => {
    /*заготовка для навигации*/
    console.log('profile settings header')
  };

  public render(): React.ReactNode {
    let { t } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.innerMasterContainer}>
          <View style={styles.innerSlaveContainer}>
            <View style={styles.headerContainer}>
              <TouchableOpacity onPress={this.onBackPress}>
                <Image style={styles.styleForImage as ImageStyle  } source={headerBackIcon} />
              </TouchableOpacity>
              <Text style={ styles.headerTextStyle }>{t('Profile:form:headerTitle')} </Text>
            </View>
          </View>
        </View>
        <Image source={profileIcon} style={styles.profileIcon as ImageStyle}/>
      </View>
    );
  }

}
export default Header;
