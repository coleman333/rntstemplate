import { ViewProps } from 'react-native';
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  t:TranslationFunction;
  onBackPress:Function
}

export interface State {
  country?: any
}
