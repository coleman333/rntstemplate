import { ViewProps } from 'react-native';
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  t: TranslationFunction;
  user: IUserData;
}

export interface State {
  user: IUserData;
}

export interface IUserData {
  [key:string]: any,
  country: string;
  first_name: string;
  last_name: string;
  email: string;
  birth_date: number;
}



export class UserData implements IUserData{

  country: string;
  first_name: string;
  last_name: string;
  email: string;
  birth_date: number;

  constructor(country: string, first_name: string, last_name: string, email: string, birth_date:number) {
    this.birth_date = birth_date;
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.country = country;
  }

}
