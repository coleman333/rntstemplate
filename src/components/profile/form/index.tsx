import React from 'react';
import { View, TextInput, Text, Picker, Image, ImageStyle } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import moment from 'moment';
import { Props, State, IUserData, UserData} from './types';
import { styles } from './styles';


const selectIcon = require('../../../../assets/images/selectIcon.png');

export class ProfileForm extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {

  };


  static getDerivedStateFromProps(props: Props, state: State): State {
    if (typeof props.user !== 'undefined' && JSON.stringify(props.user) !== JSON.stringify( state.user )) {
      state.user = props.user;
    }
    return state;
  }

  public state: State = {
    user: new UserData('', '','', '', null)
  };

  private updateField = (fieldName: string, value: string): void => {
    let user = this.state.user;
    user[fieldName] = value;
    this.setState({user});
  };

  public getValues = (): IUserData =>{
    return this.state.user;
  };



  public render() {
    let { t } = this.props;
    // let fields = this.state.fields;
    let countries = ['Singapore', 'China', 'Usa', 'Russia', 'Ukraine', 'England'];
    return (
      <View style={styles.container}>
        <View style={styles.nameGroupLabelContainer}>
          <Text style={styles.nameGroupLabel}>{t('Profile:form:first_name')}</Text>
          <Text style={styles.nameGroupLabel}>{t('Profile:form:last_name')}</Text>
        </View>
        <View style={styles.nameGroupInputContainer}>
          <TextInput
            style={styles.nameGroupInput}
            onChangeText= {(text: string)=>{ this.updateField('first_name', text) } }
            editable={false}
            value={this.state.user.first_name}
          />
          <TextInput
            style={styles.nameGroupInput}
            onChangeText= {(text: string)=>{ this.updateField('last_name', text) } }
            editable={false}
            value={this.state.user.last_name}
          />
        </View>
        <View style={styles.emailContainer}>
          <Text style={styles.nameGroupLabel}>{t('Profile:form:email')}</Text>
          <TextInput
            style={styles.formGroupInput}
            onChangeText= {(text: string)=>{ this.updateField('email', text) } }
            editable={false}
            value={this.state.user.email}
          />
        </View>
        <View style={styles.emailContainer}>
          <Text style={styles.nameGroupLabel}>{t('Profile:form:birth_date')}</Text>
          <TextInput
            style={styles.formGroupInput}
            onChangeText= {(text: string)=>{ this.updateField('birth_date', text) } }
            editable={false}
            value={moment.unix(this.state.user.birth_date).format("MM/DD/YYYY")}
          />
        </View>
        <View>
          <Text style={styles.nameGroupLabel}>{t('Profile:form:country')}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Picker
              selectedValue = { this.state.user.country }
              style={styles.formGroupInput}
              onValueChange={(item, index) => this.updateField('country', item)}
            >
              {countries.map((item: any, index: number) => {
                return <Picker.Item label={item} value={item} key={index}/>;
              })}
            </Picker>
            <Image style={styles.selectIcon as ImageStyle} source={selectIcon}/>
          </View>
        </View>
      </View>
    );
  }
}
