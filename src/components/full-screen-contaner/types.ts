import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  align?: 'center' | 'top' | 'bottom';
}
