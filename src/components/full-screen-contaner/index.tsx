import React from 'react';
import { View } from 'react-native';

import { Props } from './types';
import { styles } from './styles';

export const FullScreenContainer: React.SFC<Props> = ({ align, children, style, ...props }) => (
  <View style={[styles.container, styles[align], style]} {...props}>
    {children}
  </View>
);

FullScreenContainer.defaultProps = {
  align: 'center',
  style: {},
};
