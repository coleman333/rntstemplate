import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({
  wrapper: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: COLORS.lightBorderColor,
    backgroundColor: COLORS.white,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },

  visibleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 71,
  },

  logo: {
    width: 36.4,
    height: 42,
  },

  amount: {
    fontSize: 14.1,
    fontFamily: getFontFamily(),
    lineHeight: 20,
    letterSpacing: 0.3,
    color: COLORS.navy,
  },

  text: {
    fontSize: 14.1,
    fontFamily: getFontFamily(),
    letterSpacing: 0.3,
    color: COLORS.navy,
  },

  chevron: {
    padding: 7.5,
  },

  hiddenContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 180,
  },

  input: {
    width: '100%',
    height: 56,
    borderRadius: 3,
    borderWidth: 2,
    borderColor: COLORS.navy,
    marginTop: 4,
    marginBottom: 8,
    padding: 16,
  },

  button: {
    borderRadius: 3,
    paddingHorizontal: 16,
    paddingVertical: 10,
    backgroundColor: COLORS.navy,
  },

  buttonText: {
    fontSize: 14,
    fontFamily: getFontFamily('medium'),
    lineHeight: 16,
    letterSpacing: 1.3,
    color: COLORS.white,
  },
});
