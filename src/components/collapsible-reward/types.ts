import { StyleProp, ViewStyle } from 'react-native';
import { WithTranslation } from 'src/types/react';

export interface Props extends WithTranslation {
  title: string;
  reference?: string;
  amount: number;
  steps: string[];
  style?: StyleProp<ViewStyle>;
  animationDuration?: number;
  onVerify(value: string): void;
  onExpandStart?(topOffset: number): void;
  onExpandEnd?(topOffset: number): void;
  onCollapseStart?(topOffset: number): void;
  onCollapseEnd?(topOffset: number): void;
}

export interface State {
  collapsed: boolean;
  value: string;
}
