import React from 'react';
import Icon from 'react-native-vector-icons/Entypo';
import Collapsible from 'react-native-collapsible';
import { withNamespaces } from 'react-i18next';
import {
  Image,
  ImageStyle,
  LayoutChangeEvent,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';

import { COLORS } from 'src/constants';
import { Props, State } from './types';
import { styles } from './styles';

const OMZ_LOGO = require('../../../assets/images/omz.png');

class CollapsibleRewardComponent extends React.PureComponent<Props, State> {
  public static defaultProps: Partial<Props> = {
    reference: '@OmniazOfficial',
    animationDuration: 300,
    onExpandStart: () => {},
    onExpandEnd: () => {},
    onCollapseStart: () => {},
    onCollapseEnd: () => {},
  };

  public state: State = {
    collapsed: true,
    value: '',
  };

  // Component variables

  private topOffset: number = 0;

  // Component lifecycle methods

  public componentDidUpdate(prevProps: Props, prevState: State): void {
    const { onExpandStart, onCollapseStart } = this.props;
    const { collapsed } = this.state;

    if (prevState.collapsed && !collapsed) {
      onExpandStart(this.topOffset);
    }

    if (!prevState.collapsed && collapsed) {
      onCollapseStart(this.topOffset);
    }
  }

  // Event handlers

  private onLayout = (event: LayoutChangeEvent): void => {
    this.topOffset = event.nativeEvent.layout.y;
  };

  private onValueChange = (value: string): void => {
    this.setState({ value });
  };

  private onVerifyClick = (): void => {
    const { value } = this.state;
    if (value) {
      this.props.onVerify(value);
    }
  };

  private onAnimationEnd = (): void => {
    const { onCollapseEnd, onExpandEnd } = this.props;
    this.state.collapsed ? onCollapseEnd(this.topOffset) : onExpandEnd(this.topOffset);
  };

  // Action handlers

  private toggleCollapse = (): void => {
    this.setState((prevState: State) => ({ collapsed: !prevState.collapsed }));
  };

  // RENDER

  public render(): React.ReactNode {
    const { title, reference, amount, steps, t, style, animationDuration } = this.props;
    const { collapsed, value } = this.state;

    if (steps.length === 0) {
      return null;
    }

    return (
      <View style={[styles.wrapper, style]} onLayout={this.onLayout}>
        <View style={styles.visibleContainer}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: 16 }}>
              <Image source={OMZ_LOGO} style={styles.logo as ImageStyle} />
              <Text style={styles.amount}>+{amount}</Text>
            </View>

            <View>
              <Text style={styles.text}>{title}</Text>
              <Text style={styles.text}>{reference}</Text>
            </View>
          </View>

          <TouchableHighlight
            underlayColor="transparent"
            style={styles.chevron}
            onPress={this.toggleCollapse}
          >
            <Icon name={collapsed ? 'chevron-down' : 'chevron-up'} size={24} />
          </TouchableHighlight>
        </View>

        <Collapsible
          onAnimationEnd={this.onAnimationEnd}
          style={styles.hiddenContainer}
          duration={animationDuration}
          collapsed={collapsed}
        >
          {steps.map(
            (step: string, index: number): React.ReactElement<Text> => (
              <Text key={step} style={styles.text}>
                {index + 1}. {step}
              </Text>
            ),
          )}

          <TextInput
            style={styles.input}
            placeholder={t('input text')}
            placeholderTextColor={COLORS.placeholderColor}
            value={value}
            onChangeText={this.onValueChange}
          />

          <TouchableOpacity
            style={styles.button}
            onPress={this.onVerifyClick}
            onLongPress={this.onVerifyClick}
          >
            <Text style={styles.buttonText}>{t('buttons.verify').toUpperCase()}</Text>
          </TouchableOpacity>
        </Collapsible>
      </View>
    );
  }
}

export const CollapsibleReward = withNamespaces()(CollapsibleRewardComponent);
