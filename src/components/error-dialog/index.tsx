import React from "react";
import { Modal, View, Text } from 'react-native';
import { Button } from 'react-native-material-ui';
import Svg, { Path } from 'react-native-svg';
import Autolink from 'react-native-autolink';
import { Props, State } from "./types";
import { styles } from './styles';
import {COLORS} from "../../constants";

/*
    Usage:

        import { ErrorDialog } from "src/components/error-dialog";

        ...

        <ErrorDialog
          title   = {t('error.title')}
          message = {t('error.message')}
          email   = {t('error.email')}
          visible = {this.state.displayError}
          onPress = {this.closeModal}
        />

*/

const ICON_BORDER_PATH = 'M75 44.68c0 1.869-1.603 4.32-3.472 4.32H29.906c-.872 0-1.652.083-2.275.706L19 59.385v-8.1C19 47.959 15.7 49 15.7 49h-4.984C8.846 49 6 46.674 6 44.805V8.791C6 6.921 8.846 6 10.716 6h60.812C73.397 6 75 6.921 75 8.791V44.68zM71.653 0H10.716C5.357 0 0 3.432 0 8.791V44.68C0 50.038 5.357 55 10.716 55H12v9.618c0 2.368 2.286 4.362 4.654 4.362 1.246 0 2.514-.261 3.386-1.258L31.402 55h40.251C77.011 55 81 50.163 81 44.805V8.791C81 3.432 77.011 0 71.653 0z';
const ICON_PATH = 'M31.9 35.957c.623.623 1.371.872 2.243.872.872 0 1.62-.25 2.243-.872l4.86-4.86 4.86 4.86c.623.623 1.371.872 2.243.872.873 0 1.62-.25 2.243-.872 1.246-1.246 1.246-3.24 0-4.362l-4.984-4.86 4.86-4.86c1.246-1.246 1.246-3.24 0-4.361-1.246-1.246-3.24-1.246-4.362 0l-4.984 4.86-4.86-4.86c-1.247-1.246-3.24-1.246-4.362 0-1.246 1.246-1.246 3.24 0 4.36l4.86 4.86-4.86 4.86a3.247 3.247 0 0 0 0 4.363';

export class ErrorDialog extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    visible: false,
    title: '',
    message: '',
    email: '',
    onPress: () => {},
  };

  private onPress = (): void => {
    this.props.onPress();
  };

  private onRequestClose = (): void => {

  };


  public render(): React.ReactNode {
    const { visible, title, message, email } = this.props;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={this.onRequestClose}
      >
        <View style = {styles.container}>
          <View style = {styles.window}>
            <View style = {styles.titleArea}>
              <Text style = {styles.titleText}>{title}</Text>
              <Svg height={70} width={85}>
                <Path d={ICON_BORDER_PATH} fill={COLORS.lightMauve} stroke={COLORS.lightMauve} fillRule="evenodd"/>
                <Path d={ICON_PATH} fill={COLORS.lightMauve} stroke={COLORS.lightMauve} fillRule="evenodd"/>
              </Svg>
            </View>
            <View style = {styles.bodyArea}>
              <Text style = {styles.bodyText}>{message}</Text>
              {email !== '' &&
                <Autolink
                  text = {email}
                  email = {true}
                  linkStyle = {styles.emailText}
                />
              }
            </View>
            <View style = {styles.buttonArea}>
              <Button
                style={{ container: styles.buttonContainer, text: styles.buttonText }}
                upperCase={false}
                text={'Okay'}
                onPress={this.onPress}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }

}