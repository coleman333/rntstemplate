import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  window: {
    width: '80%',
    height: '70%',
    backgroundColor: COLORS.white,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
  },

  buttonContainer: {
    margin: 10,
    width: 200,
    borderRadius: 5,
    backgroundColor: COLORS.navy,
  },

  buttonText: {
    color: COLORS.lightMauve,
  },

  titleText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 24,
    fontWeight: '600',
    marginBottom: 30,
  },

  bodyText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 14,
    marginBottom: 15,
  },

  emailText: {
    textAlign: 'center',
    color: COLORS.mauve,
    fontWeight: '500',
    fontSize: 22,
  },

  titleArea: {
    width: '100%',
    height: '55%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },

  bodyArea: {
    width: '100%',
    height: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonArea: {
    width: '100%',
    height: '15%',
    alignItems: 'center',
    justifyContent: 'center',
  },

});
