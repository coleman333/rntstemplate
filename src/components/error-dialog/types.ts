import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  visible: boolean;
  title: string,
  message: string,
  email: string,
  onPress: Function;
}

export interface State {
}
