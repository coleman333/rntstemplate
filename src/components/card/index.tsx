import React from 'react';
import { Text, View } from 'react-native';

import { Props } from './types';
import { styles } from './styles';

export const Card: React.SFC<Props> = props => (
  <View style={styles.container}>
    <View style={styles.separator} />
    <Text style={styles.title}>{props.title.toUpperCase()}</Text>
    {props.children}
  </View>
);
