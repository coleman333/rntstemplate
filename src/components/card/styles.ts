import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingBottom: 20,
  },

  separator: {
    width: '100%',
    height: 1,
    opacity: 0.12,
    backgroundColor: COLORS.lightTextColor,
  },

  title: {
    fontSize: 14,
    fontFamily: 'Poppins',
    //fontWeight: 'bold',
    lineHeight: 16,
    letterSpacing: 1,
    color: COLORS.navy,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
});
