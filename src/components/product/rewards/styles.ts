import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';


const {width, height} = Dimensions.get('window');
const IMAGE_WIDTH = width - (width / 100 * 5);
export const styles = StyleSheet.create({

  container: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderBottomColor: COLORS.gray,
    borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 20,
  },

  title: {
    textAlign: 'left',
    color: COLORS.navy,
    fontSize: 19,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
  },

  imageHolder: {
    marginTop: 10,
    width: '100%',
    height: 220,
  },

  rewardsImage: {
    flex:1,
    height: undefined,
    width: undefined,
  },


});
