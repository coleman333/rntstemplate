import React from 'react';
import { View, Text, Image, ImageStyle, TouchableHighlight } from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';

import { SignupBanner } from "src/components/product/signup-banner";

const AD_IMAGE = require('./assets/rewards.png');

export class Rewards extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    title: '',
    labels: { earned: '', signup: '' },
    onBannerPressed: () => {},
  };

  private onBannerPressed = () : void => {
    this.props.onBannerPressed();
  };

  public render(): React.ReactNode {
    const { title, labels } = this.props;
    return (
      <View style = {styles.container}>
        <Text style = {styles.title}>{title}</Text>
        <TouchableHighlight
          onPress = {this.onBannerPressed}
          underlayColor={'transparent'}
          style = {styles.imageHolder}
        >
            <Image source = {AD_IMAGE} style = {styles.rewardsImage as ImageStyle} resizeMode = {'contain'}/>
        </TouchableHighlight>
        <TouchableHighlight
          onPress = {this.onBannerPressed}
          underlayColor={'transparent'}
        >
          <SignupBanner labels = {labels} />
        </TouchableHighlight>
      </View>
    );
  }

}