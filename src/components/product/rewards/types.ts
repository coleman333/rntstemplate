import { ViewProps } from 'react-native';
import {BannerLabels} from "../signup-banner/types";

export interface Props extends ViewProps {
  title: string;
  labels: BannerLabels;
  onBannerPressed(): void;
}

export interface State {
}
