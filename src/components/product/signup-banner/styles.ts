import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: 72,
    borderWidth: 1,
    borderColor: COLORS.gray,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    flexDirection: 'row',
  },

  left: {
    width: '60%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  right: {
    width: '40%',
    height: '100%',
    backgroundColor: COLORS.navy,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
  },

  imageHolder: {
    position: 'absolute',
    top: -20,
    left: '35%',
    width:  50,
    height: 50,
  },

  leftText: {
    color: COLORS.navy,
    fontSize: 14,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
  },

  rightText: {
    color: COLORS.mauve,
    fontSize: 13,
    fontFamily: 'Poppins',
    fontWeight: '500',
  },

  image: {
    flex:1,
    height: undefined,
    width: undefined,
  },

});
