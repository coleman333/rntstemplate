import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  labels: BannerLabels;
}

export interface State {
}

export interface BannerLabels {
  earned: string;
  signup: string;
}
