import React from 'react';
import { View, Text, Image, ImageStyle } from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';

const IMAGE = require('./assets/omz.png');

export class SignupBanner extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    labels: { earned: '', signup: '' },
  };

  public render(): React.ReactNode {
    const { labels } = this.props;
    return (
      <View style = {styles.container}>
        <View style = {styles.left}>
          <Text style = {styles.leftText}>{labels.earned}</Text>
        </View>
        <View style = {styles.right}>
          <View style = {styles.imageHolder}>
            <Image source = {IMAGE} style = {styles.image as ImageStyle} resizeMode = {'contain'}/>
          </View>
          <Text style = {styles.rightText}>{labels.signup}</Text>
        </View>
      </View>
    );
  }

}