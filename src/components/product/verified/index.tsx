import React from 'react';
import { View, Image, ImageStyle, Text } from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';

const ICON = require("./assets/verified.png");

export class Verified extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    value:  '',
  };

  public render(): React.ReactNode {
    const { value } = this.props;
    return (
      <View style = {styles.container}>
        <Image
          source = {ICON}
          style       = {styles.icon as ImageStyle}
          resizeMode  = {'contain'}
        />
        <View style = {styles.valueHolder}>
          <Text style = {styles.idText}>{value}</Text>
        </View>
      </View>
    );
  }

}