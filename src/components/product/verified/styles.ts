import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';


export const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon: {
    width:    130,
    height:   50,
  },

  valueHolder: {
    position: 'absolute',
    top:      18,
    left:     52,
    zIndex:   9999,
  },

  idText: {
    textAlign: 'left',
    color: COLORS.white,
    fontSize: 10,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
  },


});
