import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  value:  string;
}

export interface State {
}
