import { ViewProps } from 'react-native';
import {TraceabilityData, TraceabilityItem} from "src/screens/Product/types";

export interface Props extends ViewProps {
  data:  TraceabilityData;
}

export interface State {
  sources:  Array<string>;
  data:     Array<TraceabilityItem>;
  opened:   Array<boolean>;
}
