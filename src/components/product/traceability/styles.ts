import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';


export const styles = StyleSheet.create({

  container: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderBottomColor: COLORS.gray,
    borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 10,
  },

  title: {
    textAlign: 'left',
    color: COLORS.navy,
    fontSize: 19,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
  },

  itemClosed: {
    width: '100%',
    height: 60,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: COLORS.gray,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  itemOpened: {
    width: '100%',

    borderRadius: 8,
    borderWidth: 1,
    borderColor: COLORS.gray,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  dateHolder: {
    width: '25%',
    height: '100%',
    flex: 1,
    backgroundColor: COLORS.navy,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },

  dateLine1: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    minHeight: 58,
  },

  dateLine2: {
    width: '100%',
    flexGrow: 1,
    borderBottomLeftRadius: 8,
  },

  infoHolder: {
    width: '65%',
    backgroundColor: COLORS.white,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20,
  },

  infoLine1: {
    width: '100%',
    minHeight: 58,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  infoLine2: {
    width: '100%',
    height: 'auto',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginBottom: 15,
  },

  iconHolder: {
    width: '10%',
    backgroundColor: COLORS.white,
    borderTopRightRadius: 8,
    height: 58,
    borderBottomRightRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },

  dateText: {
    color: COLORS.white,
    fontSize: 12,
    fontFamily: 'Poppins',
    fontWeight: '500',
  },

  infoText: {
    color: COLORS.navy,
    fontSize: 15,
    fontFamily: 'Poppins',
    fontWeight: 'normal',
  }

});
