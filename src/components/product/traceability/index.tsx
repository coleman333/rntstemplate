import React from 'react';
import {View, Image, ImageStyle, Text, TouchableHighlight} from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import {COLORS} from "../../../constants";

export class Traceability extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    data: {},
  };

  public state: State = {
    sources:  [],
    data:     [],
    opened:   [],
  };

  public static getDerivedStateFromProps(props: Props, state: State) : State {
    if (typeof props.data !== 'undefined' && !state.sources.length) {
      let keys: Array<string> = Object.keys(props.data);
      if (keys.length) {
        for (let i = 0; i < keys.length; i++) {
          state.sources[i]  = keys[i];
          state.data[i]     = props.data[ keys[i] ];
          state.opened[i]   = false;
        }
      }
    }
    return state;
  }

  private toggle = (offset: number): void => {
    let opened = this.state.opened;
        opened[offset] = !opened[offset];
    this.setState({ opened: opened });

  };

  private renderItem = (i: number) : React.ReactNode => {
    let JSX = (
      <TouchableHighlight
        onPress = {() => { this.toggle(i); }}
        underlayColor={'transparent'}
        key = {'traceability-item-' + i}
      >
      <View style = {this.state.opened[i] ? styles.itemOpened : styles.itemClosed}>
        <View style = {styles.dateHolder}>
          <View style={styles.dateLine1}>
            <Text style = {styles.dateText}>{moment(this.state.data[i].Date).format("D MMM YYYY")}</Text>
          </View>
          {this.state.opened[i] &&
          <View style={styles.dateLine2}></View>
          }
        </View>
        <View style = {styles.infoHolder}>
          <View style = {styles.infoLine1}>
            <Text style = {styles.infoText}>{this.state.sources[i]}</Text>
          </View>
          {this.state.opened[i] &&
          <View style={styles.infoLine2}>
            <Text style = {styles.infoText}>{this.state.data[i].Address + '\n' + this.state.data[i].Country}</Text>
          </View>
          }
        </View>
        <View style = {styles.iconHolder}>
          {this.state.opened[i] === true &&
          <AntDesignIcon name={'up'} size={14} color={COLORS.black} />
          }
          {this.state.opened[i] === false &&
          <AntDesignIcon name={'down'} size={14} color={COLORS.black} />
          }
        </View>
      </View>
      </TouchableHighlight>
    );
    return JSX;
  };

  private renderItems = () : Array<React.ReactNode> => {
    let JSX = [];
    for (let i = 0; i < this.state.sources.length; i++) {
      JSX.push(this.renderItem(i));
    }
    return JSX;
  };

  public render(): React.ReactNode {
    return (
      <View style = {styles.container}>
        <Text style = {styles.title}>Traceability</Text>
        {this.renderItems()}
      </View>
    );
  }

}