import { ViewProps } from 'react-native';
import { ProductForConsumer } from 'src/models/entities/Product';

export interface Props extends ViewProps {
  product:  ProductForConsumer;
  onPress(id: number): void;
}

export interface State {
}
