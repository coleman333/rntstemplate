import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';
import {getFontFamily} from "../../../constants";


export const styles = StyleSheet.create({

  item: {
    justifyContent: 'space-between',
    width: 120,
    height: '100%',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: COLORS.lightBorderColor,
    marginHorizontal: 5,
    //paddingHorizontal: 6,
    paddingTop: 6,
  },

  imageContainer: {
    width: '100%',
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    width: 63,
    height: 113,
  },

  text: {
    fontSize: 12.1,
    fontFamily: getFontFamily(),
    lineHeight: 16,
    letterSpacing: 0.4,
    color: COLORS.lightTextColor,
  },

  titleHolder: {
    height: '20%',
    width: '100%',
    overflow: 'hidden',
    paddingHorizontal: 5,
  },

  bottomInfo: {
    height: '10%',
    width: '100%',
    flexDirection: 'row',
    paddingHorizontal: 5,
    justifyContent: 'space-between',
    backgroundColor: COLORS.lightNavy,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
  },

  bottomInfoLeft: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  bottomInfoRight: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },

  bottomText: {
    fontFamily: 'Poppins',
    fontSize: 11,
    color: COLORS.white,
  },

});
