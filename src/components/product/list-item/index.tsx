import React from 'react';
import { View, Image, ImageStyle, Text, TouchableHighlight } from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';
import { getImageURL } from "src/utils/product";

export class ProductListItem extends React.Component<Props, State> {

  private onPress = (): void => {
    this.props.onPress(this.props.product.id);
  };

  public render(): React.ReactNode {

    const { product } = this.props;

    let imageURL = getImageURL(product.product_images);
    let priceCurrencyPrefix, priceCurrencySuffix;

    if (product.average_price_currency) {
      if (product.average_price_currency === '$') {
        priceCurrencyPrefix = '$';
      } else {
        priceCurrencySuffix = product.average_price_currency;
      }
    } else {
      priceCurrencyPrefix = '$';
    }

    return (
      <TouchableHighlight
        onPress = {this.onPress}
        underlayColor = {'transparent'}
      >
        <View style={styles.item}>
          <View style={styles.imageContainer}>
            <Image
              style       = {styles.image as ImageStyle}
              source      = {{uri: imageURL}}
              resizeMode  = {'contain'}
            />
          </View>
          <View style = {styles.titleHolder}>
            <Text style={styles.text}>{product.name}</Text>
          </View>
          <View style = {styles.bottomInfo}>
            <View style = {styles.bottomInfoLeft}>
              <Text style={styles.bottomText}>{product.user_rating.toFixed(2)}</Text>
            </View>
            <View style = {styles.bottomInfoRight}>
              <Text style={styles.bottomText}>{priceCurrencyPrefix}{product.average_price.toFixed(2)}{priceCurrencySuffix}</Text>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

}