import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';


export const styles = StyleSheet.create({

  container: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    // borderBottomColor: COLORS.gray,
    // borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    //paddingBottom: 20,
  },

  title: {
    textAlign: 'left',
    color: COLORS.navy,
    fontSize: 19,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
  },

  text: {
    marginTop: 5,
    textAlign: 'left',
    color: COLORS.navy,
    fontSize: 13.5,
    fontFamily: 'Poppins',
  },

});
