import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  title:  string;
  text:   string;
}

export interface State {
}
