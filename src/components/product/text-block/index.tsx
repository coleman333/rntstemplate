import React from 'react';
import { View, Text, Image, ImageStyle } from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';

export class TextBlock extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    title:  '',
    text:   '',
  };

  public render(): React.ReactNode {
    const { title, text } = this.props;
    return (
      <View style = {styles.container}>
        <Text style = {styles.title}>{title}</Text>
        <Text style = {styles.text}>{text}</Text>
      </View>
    );
  }

}