import React from 'react';
import {
  View,
  Text,
  Image,
  ImageStyle,
  Linking,
  TouchableHighlight,
  Dimensions,
} from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';
import { Verified } from 'src/components/product/verified';
import { CommonBack } from 'src/components/common-back';
import StarsAndButtonRating from 'src/components/stars-and-button-rating';

const { height } = Dimensions.get('window');
const HOLDER_HEIGHT = height - (height / 100 * 3);
const CONSUMER_DELTA_Y = 100;


export class BottleDetails extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    onBackPressed: (): void => {
    },
  };

  private onBuyPressed = (): void => {
    Linking.openURL('https://www.wineconnection.com.sg/buy-wine-online.html');
  };

  private getImageURL = (images: Array<any>): string => {
    let image = '';
    if(images) {
      for (let i = 0; i < images.length; i++) {
        if (images[i].image_type === 'FRONT') {
          image = images[i].image_id;
        }
      }
    }
    return image;
  };

  public state: State = {
    displayRatingModal: false,
  };

  public render(): React.ReactNode {
    const { details, labels, auth, onRate, t, verified } = this.props;

    let priceCurrencyPrefix, priceCurrencySuffix;
    let imageURL = this.getImageURL(details.product.product_images);
    let dynamicHeight = auth.authorized ? HOLDER_HEIGHT - CONSUMER_DELTA_Y : HOLDER_HEIGHT;


    if (details.product.average_price_currency) {
      if (details.product.average_price_currency === '$') {
        priceCurrencyPrefix = '$';
      } else {
        priceCurrencySuffix = details.product.average_price_currency;
      }
    } else {
      priceCurrencyPrefix = '$';
    }
    return (
      <View style={[styles.container, { height: dynamicHeight }]}>
        <View style={styles.left}>
          {auth.authorized === false &&
          <View style={styles.backHolder}>
            <CommonBack title={'Scan'} theme={'dark'} onPress={this.props.onBackPressed}/>
          </View>
          }
          <View style={styles.imageHolder}>
            <Image
              source={{ uri: imageURL }}
              style={styles.bottleImage as ImageStyle}
              resizeMode={'contain'}
            />
          </View>
          {verified === true && typeof details.textIdentifier !== 'undefined' &&
          <View style={styles.rightInfoHolder}>
            <Verified value={details.textIdentifier}/>
          </View>
          }
        </View>
        <View style={styles.right}>
          <View style={styles.rightContent}>
            <Text style={styles.nameText}>{details.product.name}</Text>
            <Text
              style={styles.yearText}>{details.product.year || (details.product.wine && details.product.wine.vintage) || 'unknown year'}</Text>
            <Text style={styles.countryText}>{details.product.country || 'unknown country'}</Text>
            <View style={styles.StarsAndButtonContainer}>
              <StarsAndButtonRating
                t = {t}
                readonly = {auth.authorized === false}
                stars = {details.product.user_rating ? details.product.user_rating : 0}
                userRating = {details.product.user_review ? details.product.user_review.numerical_rating : 0}
                onRate = {onRate}
              />
            </View>
            <Text style={styles.alcText}>{details.product.alcohol_content + '%'}</Text>
            <Text style={styles.variantText}>{details.product.variant + 'ml'}</Text>
            <Text style={styles.grapeText}>{details.product.wine && details.product.wine.grape_varietal}</Text>
            <View style={styles.priceHolder}>
              <Text
                style={styles.priceText}>{priceCurrencyPrefix}{details.product.average_price}{priceCurrencySuffix}</Text>
              <Text style={styles.priceLabelsText}>{labels.price_label}</Text>
            </View>
            {auth.authorized === true &&
            <TouchableHighlight
              onPress={this.onBuyPressed}
              underlayColor={'transparent'}
            >
              <Text style={styles.priceLabelsText}>{labels.buy_text}</Text>
            </TouchableHighlight>
            }
            <View style={styles.ratingHolder}/>
          </View>
        </View>
      </View>
    );
  }

}
