import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';

const {width, height} = Dimensions.get("window");
const HOLDER_HEIGHT = height - (height / 100 * 3);

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: 520,
    height: HOLDER_HEIGHT,
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomColor: COLORS.gray,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },

  left: {
    width: '50%',
    height: '100%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },

  right: {
    width: '50%',
    height: '100%',
    backgroundColor: COLORS.navy,
  },

  rightContent: {
    width: '100%',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    paddingLeft: 10,
    flexDirection: 'column',
  },

  backHolder: {
    width: '100%',
    height: '10%',
    flexGrow: 1,
  },

  backComponentHolderStyles: {
    marginTop: 0,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    flexDirection: 'row',
    width: '100%',
    height: 50,
  },

  imageHolder: {
    width: '100%',
    //height: '70%',
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //marginBottom: 20,
  },

  rightInfoHolder: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  bottleImage: {
    minWidth:  100,
    minHeight: 400,
  },

  nameText: {
    color: COLORS.white,
    fontSize: 23,
    fontFamily: 'Poppins',
    fontWeight: '600',
  },

  yearText: {
    marginTop: 20,
    textAlign: 'left',
    color: COLORS.white,
    fontSize: 19,
    fontFamily: 'Poppins',
    fontWeight: '500',
  },

  countryText: {
    marginTop: 20,
    textAlign: 'left',
    color: COLORS.white,
    fontSize: 19,
    fontFamily: 'Poppins',
    fontWeight: '500',
  },

  ratingText: {
    color: COLORS.white,
    fontSize: 23,
    fontFamily: 'Poppins',
    fontWeight: '600',
  },

  ratingHolder: {
    marginTop: 20,
  },

  priceHolder: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  StarsAndButtonContainer:{
    height:'11%',
    width:'100%'
  },
  alcText: {
    marginTop: 20,
    textAlign: 'left',
    color: COLORS.white,
    fontSize: 11,
    fontFamily: 'Poppins',
  },

  variantText: {
    marginTop: 20,
    textAlign: 'left',
    color: COLORS.white,
    fontSize: 11.5,
    fontFamily: 'Poppins',
  },

  grapeText: {
    marginTop: 20,
    textAlign: 'left',
    color: COLORS.white,
    fontSize: 11.5,
    fontFamily: 'Poppins',
  },

  priceText: {
    textAlign: 'left',
    color: COLORS.white,
    fontSize: 23,
    fontFamily: 'Poppins',
    fontWeight: '600',
    marginRight: 5,
  },

  priceLabelsText: {
    textAlign: 'left',
    textAlignVertical: 'center',
    color: COLORS.white,
    fontSize: 11.5,
    fontFamily: 'Poppins',
  },

  idLabelText: {
    textAlign: 'left',
    color: COLORS.navy,
    fontSize: 15,
    fontFamily: 'Poppins',
  },

  idText: {
    textAlign: 'left',
    color: COLORS.navy,
    fontSize: 15,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
  },

});
