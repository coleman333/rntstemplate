import { ViewProps } from 'react-native';
import { IProductDetailsState } from "src/redux/reducers/Products/details";
import { IAuthState } from "src/redux/reducers/Auth";
import { TranslationFunction } from 'i18next';

export interface LabelTranslations {
  price_label: string;
  buy_text: string;
  omniaz_id: string;
}

export interface Props extends ViewProps {
  verified: boolean;
  details?: IProductDetailsState;
  labels: LabelTranslations;
  auth: IAuthState;
  onBackPressed(): void;
  setRating(data: any): void;
  onRate(rating:number) : void;
  t: TranslationFunction;
}

export interface State {
  [key:string]: any,
}
