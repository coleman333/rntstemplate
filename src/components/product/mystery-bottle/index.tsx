import React from 'react';
import {
  View,
  Text,
  Image,
  ImageStyle,
  Dimensions,
} from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';
const mysteryBottle3 = require('../../../../assets/images/MysteryBottle3.png');
const { height } = Dimensions.get('window');
const HOLDER_HEIGHT = height - (height / 100 * 3);
const CONSUMER_DELTA_Y = 100;

/* template for mystery bottle

<MysteryBottle
  t={t}
  onBackPressed={this.onBackPressed}
  }}
/>
*/

export class MysteryBottle extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    onBackPressed: (): void => {
    },
  };

  public render(): React.ReactNode {
    const { t} = this.props;
    let dynamicHeight = HOLDER_HEIGHT + CONSUMER_DELTA_Y;
    return (
      <View style={[styles.container,
        { height: dynamicHeight }
        ]}>
        <View style={styles.left}>

          <View style={styles.imageHolder}>
            <Image source={mysteryBottle3 }
              style={styles.bottleImage as ImageStyle}
            />
          </View>
        </View>
        <View style={styles.right}>
          <View style={styles.rightContent}>
            <Text style={styles.nameText}>{t('Profile:mystery_Bottle')}</Text>

            <View style={styles.ratingHolder}/>
          </View>
        </View>
      </View>
    );
  }

}

