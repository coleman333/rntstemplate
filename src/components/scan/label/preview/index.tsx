import React from 'react';
import {View, Text, Image, ImageStyle} from 'react-native';
import { Button } from 'react-native-material-ui';

import { Props, State } from './types';
import { styles } from './styles';

export class Preview extends React.Component<Props, State> {

  public constructor(props: Props) {
    super(props);
  }

  public state: State = {
  };

  public render(): React.ReactNode {
    const { data, onRetake, onUpload, t } = this.props;
    return (
      <View style = {styles.container}>
        <View style = {styles.previewArea}>
          <Image
            source={{uri: data.uri}}
            style={styles.image as ImageStyle}
            resizeMode={'cover'}
          />
          <View style = {styles.messageHolder}>
            <Text style = {styles.message}>{t('Scan:preview_message')}</Text>
          </View>
        </View>
        <View style = {styles.bottomArea}>
          <Button
            style={{ container: styles.buttonContainer, text: styles.buttonText }}
            upperCase={false}
            text={t('Scan:retake')}
            onPress={onRetake}
          />
          <Button
            style={{ container: styles.buttonContainer, text: styles.buttonText }}
            upperCase={false}
            text={t('Scan:upload')}
            onPress={onUpload}
          />
        </View>
      </View>
    );
  }

}