import { ViewProps } from 'react-native';
import { CameraImageObject } from "src/screens/ScanTab/types";
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  t: TranslationFunction;
  data: CameraImageObject;
  onRetake(): void;
  onUpload(): void;
}

export interface State {
}
