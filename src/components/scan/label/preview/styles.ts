import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
  },

  previewArea: {
    width: '100%',
    height: '80%',
  },

  bottomArea: {
    flexDirection: 'row',
    width: '100%',
    height: '20%',
    backgroundColor: COLORS.OMNIAZ_submenu,
    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    width: '100%',
    height: '100%',
  },

  buttonContainer: {
    margin: 10,
    width: 150,
    height: 31,
    borderRadius: 15,
    backgroundColor: COLORS.white,
    shadowOffset:{  width: 1,  height: 1 },
    shadowColor: COLORS.black,
    shadowOpacity: 0.7,
    elevation: 3,
  },

  buttonText: {
    color: COLORS.navy,
    fontSize: 16,
    fontWeight: '500',
  },

  messageHolder: {
    position: 'absolute',
    zIndex: 9999,
    bottom: 0,
    left: 0,
    width: '100%',
    height: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.black,
    opacity: 0.7,
  },

  message: {
    color: COLORS.white,
    fontSize: 16,
  },


});
