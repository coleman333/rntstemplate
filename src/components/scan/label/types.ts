import { ViewProps } from 'react-native';
import { CameraImageObject } from "src/screens/ScanTab/types";
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  t: TranslationFunction;
  onSuccess(data: CameraImageObject): void;
  showLoader(): void;
  hideLoader(): void;
}

export interface State {
  data: CameraImageObject | null;
  display: 'camera' | 'preview';
}
