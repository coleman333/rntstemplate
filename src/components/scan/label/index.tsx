import React from 'react';

import { Props, State } from './types';
import { CameraImageObject } from "src/screens/ScanTab/types";

import { ScanByPhoto } from "./photo";
import { Preview } from "./preview";

export class LabelScan extends React.Component<Props, State> {

  public constructor(props: Props) {
    super(props);
  }

  public state: State = {
    data:     null,
    display:  'camera',
  };

  private onImageTaken = (data: CameraImageObject): void => {
    this.setState({data: data, display: 'preview'});
  };

  private onRetake = (): void => {
    this.setState({display: 'camera'});
  };

  private onUpload = (): void => {
    this.props.onSuccess(this.state.data);
  };

  private renderCamera = (): React.ReactNode => {
    return (
      <ScanByPhoto
        t = {this.props.t}
        onSuccess = {this.onImageTaken}
        showLoader = {this.props.showLoader}
        hideLoader = {this.props.hideLoader}
      />
    );
  };

  private renderPreview = (): React.ReactNode => {
    return <Preview t = {this.props.t} onRetake={this.onRetake} onUpload={this.onUpload} data = {this.state.data}/>
  };

  public render(): React.ReactNode {
    return this.state.display === 'camera' ? this.renderCamera() : this.renderPreview();
  }
}