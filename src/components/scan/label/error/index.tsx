import React from "react";
import { Modal, View, Text } from 'react-native';
import { Button } from 'react-native-material-ui';
import Svg, { Path } from 'react-native-svg';
import Autolink from 'react-native-autolink';
import { Props, State } from "./types";
import { styles } from './styles';
import {COLORS} from "../../../../constants";

/*
    Usage:

        import { ErrorDialog } from "src/components/error-dialog";

        ...

        <ErrorDialog
          title   = {t('error.title')}
          message = {t('error.message')}
          email   = {t('error.email')}
          visible = {this.state.displayError}
          onPress = {this.closeModal}
        />

*/


export class ErrorLabelDialog extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    visible: false,
    title: '',
    message: '',
    buttonText: '',
    onPress: () => {},
  };

  private onPress = (): void => {
    this.props.onPress();
  };

  private onRequestClose = (): void => {

  };


  public render(): React.ReactNode {
    const { visible, title, message, buttonText } = this.props;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={this.onRequestClose}
      >
        <View style = {styles.container}>
          <View style = {styles.window}>
            <View style = {styles.titleArea}>
              <Text style = {styles.titleText}>{title}</Text>
            </View>
            <View style = {styles.bodyArea}>
              <Text style = {styles.bodyText}>{message}</Text>
            </View>
            <View style = {styles.buttonArea}>
              <Button
                style={{ container: styles.buttonContainer, text: styles.buttonText }}
                upperCase={false}
                text={buttonText}
                onPress={this.onPress}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }

}
