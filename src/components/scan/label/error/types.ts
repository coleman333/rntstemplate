import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  visible: boolean;
  title: string,
  message: string,
  buttonText: string,
  onPress: Function;
}

export interface State {
}
