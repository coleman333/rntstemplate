import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  window: {
    width: '80%',
    height: '35%',
    backgroundColor: COLORS.white,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
  },

  buttonContainer: {
    margin: 10,
    width: 200,
    borderRadius: 5,
    backgroundColor: COLORS.lightMauve,
  },

  buttonText: {
    fontWeight: '600',
    color: COLORS.navy,
  },

  titleArea: {
    width: '100%',
    height: '25%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  titleText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 25,
    fontWeight: '700',
  },

  bodyArea: {
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 1,
  },

  buttonArea: {
    width: '100%',
    height: '25%',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowRadius: 2,
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: 0.5,
    elevation: 3,
  },

  bodyText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 16,
    marginBottom: 15,
  },

});
