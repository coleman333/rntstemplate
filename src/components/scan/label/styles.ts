import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
  },

  cameraArea: {
    width: '100%',
    height: '80%',
  },

  bottomArea: {
    width: '100%',
    height: '20%',
    backgroundColor: COLORS.OMNIAZ_submenu,
    justifyContent: 'center',
    alignItems: 'center',
  },


});
