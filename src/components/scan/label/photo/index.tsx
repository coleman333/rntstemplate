import React from 'react';
import {View, TouchableOpacity, Dimensions, Image, ImageStyle} from 'react-native';

import { Props, State } from './types';
import { styles } from './styles';
import { PhotoOverlay } from './overlay';
import { RNCamera } from 'react-native-camera';

const {width, height} = Dimensions.get("window");
const PHOTO_SAFE_WIDTH = 1000 * height / width;

export class ScanByPhoto extends React.Component<Props, State> {


  public state: State = {};

  private camera: RNCamera;

  public constructor(props: Props) {
    super(props);
  }

  private makePhoto = async (): Promise<void> => {
    this.props.showLoader();
    if (this.camera) {
      this.props.onSuccess(
        await this.camera.takePictureAsync({
          width: PHOTO_SAFE_WIDTH,
          quality: 0.7,
          base64: true,
          fixOrientation: true,
          forceUpOrientation: true,
        })
      );
    }
    this.props.hideLoader();
  };

  public render(): React.ReactNode {
    const { t } = this.props;
    return (
      <View style = {styles.container}>
        <View style = {styles.cameraArea}>
          <RNCamera
            ref={ref => { this.camera = ref; }}
            style = {styles.camera}
            type = {RNCamera.Constants.Type.back}
            flashMode = {RNCamera.Constants.FlashMode.auto}
            permissionDialogTitle = {t('Scan:camera_permission_title')}
            permissionDialogMessage = {t('Scan:camera_permission_text')}
            onGoogleVisionBarcodesDetected = {({ barcodes }) => {}}
          />
          <PhotoOverlay
            message = {t('Scan:photo_overlay_text')}
          />
        </View>
        <View style = {styles.bottomArea}>
          <TouchableOpacity
            onPress = {this.makePhoto}
            style = {styles.cameraButton}>
            <Image
              source={require('./assets/camera.png')}
              style={styles.cameraIcon as ImageStyle}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
