import React from 'react';
import { View, Dimensions, Text } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import {COLORS} from "src/constants";
import { Props, State } from './types';
import { styles } from './styles';

const PATH_ARC = 'M0,29.88a4.21,4.21,0,0,1,3-4c30.65-9.08,74.2-14.75,122.51-14.75,48.82,0,92.77,5.79,123.47,15a4.2,4.2,0,0,1,3,4V0H0Z';
const { width } = Dimensions.get('window');

export class PhotoOverlay extends React.Component<Props, State> {

  private readonly maskLength: number;

  public static defaultProps: Partial<Props> = {
    message: '',
  };

  public constructor(props: Props) {
    super(props);

    this.maskLength = ( width * 85 ) / 100;
  }

  public state: State = {
  };

  public render(): React.ReactNode {
    const { message } = this.props;
    return (
      <View style = {styles.container}>
        <View style = {styles.leftGray}></View>
        <View style = {styles.rightGray}></View>
        <View style = {styles.topGray}>
          <View style={ styles.grayBack }></View>
          <Svg viewBox="0 0 252 30" width={width * 0.8 + 1} height={width * 0.8 * 0.12 + 1}>
            <Path d = { PATH_ARC } fill={COLORS.grayOverlay} />
          </Svg>
        </View>
        <View style = {styles.bottomGray}>
          <View style = { styles.grayBack }></View>
          <Svg viewBox="0 0 252 30" width={width * 0.8 + 1} height={width * 0.8 * 0.12 + 1}>
            <Path d = { PATH_ARC } fill={COLORS.grayOverlay} />
          </Svg>
        </View>
        { message !== '' && <View style = { styles.messageContainer }>
          <Text style = { styles.messageText }>{ message }</Text>
        </View>}
      </View>
    );
  }

}
