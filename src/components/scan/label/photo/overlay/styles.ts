import { Dimensions, StyleSheet } from 'react-native';
import {COLORS} from "src/constants";

const OPACITY: number = 0.8;
const SIZE: number = 205;
const BORDER: number = 1;
const LINE_PADDING: number = 15;

export const styles = StyleSheet.create({

  container: {
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 10,
    width: '100%',
    height: '100%',
    flexDirection: 'row',
  },

  leftSide: {
    width: '50%',
    height: '100%',
  },

  rightGray: {
    position: 'absolute',
    left: '90%',
    top: 0,
    width: '10%',
    height: '100%',
    backgroundColor: COLORS.grayOverlay,
  },

  leftGray: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '10%',
    height: '100%',
    backgroundColor: COLORS.grayOverlay,
  },

  topGray: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '10%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  bottomGray: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: '100%',
    height: '12%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    transform: [
      {
        scaleY: -1
      }
    ],
  },

  grayBack: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '40%',
    backgroundColor: COLORS.grayOverlay,
  },

  messageContainer: {
    width: '100%',
    position: 'absolute',
    left: 0,
    bottom: 5,
    alignItems: 'center',
  },

  messageText: {
    fontSize: 16,
    color: '#fff',
  },

});
