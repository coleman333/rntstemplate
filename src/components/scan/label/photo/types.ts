import { ViewProps } from 'react-native';
import { CameraImageObject } from "src/screens/ScanTab/types";
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  onSuccess(data: CameraImageObject): void;
  showLoader(): void;
  hideLoader(): void;
  t: TranslationFunction;
}

export interface State {
}
