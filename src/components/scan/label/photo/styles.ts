import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
  },

  cameraArea: {
    width: '100%',
    height: '80%',
  },

  bottomArea: {
    width: '100%',
    height: '20%',
    backgroundColor: COLORS.OMNIAZ_submenu,
    justifyContent: 'center',
    alignItems: 'center',
  },

  camera: {
    width: '100%',
    height: '100%',
  },

  cameraIcon: {
    width: '60%',
    height: '60%',
  },

  cameraButton: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 3,
    borderColor: COLORS.lightMauve,
    backgroundColor: COLORS.white,
    justifyContent: 'center',
    alignItems: 'center',
  },

  button: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 20,
  },

  label: {
    fontSize: 14,
    color: COLORS.white,
  },

});
