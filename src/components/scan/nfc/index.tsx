import React from 'react';
import { View, Text, Image, ImageStyle, Platform } from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';
import { NFC_Reader, NFC_Interface } from "./reader";

const imageURL = require('./assets/nfc-demo.png');

export class ScanNFC extends React.Component<Props, State> {

  private NFC: NFC_Interface;

  public static defaultProps: Partial<Props> = {
    t: () => {},
    onSuccess: () => {},
    onCancel: () => {},
  };

  public constructor(props: Props) {
    super(props);
    this.NFC = new NFC_Reader(Platform.OS);
  }

  public state: State = {
  };

  public async componentDidMount(): Promise<any> {
    await this.NFC.read((uri: string) => {
      console.log('uri', uri);
      if (uri.length) {
        this.props.onSuccess(uri);
      } else {
        console.log('uri not given');
      }
    }, this.props.onCancel);
  }


  public render(): React.ReactNode {
    const {t} = this.props;
    return (
      <View style = {styles.container}>
        <View style = {styles.topArea}>
          <Image
            source      = {imageURL}
            style       = {styles.image as ImageStyle}
            resizeMode  = {'contain'}
          />
        </View>
        <View style = {styles.bottomArea}>
          <Text style = {styles.text}>{t('Scan:nfc_message')}</Text>
        </View>
      </View>
    );
  }

}