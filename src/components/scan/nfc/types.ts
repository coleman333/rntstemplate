import { ViewProps } from 'react-native';
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  t: TranslationFunction;
  onSuccess(link: string): void;
  onCancel(): void;
}

export interface State {
}
