import {StyleSheet} from "react-native";
import {COLORS} from "../../../constants";

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.OMNIAZ_submenu,
  },


  topArea: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    height: '60%',
  },

  bottomArea: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '40%',
  },

  image: {
    height: 115,
  },

  text: {
    fontFamily: 'Poppins',
    fontSize: 13.5,
    color: COLORS.white,
    textAlign: 'center',
  },

});