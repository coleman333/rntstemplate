import NFC, {NdefParser} from 'react-native-nfc-manager';
import en from "../../../i18n/en";

export interface NFC_Interface {
  read( success: Function, cancel: Function ): Promise<string>;
}

export class NFC_Reader implements NFC_Interface {

  private readonly OS: string;

  constructor(OS: string) {
    this.OS = OS;
  }

  public read = async (success: Function, cancel: Function): Promise<string> => {
    console.log('NFC ---');
    return this.OS === 'android' ?
      await this.read_Android(success, cancel) :
      await this.read_iOS(success, cancel);
  };

  private read_Android = async (success: Function, cancel: Function): Promise<string> => {
    let enabled = await NFC.isEnabled(); //Android only
    const supported = await NFC.isSupported();

    console.log('NFC supported:', supported);
    console.log('NFC enabled:', enabled);

    if (supported) {
      if (enabled) {
        await NFC.registerTagEvent(tag => {
          if (typeof tag.ndefMessage[0] !== 'undefined') {
            let result = NdefParser.parseUri(tag.ndefMessage[0]);
            if (typeof result.uri !== 'undefined') {
              success(result.uri);
            }
          }
        }, '', true);
      } else {
        NFC.goToNfcSetting();
        cancel();
      }
    }

    return '';
  };

  private read_iOS = async (success: Function, cancel: Function): Promise<string> => {

    const supported = await NFC.isSupported();
    console.log('NFC supported:', supported);

    if (supported) {

      await NFC.start({onSessionClosedIOS: () => {
          NFC.stop();
          cancel();
        }})
        .catch(error => {
          console.log('NFC started', error.message);
        });

      await NFC.registerTagEvent(tag => {
        if (typeof tag.ndefMessage[0] !== 'undefined') {
          let result = NdefParser.parseUri(tag.ndefMessage[0]);
          if (typeof result.uri !== 'undefined') {
            success(result.uri);
          }
        }
      }, '', true);
    }
    return '';
  };
  
}