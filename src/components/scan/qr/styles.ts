import {StyleSheet} from "react-native";
import {COLORS} from "../../../constants";

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
  },

  camera: {
    width: '100%',
    height: '100%',
  },

  topViewStyle: {
    position: 'absolute',
    zIndex: 999,
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },

  button: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 20,
  },

  label: {
    fontSize: 14,
    color: COLORS.white,
  },


  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },

});