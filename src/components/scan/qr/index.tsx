import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { Props, State } from './types';
import { styles } from './styles';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { QROverlay } from './overlay';


export class ScanQR extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    showLoader: () => {},
    onSuccess: () => {},
    onError: () => {},
    reactive: false,
    overlayText: '',
  };

  private scanner: any;

  public constructor(props: Props) {
    super(props);
    this.scanner = null;
  }

  public componentDidUpdate(prevProps: Readonly<Props>): void {
    if (prevProps.reactive === false && this.props.reactive) {
      this.onReactive();
    }
  }

  public state: State = {
  };

  private onSuccess = (e: any): void => {
    if (typeof e.data !== 'undefined') {
      this.props.showLoader();
      this.props.onSuccess(e.data);

      //stub
      //TODO: stub, uncomment line above
      //this.props.onSuccess("https://google.com"); //error
      //this.props.onSuccess("https://register.dev.omniaz.io/nhq0?s=2009970&n=NI3TKR2W");
      //this.props.onSuccess("https://register.omniaz.io/nhn0?m=046A6C62695881x000023&n=F456780B");
      //this.props.onSuccess("https://register.omniaz.io/nhq0?m=046A6C62695881&n=F456780B");
      //stub

    } else {
      this.props.onError();
    }
  };

  private onReactive = () : void => {
    if (this.scanner) {
      this.scanner.reactivate();
    }
  };


  public render(): React.ReactNode {
    return (
      <View style = {styles.container}>
        <QRCodeScanner
          ref = { (component: any) => {this.scanner = component} }
          onRead = {this.onSuccess}
          cameraStyle = {styles.camera}
          topViewStyle = {styles.topViewStyle}
          topContent = { <QROverlay message = {this.props.overlayText} /> }
        />
      </View>
    );
  }

}