import {StyleSheet} from "react-native";
import {COLORS} from "src/constants";

const OPACITY: number = 0.8;
const SIZE: number = 205;
const BORDER: number = 1;
const LINE_PADDING: number = 15;

export const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    flex: 1,
  },

  topSide: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: COLORS.black,
    opacity: OPACITY,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingVertical: LINE_PADDING,
  },

  centerSide: {
    width: '100%',
    height: SIZE,
    flexDirection: 'row',
  },

  bottomSide: {
    height: '40%',
    width: '100%',
    backgroundColor: COLORS.black,
    opacity: OPACITY,
    justifyContent: 'flex-start',
    alignItems: 'center',
    //paddingHorizontal: 80,
    paddingVertical: LINE_PADDING,

  },

  centerLeft: {
    paddingHorizontal: LINE_PADDING,
    flexGrow: 1,
    backgroundColor: COLORS.black,
    opacity: OPACITY,
  },

  centerRight: {
    paddingHorizontal: LINE_PADDING,
    flexGrow: 1,
    backgroundColor: COLORS.black,
    opacity: OPACITY,
  },

  centerCenter: {
    width: SIZE,
    height: SIZE,
    borderRadius: 10,
  },

  messageText: {
    textAlign: 'center',
    color: COLORS.white,
  },

  topLine: {
    width: SIZE,
    borderBottomWidth: BORDER,
    borderBottomColor: COLORS.white,
  },

  bottomLine: {
    width: SIZE,
    borderTopWidth: BORDER,
    borderTopColor: COLORS.white,
  },

  leftLine: {
    height: SIZE,
    borderRightWidth: BORDER,
    borderRightColor: COLORS.white,
  },

  rightLine: {
    height: SIZE,
    borderLeftWidth: BORDER,
    borderLeftColor: COLORS.white,
  },

  messageHolder: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 80,

  },

});