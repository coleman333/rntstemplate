import React from 'react';
import { View, Dimensions, Text } from 'react-native';

import { Props, State } from './types';
import { styles } from './styles';


export class QROverlay extends React.Component<Props, State> {

  private readonly maskLength: number;

  public static defaultProps: Partial<Props> = {
    message: '',
  };

  public constructor(props: Props) {
    super(props);
    let { width } = Dimensions.get('window');
    this.maskLength = ( width * 85 ) / 100;
  }

  public state: State = {
  };


  public render(): React.ReactNode {
    const { message } = this.props;
    return (
      <View style = {styles.container}>
        <View style = {styles.topSide}>
          <View style = {styles.topLine}/>
        </View>
        <View style = {styles.centerSide}>
          <View style = {styles.centerLeft}>
            <View style = {styles.leftLine}/>
          </View>
          <View style = {styles.centerCenter}></View>
          <View style = {styles.centerRight}>
            <View style = {styles.rightLine}/>
          </View>
        </View>
        <View style = {styles.bottomSide}>
          <View style = {styles.bottomLine}/>
          <View style = {styles.messageHolder}>
            <Text style = {styles.messageText}>{message}</Text>
          </View>

        </View>
      </View>
    );
  }

}