import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  message: string;
}

export interface State {
}
