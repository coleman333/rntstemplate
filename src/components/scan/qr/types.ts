import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  showLoader:   Function;
  onSuccess:    Function;
  onError:      Function;
  reactive:     boolean;
  overlayText:  string;
}

export interface State {
}
