import { Animated, ViewProps } from 'react-native';
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  active?: string;
  onChange?: Function;
  t: TranslationFunction;
}

export interface State {
  animatedOpacity: Animated.Value,
}
