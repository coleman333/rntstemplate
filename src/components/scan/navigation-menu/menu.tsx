import React from 'react';
import { TouchableHighlight, Animated, Text, View } from 'react-native';
import {Props, State} from "./types";
import {styles} from "./styles";

const ANIMATION_DURATION = 1000; //ms

export class ScanMenu extends React.Component<Props, State> {
  public static defaultProps: Partial<Props> = {
    active: '',
    onChange: () => {},
  };

  public state: State = {
    animatedOpacity: new Animated.Value(0),
  };

  public componentDidMount() : void {
    this.startAnimation();
  }

  private startAnimation = (): void => {
    Animated.timing(this.state.animatedOpacity as Animated.Value, {
      toValue: 1,
      duration: ANIMATION_DURATION,
    }).start();
  };


  private onPressed_PHOTO = (): void => {
    if (this.props.active !== 'photo') {
      this.props.onChange('photo');
    } else {
      this.props.onChange('');
    }
  };

  private onPressed_QR = (): void => {
    if (this.props.active !== 'qr') {
      this.props.onChange('qr');
    } else {
      this.props.onChange('');
    }
  };

  private onPressed_NFC = (): void => {
    if (this.props.active !== 'nfc') {
      this.props.onChange('nfc');
    } else {
      this.props.onChange('');
    }
  };

  public render(): React.ReactNode {

    const { active, t } = this.props;
    let { animatedOpacity } = this.state;
    return (
      <View style = {styles.container}>
        <TouchableHighlight
          style = {styles.clickable}
          underlayColor={'transparent'}
          onPress = {this.onPressed_QR}
        >
          <View style = {[styles.labelButton, active === 'qr' ? styles.labelButton_ACTIVE : {}]}>
            <Text style={styles.labelText}>{t('Scan:qr')}</Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          style = {[styles.clickable]}
          underlayColor={'transparent'}
          onPress = {this.onPressed_NFC}
        >
          <View style = {[styles.labelButton, active === 'nfc' ? styles.labelButton_ACTIVE : {}]}>
            <Text style={styles.labelText}>{t('Scan:nfc')}</Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          style = {styles.clickable}
          underlayColor={'transparent'}
          onPress = {this.onPressed_PHOTO}
        >
          <View style = {[styles.labelButton, active === 'photo' ? styles.labelButton_ACTIVE : {}]}>
            <Text style={styles.labelText}>{t('Scan:label')}</Text>
          </View>
        </TouchableHighlight>

      </View>
    );

  }
}
