import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: '100%',
    flex: 1,
    // borderWidth: 1,
    // borderColor: '#f00',
  },

  margins: {
    marginLeft: 30,
    marginRight: 30,
  },

  clickable: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },

  labelButton: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 2,
    width: 60,
    borderRadius: 10,
    backgroundColor: 'transparent',
  },

  labelButton_ACTIVE: {
    backgroundColor: COLORS.lightMauve,
  },

  labelText: {
    fontFamily: 'Poppins',
    fontSize: 13.5,
    color: COLORS.white,
  },
});
