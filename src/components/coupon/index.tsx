import React from 'react';
import {
  Linking,
  Clipboard,
  View,
  Text,
  TouchableHighlight,
  NativeSyntheticEvent,
  ImageLoadEventData,
} from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';
import { COLORS } from 'src/constants';
import differenceInDays from 'date-fns/difference_in_days';
import { ONLINE_COUPON_CLICK } from 'src/constants/ActionTypes/Events';
import { CouponImage } from './coupon-image';
import { RNToasty } from 'react-native-toasty'

const URL = 'https://www.wineconnection.com.sg/buy-wine-online.html';

export class Coupon extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    single: true,
    t: () => {},
    authorized: false,
    couponEvent: () => {},
    onButtonPressed: () => {},
  };

  public state: State = {
    left: 0,
    loaded: false,
  };

  private centerButton = (e: any): void => {
    this.setState({ left: e.nativeEvent.layout.width / 2 - 50 });
  };

  private bannerPressed = (): void => {
    if (this.props.authorized) {
      if (typeof this.props.data !== 'undefined' && typeof this.props.data.coupon !== 'undefined') {
        this.props.couponEvent({
          target_id: this.props.data.coupon.id,
          action: ONLINE_COUPON_CLICK,
        });
        setTimeout(() => {
          Linking.openURL(URL);
        }, 1000 * 1);
      }
    } else {
      this.props.onButtonPressed();
    }
  };

  private writeToClipboard = async () => {
    const {t} = this.props;
    if (typeof this.props.data.coupon !== 'undefined') {
      await Clipboard.setString(this.props.data.coupon.alphanumeric_code);
      RNToasty.Success({
        title: t('Coupon:copied'),
        titleSize: 12,
      });
    }
  };

  private onImageLoaded = (event: NativeSyntheticEvent<ImageLoadEventData>): void => {
    this.setState({loaded: true});
  };

  private expire = (timestamp: number): number => {
    return differenceInDays(new Date(timestamp * 1000), new Date());
  };

  public render(): React.ReactNode {
    const { t, data, authorized } = this.props;

    if (typeof data.coupon === 'undefined') return null;
    const exp = this.expire( data.expiry_date );
    return (
      <View style={[styles.container, this.state.loaded ? {opacity: 1} : {opacity: 0}]}>
        <TouchableHighlight
          style = {[styles.top]}
          underlayColor = {'transparent'}
          onPress = {this.bannerPressed}
        >
          <CouponImage
            source = {data.coupon.image_id}
            onLoad = {this.onImageLoaded}
          />
        </TouchableHighlight>
        <View style={styles.bottom} onLayout = {this.centerButton}>
          <View style={styles.bottomLeft}>
            <Text style = {styles.codeLabel}>{t('couponCode')}</Text>
            <TouchableHighlight
              style = {styles.codeArea}
              underlayColor = {COLORS.white}
              onLongPress = {this.writeToClipboard}
            >
              <Text style = {styles.couponCode}>{data.coupon.alphanumeric_code}</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.bottomRight}>
            <Text style = {styles.expiresLabel}>{t('expires')}</Text>
            <Text style = {styles.expiresDate}>{exp + ' '}{exp === 1 ? t('day') : t('days')}</Text>
          </View>
          <TouchableHighlight
            style = {[styles.button, { left: this.state.left }]}
            underlayColor = {COLORS.lightMauve}
            onPress = {this.bannerPressed}
          >
            <Text style = {styles.buttonText}>{t(authorized ? 'redeem' : 'signup')}</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

}