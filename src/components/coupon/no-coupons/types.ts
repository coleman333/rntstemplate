import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  text:   string;
}

export interface State {
}
