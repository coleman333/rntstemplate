import React from 'react';
import { View, Text } from 'react-native';
import { Props, State } from './types';
import { styles } from './styles';

export class NoCoupons extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    text:   '',
  };

  public render(): React.ReactNode {
    const { text } = this.props;
    return (
      <View style = {styles.container}>
        <Text style = {styles.text}>{text}</Text>
      </View>
    );
  }

}