import {Dimensions, StyleSheet} from 'react-native';
import { COLORS } from 'src/constants/colors';


export const styles = StyleSheet.create({

  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
  },

  title: {
    textAlign: 'left',
    color: COLORS.navy,
    fontSize: 19,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
  },

  text: {
    marginTop: 5,
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 13.5,
    fontFamily: 'Poppins',
  },

});
