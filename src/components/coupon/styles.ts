import { StyleSheet, Dimensions, Platform } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

const RADIUS = 16;
const { width } = Dimensions.get('window');

export const styles = StyleSheet.create({

  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.navy,
    borderRadius: RADIUS,
  },

  top: {
    width: '100%',
    // borderTopLeftRadius: RADIUS,
    // borderTopRightRadius: RADIUS,
    backgroundColor: COLORS.white,
  },

  bottom: {
    flexGrow: 1,
    flexDirection: 'row',
    width: '100%',
    // borderBottomLeftRadius: RADIUS,
    // borderBottomRightRadius: RADIUS,
  },

  bottomRight: {
    width: '30%',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 15,
  },

  bottomLeft: {
    width: '70%',
    paddingLeft: 15,
    justifyContent: 'center',
  },

  codeArea: {
    width: 100,
    height: 23,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    backgroundColor: COLORS.white,
    borderColor: COLORS.lightMauve,
    borderStyle: 'dashed',
    borderRadius: 5,
  },

  button: {
    position: 'absolute',
    top: -12,
    left: '50%',
    width: 99,
    height: 24,
    borderRadius: 3,
    backgroundColor: COLORS.lightMauve,
    zIndex: 99999,
    shadowOffset:{  width: 2,  height: 4,  },
    shadowColor: 'black',
    shadowOpacity: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonText: {
    fontFamily: 'Poppins',
    fontSize: 11,
    fontWeight: '600',
    letterSpacing: 0.2,
    color: COLORS.navy,
  },

  expiresLabel: {
    fontFamily: 'Poppins',
    fontSize: 8,
    color: COLORS.white,
  },

  expiresDate: {
    fontFamily: 'Poppins',
    fontSize: 14,
    color: COLORS.white,
  },

  codeLabel: {
    fontFamily: 'Poppins',
    fontSize: 8.3,
    color: COLORS.white,
    marginBottom: 5,
  },

  couponCode: {
    fontFamily: 'Poppins',
    fontSize: 16,
    fontWeight: '500',
    color: COLORS.black,
  },

});
