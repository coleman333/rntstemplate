import { ViewProps } from 'react-native';
import { UserCoupon } from 'src/models/entities/Coupon';
import { TranslationFunction } from 'i18next';

export interface Props extends ViewProps {
  single: boolean;
  t: TranslationFunction;
  data: UserCoupon;
  authorized: boolean;
  couponEvent(data: object): void;
  onButtonPressed?(): void;
}

export interface State {
  left: number;
  loaded: boolean;
}
