import React from 'react';
import {Image, ImageLoadEventData, NativeSyntheticEvent, View, ViewProps} from 'react-native';

export interface Props extends ViewProps {
  source:   string;
  onLoad(event: NativeSyntheticEvent<ImageLoadEventData>): void;
}

export interface State {
  width:  number;
  height: number;
}

export class CouponImage extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    source: '',
    onLoad: () => {},
  };

  public state: State = {
    width:  0,
    height: 0,
  };

  private _onLayout = (event: any): void => {
    const containerWidth = event.nativeEvent.layout.width;
    if (this.props.source.length) {
      Image.getSize(this.props.source, (width, height) => {
        this.setState({
          width:  containerWidth,
          height: containerWidth * height / width
        });
      }, () => {
        console.log('CouponImage: Image.getSize() failed');
      });
    }
  };

  render() {
    return (
      <View onLayout={this._onLayout}>
        <Image
          source = {{uri: this.props.source}}
          style = {{
            width:  this.state.width,
            height: this.state.height
          }}
          onLoad = {this.props.onLoad}
        />
      </View>
    );
  }
}