import React from 'react';
import { ViewProps } from 'react-native';

export interface BodyProps extends ViewProps {
    pageIndex: number;
    pages: Array<React.ReactNode>;
}

export interface HeaderProps extends ViewProps {
    pageIndex: number;
    setPageIndex: Function;
    pagesNames: string[];
}
