import React from 'react';
import {Text, TextStyle, View, Button, TouchableOpacity, ScrollView} from 'react-native';
import { Icon } from 'react-native-material-ui';
import { BodyProps, HeaderProps } from './types';
import { HorizontalScroll } from '../horizontal-scroll';
import { ProductListItem } from "src/components/product/list-item";
import { ProductForConsumer } from 'src/models/entities/Product';
import { styles } from './styles';
import value from '*.json';

export class ScansListBody extends React.Component<BodyProps> {

  public render(): React.ReactNode {
    const {pageIndex} = this.props;

    return (
      <View
        style={{
          padding: 10,
        }}
      >
        {this.props.pages[pageIndex]}

        <View
          style={{
            alignItems: 'center',
          }}
        >
        </View>
      </View>
    );
  }
}

export class ScansListHeader extends React.Component<HeaderProps> {

  public static defaultProps: Partial<HeaderProps> = {
    pagesNames: ['default tab'],
  };

  public render(): React.ReactNode {
    const { setPageIndex, pageIndex, pagesNames } = this.props;
    return (
      <View
        style={{
          alignItems: 'center',
          marginTop: 10,
        }}
      >
        <Text
          style={{
            margin: 5,
            fontWeight: '900',
          }}
        >YOUR SCANS</Text>
        <View
          style={{
            marginTop: 10,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}
        >
          {pagesNames.map((v,i) => <TouchableOpacity
            onPress={() => setPageIndex(i)}
            key={'header-list-item-' + Math.random()}
          >
            <Text style={i === pageIndex ? styles.activeSelection : styles.passiveSelection}>{v}</Text>
          </TouchableOpacity>)}
        </View>
      </View>
    );
  }
}