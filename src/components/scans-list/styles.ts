import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
    activeSelection: {
        color: '#be7b91',
        fontWeight: '800',
        padding: 5,
        borderBottomWidth: 2,
        borderColor: '#be7b91',
    },
    passiveSelection: {
        color: '#b2b2b2',
        fontWeight: '800',
        padding: 5,
    }
});
