import React from 'react';
import { ScrollView, View } from 'react-native';

import { Props } from './types';
import { styles } from './styles';

export const HorizontalScroll: React.SFC<Props> = ({ children, innerRef, ...scrollProps }) => (
  <ScrollView
    showsHorizontalScrollIndicator={false}
    bounces={false}
    {...scrollProps}
    horizontal={true}
    ref={innerRef}
  >
    <View style={styles.container}>{children}</View>
  </ScrollView>
);
