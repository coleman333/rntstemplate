import React from 'react';
import { ScrollView, ScrollViewProps } from 'react-native';

export interface Props extends ScrollViewProps {
  innerRef?: React.Ref<ScrollView>;
}
