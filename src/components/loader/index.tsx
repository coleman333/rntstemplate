import React from 'react';
import { View, TouchableHighlight } from 'react-native';
import * as Progress from 'react-native-progress';
import { Props, State } from './types';
import { styles } from './styles';
import { COLORS } from "../../constants";

export class Loader extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    visible: false,
  };

  private onPress = (): void => {
    if (this.props.onPress) {
      this.props.onPress();
    }
  };

  public render(): React.ReactNode {
    if (this.props.visible) {
      return (
        <TouchableHighlight
          onPress = {this.onPress}
          underlayColor = {'transparent'}
          style = {styles.container}
        >
          <Progress.Circle
            size = {50}
            indeterminate = {true}
            borderWidth = {4}
            borderColor = {COLORS.lightMauve}
          />
        </TouchableHighlight>
      );
    } else {
      return null;
    }
  }

}