import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  visible: boolean;
  onPress?: Function;
}

export interface State {
}
