import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    zIndex: 9999,
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.black,
    opacity: 0.7,
  },
});
