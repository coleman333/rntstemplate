import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  title?: string;
  theme: 'dark' | 'light';
  onPress?(): void;
}
