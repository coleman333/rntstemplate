import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
  holder: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    flexDirection: 'row',
    width: '100%',
    height: 50,
    paddingLeft: 10,
  },

  left: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  center: {
    paddingLeft: 4,
    paddingBottom: 2,
    width: '50%',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },

  title: {
    fontSize: 14,
    fontWeight: '500',
  },

  dark: {
    color: COLORS.navy,
  },

  light: {
    color: COLORS.white,
  },
});
