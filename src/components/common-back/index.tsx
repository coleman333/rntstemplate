import React from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { COLORS } from 'src/constants/colors';
import { Props } from './types';
import { styles } from './styles';

export const CommonBack: React.SFC<Props> = ({ title, theme, onPress }) => (
    <TouchableHighlight underlayColor={'transparent'}  onPress={onPress}>
      <View style={styles.holder}>
        <Icon name="ios-arrow-back" size={16} color={theme === 'dark' ? COLORS.navy : COLORS.white} />
        <View style={styles.center}>{title && <Text style={[styles.title, styles[theme]]}>{title}</Text>}</View>
      </View>
    </TouchableHighlight>
);

CommonBack.defaultProps = {
  title: '',
  onPress: () => {},
};
