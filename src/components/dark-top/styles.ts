import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    backgroundColor: COLORS.gray,
  },

  topArea: {
    zIndex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomEndRadius: 900,
    borderBottomLeftRadius: 900,
    backgroundColor: COLORS.navy,
  },

  height100: {
    height: '100%',
  },

  height50: {
    height: '50%',
  },

  height30: {
    height: '30%',
  },

  halfCircle: {
    backgroundColor: COLORS.navy,
    width: '150%',
    height: 350,
    marginTop: -300,
    borderBottomEndRadius: 900,
    borderBottomLeftRadius: 900,
    elevation: 10,
  },

  svgArea: {
    zIndex: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
  },
});
