import React from 'react';
import { Animated, Dimensions, View } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { COLORS } from 'src/constants';
import { Props, State } from './types';
import { styles } from './styles';

const { width } = Dimensions.get('window');
const SVG_PATH = 'M0 0c106.05 53.333 217.716 53.333 ' + width + ' 0H0';
const ANIMATION_DURATION = 500; //ms
const INITIAL_HEIGHT = '100%';

export class DarkTop extends React.Component<Props, State> {
  public static defaultProps: Partial<Props> = {
    animate: false,
    rounded: true,
    beforeAnimate: () => {},
  };

  public state: State = {
    heightAnim: INITIAL_HEIGHT,
    finalHeight: 0,
  };

  // Component lifecycle methods

  public componentDidUpdate(prevProps: Props, prevState: State): void {
    if (this.props.animate && prevState.heightAnim === INITIAL_HEIGHT && this.state.heightAnim !== INITIAL_HEIGHT) {
      this.props.beforeAnimate();
      this.startAnimation();
      //setTimeout(this.startAnimation, 500);
    }
  }

  private percentsFrom = (from: number, percents: number): number => {
    return from / 100 * percents;
  };

  // Event handlers

  private onLayoutHandler = (event: any): void => {
    if (this.props.animate && this.state.heightAnim === INITIAL_HEIGHT) {
      this.setState({
        heightAnim: new Animated.Value(event.nativeEvent.layout.height),
        finalHeight: this.percentsFrom(event.nativeEvent.layout.height, 30),
      });
    }
  };

  // Helper functions

  private startAnimation = (): void => {
    Animated.timing(this.state.heightAnim as Animated.Value, {
      toValue: this.state.finalHeight,
      duration: ANIMATION_DURATION,
    }).start();
  };

  // RENDER

  public render(): React.ReactNode {
    const { animate, rounded, style, children } = this.props;
    const { heightAnim } = this.state;

    return (
      <React.Fragment>
        {animate === true && (
          <Animated.View
            style={[styles.topArea, styles.height100, style, { height: heightAnim }]}
            onLayout={this.onLayoutHandler}
          >
            {children}
          </Animated.View>
        )}
        {animate === false && (
          <View style={[styles.topArea, style]}>{children}</View>
        )}
        {rounded === true && (
          <View style={styles.svgArea}>
            <View
              style={styles.halfCircle}
            />
          </View>
        )}
      </React.Fragment>
    );
  }
}
