import { Animated, ViewProps } from 'react-native';

export interface Props extends ViewProps {
  animate?: boolean;
  rounded?: boolean;
  beforeAnimate?(): void;
}

export interface State {
  heightAnim: string | Animated.Value;
  finalHeight: number;
}
