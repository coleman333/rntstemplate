import { StyleSheet } from 'react-native';
import { COLORS, getFontFamily } from 'src/constants';

export const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.lightBorderColor,
    minWidth: 100,
    height: 32,
    paddingHorizontal: 13,
    marginHorizontal: 15,
  },

  text: {
    fontSize: 14.1,
    fontFamily: getFontFamily(),
    lineHeight: 20,
    letterSpacing: 0.3,
    color: COLORS.lightTextColor,
  },

  active: {
    borderColor: COLORS.black,
    backgroundColor: 'rgba(0, 0, 0, 0.08)',
  },
});
