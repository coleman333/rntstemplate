import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { Props } from './types';
import { styles } from './styles';

export const RoundedButton: React.SFC<Props> = ({ text, active, ...props }) => (
  <TouchableOpacity {...props} style={[styles.button, active && styles.active]}>
    <Text style={styles.text}>{text}</Text>
  </TouchableOpacity>
);

RoundedButton.defaultProps = {
  active: false,
};
