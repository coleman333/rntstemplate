import React from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { COLORS } from 'src/constants/colors';
import { Props } from './types';
import { styles } from './styles';

export const HeaderBack: React.SFC<Props> = ({ title, icon, onPress }) => (
  <View style={icon ? styles.holder2 : styles.holder1}>
    <TouchableHighlight underlayColor={'transparent'} style={styles.left} onPress={onPress}>
        <Icon name="ios-arrow-back" size={44} color={COLORS.white} />
    </TouchableHighlight>
    <View style={styles.center}>
      {icon}
      {title && <Text style={[styles.title, icon ? styles.titleMargins : {}]}>{title}</Text>}
    </View>
    <View style={styles.right} />
  </View>
);

HeaderBack.defaultProps = {
  title: '',
  onPress: () => {},
};
