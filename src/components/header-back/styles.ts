import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({


  holder1: {
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    flexDirection: 'row',
    width: '100%',
    height: 50,
  },

  holder2: {
    marginTop: 30,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    flexDirection: 'row',
    width: '100%',
    paddingTop: 10,
  },

  left: {
    width: '15%',
    alignItems: 'center',
  },

  center: {
    width: '50%',
    alignItems: 'center',
  },

  right: {
    width: '15%',
  },

  titleMargins: {
    marginTop: 60,
  },

  title: {
    color: COLORS.lightMauve,
    fontSize: 24,
    fontWeight: '500',
  },
});
