import React from 'react';
import { ViewProps } from 'react-native';

export interface Props extends ViewProps {
  title?: string;
  icon?: React.ReactNode;
  onPress?(): void;
}
