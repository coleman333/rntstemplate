import React from "react";
import { Modal, View, Text } from 'react-native';
import { IconToggle } from 'react-native-material-ui';
import { COLORS } from '../../constants';
import { HorizontalScroll } from 'src/components/horizontal-scroll';
import { ProductForConsumer } from '../../models/entities/Product';

import { Props, State } from "./types";
import { styles } from './styles';
import { ProductListItem } from '../product/list-item';

export class ClosestMatches extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    visible: false,
    title: '',
    message: '',
    data: [],
    onClose: () => {},
    onTap: () => {},
  };

  private onClose = (): void => {
    this.props.onClose();
  };

  private onRequestClose = (): void => {};

  public render(): React.ReactNode {
    const { visible, title, message, data } = this.props;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={this.onRequestClose}
      >
        <View style = {styles.container}>
          <View style = {styles.window}>
            <View style = {styles.titleArea}>
              <Text style = {styles.titleText}>{title}</Text>
            </View>
            <View style = {styles.bodyArea}>
              <Text style = {styles.bodyText}>{message}</Text>
            </View>
            <View style={ styles.productListContainer }>
              <HorizontalScroll>
                {data && data.map(
                  (item: ProductForConsumer, index: number): React.ReactElement<View> => (
                    <ProductListItem
                      product = { item }
                      onPress = { () => { this.props.onTap(item); } }
                      key = {'product-list-item-' + index}
                    />
                  )
                )}
              </HorizontalScroll>
            </View>
            <View style = { styles.buttonCloseArea }>
              <IconToggle
                color = { COLORS.icon }
                name = {'clear'}
                onPress = {this.onClose}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }

}
