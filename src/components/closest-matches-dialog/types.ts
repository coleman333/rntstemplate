import { ViewProps } from 'react-native';
import { ProductForConsumer } from 'src/models/entities/Product';

export interface Props extends ViewProps {
  visible: boolean;
  title: string,
  message: string,
  data: Array<ProductForConsumer>;
  onClose: Function;
  onTap: Function;
}

export interface State {
}
