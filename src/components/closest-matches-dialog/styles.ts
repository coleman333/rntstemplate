import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  window: {
    width: '80%',
    // height: '48%',
    backgroundColor: COLORS.white,
    borderWidth: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    borderRadius: 15,
  },

  buttonCloseArea: {
    position: 'absolute',
    top: -5,
    right: -5,
  },

  titleArea: {
    width: '100%',
    marginBottom: 20,
    paddingTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },

  titleText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 25,
    fontWeight: '700',
  },

  bodyArea: {
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },

  bodyText: {
    textAlign: 'center',
    color: COLORS.navy,
    fontSize: 16,
    marginBottom: 15,
  },

  productListContainer: {
    height: 230,
  },

});
