import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    backgroundColor: COLORS.gray,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },

  separator: {
    width: '100%',
    height: 2,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grayButton,
    marginTop: 20,
    marginBottom: 20,
  },
});
