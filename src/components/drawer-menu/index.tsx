import React from "react";
import { SafeAreaView, View, TouchableHighlight, Image } from 'react-native';
import { Navigation } from 'src/services/Navigation';
import { Routes } from 'src/navigation/route-names';
import { User } from 'src/components/user';
import { MenuItem } from 'src/components/drawer-menu/menu-item';
import { connect } from './connect';
import { Props, State } from "./types";
import { styles } from './styles';

@connect
export class Menu extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    logout: () => {},
  };

  private pressed_Home = (): void => {
    Navigation.navigateTo(Routes.HomeTabIndex);
  };

  private pressed_Wallet = (): void => {
    Navigation.navigateTo(Routes.WalletTab);
  };

  private pressed_Rewards = (): void => {
    Navigation.navigateTo(Routes.RewardsTab);
  };

  private pressed_Network = (): void => {
    Navigation.navigateTo(Routes.ProfileSettings);
  };

  private pressed_Scan = (): void => {
    Navigation.navigateTo(Routes.ScanTab);
  };

  private pressed_Logout = (): void => {
    this.props.logout();
  };



  public render(): React.ReactNode {
    const { state } = this.props.navigation;
    let userInfo = typeof this.props.auth !== 'undefined' && typeof this.props.auth.userProfile !== 'undefined' &&
      typeof this.props.auth.userProfile.first_name !== 'undefined';
    return (
      <SafeAreaView style = {styles.container}>
        {userInfo === true &&
        <User
          name  = {this.props.auth.userProfile.first_name + ' ' + this.props.auth.userProfile.last_name}
          email = {this.props.auth.userProfile.email}
        />
        }

        <View style = {styles.separator}></View>
        <MenuItem title = "Profile" icon = {'network'}  focused = {state.routes[0].index === 4} onPress={this.pressed_Network} />
        <MenuItem title = "Scan"    icon = {'scan'}     focused = {state.routes[0].index === 2} onPress={this.pressed_Scan} />
        <MenuItem title = "Wallet"  icon = {'wallet'}   focused = {state.routes[0].index === 1} onPress={this.pressed_Wallet} />
        <MenuItem title = "Rewards" icon = {'rewards'}  focused = {state.routes[0].index === 3} onPress={this.pressed_Rewards} />
        <MenuItem title = "Log out" icon = {'logout'}   focused = {state.routes[0].index === 5} onPress={this.pressed_Logout} />
      </SafeAreaView>
    );
  }

}