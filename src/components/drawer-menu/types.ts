import { ViewProps } from 'react-native';
import { IAuthState } from "src/redux/reducers/Auth";


export interface Props extends ViewProps {
  auth: IAuthState;
  logout: Function;
  navigation: any;
}

export interface State {
}
