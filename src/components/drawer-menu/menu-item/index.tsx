import React from "react";
import { View, TouchableHighlight, Text } from 'react-native';
import { TabIcon } from 'src/navigation/application/tab-icon';
import { Props, State } from "./types";
import { styles } from './styles';

export class MenuItem extends React.Component<Props, State> {

  public static defaultProps: Partial<Props> = {
    title: '',
    focused: false,
    onPress: () => {},
  };

  onPress = () : void => {
    if (typeof this.props.onPress !== 'undefined') {
      this.props.onPress();
    }
  };

  public render(): React.ReactNode {
    const { focused } = this.props;
    return (
      <TouchableHighlight
        style = {styles.wrapper}
        onPress = {this.onPress}
        underlayColor={'transparent'}
      >
        <View style = {styles.container}>
          <View style = {styles.iconHolder}>
            {typeof this.props.icon !== 'undefined' &&
            <TabIcon tab={this.props.icon} focused={focused} />
            }
          </View>
          <View style = {styles.linkHolder}>
            <Text style = {styles.linkText}>{this.props.title}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

}