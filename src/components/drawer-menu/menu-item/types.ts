import { ViewProps } from 'react-native';


export interface Props extends ViewProps {
  title: string;
  icon?: 'home' | 'wallet' | 'scan' | 'rewards' | 'network' | 'logout';
  focused: boolean;
  onPress?: Function;
}

export interface State {
}
