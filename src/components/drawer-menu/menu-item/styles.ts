import { StyleSheet } from 'react-native';
import { COLORS } from 'src/constants/colors';

export const styles = StyleSheet.create({

  wrapper: {
    height: '7%',
    width: '100%',
    justifyContent: 'center',
  },

  container: {
    height: '100%',
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },

  iconHolder: {
    width: '20%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  linkHolder: {
    width: '80%',
    height: '100%',
    paddingLeft: 16,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },

  linkText: {
    fontFamily: 'Montserrat',
    fontSize: 14,
    fontWeight: "500",
    color: COLORS.black,
    opacity: 0.9,

  },

});
