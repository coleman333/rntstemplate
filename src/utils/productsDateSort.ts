import Product from "src/models/entities/Product";

export type Year = {
    [key: number]: Array<Product>;
}

export type AlcoType = {
    [key: number]: Year;
}

export type Sorted = {
    [key: string]: AlcoType;
}

export const productsDateSort = (products: Array<any>) => {
    const sortedData = {} as Sorted;

    products.map((val) => {
        const time = new Date(val?.event?.request_extra_data?.datetime * 1000);
        const year = time.getFullYear();
        const month = time.getMonth();
        const category = val?.product?.category;

        sortedData[category] = !!sortedData[category] ? sortedData[category] :
            {} as AlcoType;
        sortedData[category][year] = !!sortedData[category][year] ? sortedData[category][year] : 
            {} as Year;
        sortedData[category][year][month] = !!sortedData[category][year][month] ? sortedData[category][year][month] :
            [] as Array<Product>;
        sortedData[category][year][month].push(val as Product);
    });
    
    // alert(JSON.stringify(sortedData));

    return sortedData;
}