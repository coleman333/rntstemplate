export const getImageURL = (images: Array<any>): string => {
  let image = images[0]?.image_id;
  for (let i = 0; i < images.length; i++) {
    if (images[i].image_type === 'FRONT') {
      image = images[i].image_id;
    }
  }
  return image;
};