import { connect } from 'react-redux';
import { AnyFunction } from 'src/types/common';

export const reduxify = (
  mapStateToProps: AnyFunction,
  mapDispatchToProps?: AnyFunction,
  mergeProps?: any,
  options?: any,
) => (target: any) =>
  connect(
    mapStateToProps as any,
    mapDispatchToProps as any,
    mergeProps,
    options,
  )(target) as any;
