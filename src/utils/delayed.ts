export const delayed = (promise: Promise<any>, delay: number = 1000): Promise<any> => {
  return Promise.all([promise, new Promise(resolve => setTimeout(resolve, delay))]).then(
    response => response[0],
  );
};
