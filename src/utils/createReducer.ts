import { AnyAction } from 'redux';
import { AnyObject } from 'src/types/common';

export const createReducer = (initialState: AnyObject) => (derivations: AnyObject = {}) => (
  state = initialState,
  action: AnyAction,
) => (derivations[action.type] || (() => state))(state, action);
