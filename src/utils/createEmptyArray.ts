export const createEmptyArray = (length: number = 0): undefined[] => [
  ...Array(length > 0 ? length : 0),
];
