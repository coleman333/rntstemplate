import React from 'react';
import { RootContainer } from 'src/containers/Root';
import { StatusBar } from 'react-native';

import { COLORS } from 'src/constants/colors';

StatusBar.setBackgroundColor(COLORS.navy);

export const App: React.SFC = () => <RootContainer />;
