declare module 'apollo-link-context';
declare module 'react-native-vector-icons/AntDesign';
declare module 'react-native-vector-icons/MaterialIcons';
declare module 'react-native-vector-icons/MaterialCommunityIcons';
declare module 'react-native-google-signin/src/GoogleSignin';
declare module 'react-navigation-drawer';
declare module 'react-native-autolink';
declare module 'apollo-upload-client';
declare module 'react-native-material-textfield';
declare module 'react-native-qrcode-scanner';
declare module 'react-native-star-rating';
declare module 'react-native-toasty';
//declare module '*.json';

declare module '*.json' {
  const value: any;
  export default value;
}
declare module 'json!*' {
  const value: any;
  export default value;
}
declare module '*.graphql' {
  const value: any;
  export default value;
}
declare module 'graphql!*' {
  const value: any;
  export default value;
}