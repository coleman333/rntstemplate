import { WithNamespaces } from 'react-i18next';
import { NavigationParams, NavigationScreenProp } from 'react-navigation';

export type WithTranslation = WithNamespaces;

interface NavigationState {
  key: string;
  routeName: string;
}
export type Navigation<Params = NavigationParams> = NavigationScreenProp<NavigationState, Params>;
