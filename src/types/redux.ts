import { State as GlobalState } from 'src/redux/reducers/global/types';

export interface AppState {
  global: GlobalState;
}

export interface IReduxState {
  authorized?: boolean;
  initial?: boolean;
  dispatch?: Function;
  error?: any;
  pending?: boolean;
  fulfilled?: boolean;
  rejected?: boolean;
}
