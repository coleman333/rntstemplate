export interface IGetUser {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  birth_date: number;
  country: string;
  image_id: string;
  tokens_amount: number;
  organization_id: number;
  age_range_from: number;
  age_range_to: number;
  gender: string;
  location: Object;
}

export interface IGetSocialApps {
  getSocialApps: {
    facebook: string;
    google: string;
  };
}

export interface ILogin {
  auth_token: boolean;
}

export interface IUpdateProfile {
  birth_date: number;
  country: string;
  email: string;
  first_name: string;
  id: number;
  image_id: string;
  last_name: string;
  tokens_amount: number;
}