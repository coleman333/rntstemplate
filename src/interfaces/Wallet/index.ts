export interface IWallet {
  balanceOMZ: number;
  balanceCurrency: number;
  conversationRate: number;
}

