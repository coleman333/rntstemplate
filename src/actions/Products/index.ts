import {
  PAGE_PRODUCT_DETAILS_ADD_REVIEW_REQUEST,
  PAGE_PRODUCT_DETAILS_GET_PRODUCT_REQUEST,
  PAGE_PRODUCT_DETAILS_SCAN_REQUEST,
  PAGE_PRODUCT_DETAILS_RESET_PRODUCT_REQUEST,
  COPY_PRODUCT_DETAILS,
  GET_PRODUCT_BY_PHOTO_REQUEST,
} from 'src/constants/ActionTypes/Products';
import { ProductService } from 'src/services/Products';
import { PublicScanParams } from 'src/models/entities/ScanParams';
import { ProductForConsumer } from 'src/models/entities/Product';

export const getProductByPhotoPendingState = {
  type: GET_PRODUCT_BY_PHOTO_REQUEST + '_PENDING',
};

export const dataActionPendingState = {
  type: PAGE_PRODUCT_DETAILS_ADD_REVIEW_REQUEST + '_PENDING',
};

export const getProductPendingState = {
  type: PAGE_PRODUCT_DETAILS_GET_PRODUCT_REQUEST + '_PENDING',
};

export const scanActionPendingState = {
  type: PAGE_PRODUCT_DETAILS_SCAN_REQUEST + '_PENDING',
};

export const resetProductActionState = {
  type: PAGE_PRODUCT_DETAILS_RESET_PRODUCT_REQUEST + '_FULFILLED',
};
/*
export const reviewAction = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    ProductService.reviewAction(
      PAGE_PRODUCT_DETAILS_ADD_REVIEW_REQUEST,
      dispatch,
      data,
    );
    dispatch(dataActionPendingState);
    return dataActionPendingState;
  };
};
*/
export const getProductAction = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    ProductService.getProduct(PAGE_PRODUCT_DETAILS_GET_PRODUCT_REQUEST, dispatch, data);
    dispatch(getProductPendingState);
    return getProductPendingState;
  };
};


export const scanAction = function(data: PublicScanParams) {
  return (dispatch: Function, getState: Function) => {
    ProductService.getScanProduct(PAGE_PRODUCT_DETAILS_SCAN_REQUEST, dispatch, data);
    dispatch(scanActionPendingState);
    return scanActionPendingState;
  };
};

export const getProductByPhoto = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    ProductService.getProductByPhoto(GET_PRODUCT_BY_PHOTO_REQUEST, dispatch, data);
    dispatch(getProductByPhotoPendingState);
    return getProductByPhotoPendingState;
  };
};



export const resetProduct = function() {
  return (dispatch: Function, getState: Function) => {
    dispatch(resetProductActionState);
    return resetProductActionState;
  };
};

export const copyProductDetails = function (data: ProductForConsumer) {
  return (dispatch: Function, getState: Function) => {
    dispatch({type: COPY_PRODUCT_DETAILS, payload: data});
  };
};
