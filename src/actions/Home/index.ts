import { ACTION_HOME_GET_DATA } from 'src/constants/ActionTypes/Home';
import { HomeService } from 'src/services/Home';

export const getHomePageActionState = {
  type: ACTION_HOME_GET_DATA + '_PENDING',
};

export const getHomeData = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    HomeService.getData(ACTION_HOME_GET_DATA, dispatch, data);
    dispatch(getHomePageActionState);
    return getHomePageActionState;
  };
};
