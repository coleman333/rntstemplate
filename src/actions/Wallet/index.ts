import {
  GET_BALANCE,
} from 'src/constants/ActionTypes/Wallet';
import {WalletService} from 'src/services/Wallet';

export const getBalanceState = {
  type: GET_BALANCE + '_PENDING',
};

// TODO: add debounce here and to all actions initiating requests to API:

export const getBalance = function(data: any)
{
  return (dispatch: Function, getState: Function) => {
    WalletService.getBalance(
      GET_BALANCE, dispatch, data
    );
    dispatch(getBalanceState);
  };
};

