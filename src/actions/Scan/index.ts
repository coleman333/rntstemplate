import {
  SCAN_PARAMS_CLEAR,
  SCAN_PARAMS_SAVE,
  SCAN_HISTORY_GET
} from 'src/constants/ActionTypes/Scan';

import { RewardsService } from 'src/services/Scans';

import {ScanParamsObject} from "src/screens/ScanTab/types";

export const getScanHistoryPendingState = {
  type: SCAN_HISTORY_GET + '_PENDING',
}

export const loadScanHistory = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    RewardsService.getScanHistory(SCAN_HISTORY_GET, dispatch, data);
    dispatch(getScanHistoryPendingState);
    return getScanHistoryPendingState;
  }
}

export const saveScanParams = function(params: ScanParamsObject) {
  return (dispatch: Function, getState: Function) => {
    dispatch({type: SCAN_PARAMS_SAVE, payload: params});
  };
};

export const clearScanParams = function(params: ScanParamsObject) {
  return (dispatch: Function, getState: Function) => {
    dispatch({type: SCAN_PARAMS_CLEAR});
  };
};

