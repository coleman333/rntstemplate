import {
  SET_REVIEW,
} from 'src/constants/ActionTypes/Rating';
import {ProductModalRatingService} from 'src/services/Rating';

export const mutateReviewState = {
  type: SET_REVIEW + '_PENDING',
};

// TODO: add debounce here and to all actions initiating requests to API:

export const reviewAction = function(data: any)
{
  return (dispatch: Function, getState: Function) => {
    ProductModalRatingService.reviewAction(
      SET_REVIEW, dispatch, data
    );
    dispatch(mutateReviewState);
  };
};
