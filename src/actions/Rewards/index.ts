import { PAGE_REWARDS_GET_DATA } from 'src/constants/ActionTypes/Rewards';
import { RewardsService } from 'src/services/Rewards';

export const getRewardsPageActionState = {
  type: PAGE_REWARDS_GET_DATA + '_PENDING',
};

export const getRewards = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    RewardsService.getRewardsData(PAGE_REWARDS_GET_DATA, dispatch, data);
    dispatch(getRewardsPageActionState);
    return getRewardsPageActionState;
  };
};
