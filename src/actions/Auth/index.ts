import {
  USER_PROFILE_GET_INITIAL_REQUEST,
  USER_PROFILE_GET_PROFILE_REQUEST,
  USER_PROFILE_UPDATE_PROFILE_REQUEST,
  LOGOUT_REQUEST,
  DELETE_REQUEST,
  SOCIAL_LOGIN_REQUEST,
  GET_SOCIAL_IDS_REQUEST,
  AUTH_SET_INITIAL,
  AUTH_SET_DEEP_LINK,
} from 'src/constants/ActionTypes/Auth';
import {AuthService} from 'src/services/Auth';

export const getUserProfileActionState = {
  type: USER_PROFILE_GET_PROFILE_REQUEST + '_PENDING',
};

export const getUserProfileInitialState = {
  type: USER_PROFILE_GET_INITIAL_REQUEST + '_PENDING',
};

export const updateUserProfileActionState = {
  type: USER_PROFILE_UPDATE_PROFILE_REQUEST + '_PENDING',
};

export const logoutActionState = {
  type: LOGOUT_REQUEST + '_PENDING',
};

export const deleteActionState = {
  type: DELETE_REQUEST + '_PENDING',
};

export const loginPendingState = {
  type: SOCIAL_LOGIN_REQUEST + '_PENDING',
};

export const getSocialIdsPendingState = {
  type: GET_SOCIAL_IDS_REQUEST + '_PENDING',
};

// TODO: add debounce here and to all actions initiating requests to API:

export const setInitialAuth = function(initial: boolean) {
  return (dispatch: Function, getState: Function) => {
    dispatch({type: AUTH_SET_INITIAL, payload: {initial: initial}});
  };
};

export const setDeepLink = function(link: string) {
  return (dispatch: Function, getState: Function) => {
    dispatch({type: AUTH_SET_DEEP_LINK, payload: {deeplink: link}});
  };
};

export const getProfile = function(initial: boolean) {
  console.log('initial', initial);
  return (dispatch: Function, getState: Function) => {
    AuthService.getProfile(
      initial ? USER_PROFILE_GET_INITIAL_REQUEST : USER_PROFILE_GET_PROFILE_REQUEST,
      dispatch
    );
    dispatch(initial ? getUserProfileInitialState : getUserProfileActionState);
    return initial ? getUserProfileInitialState : getUserProfileActionState;
  };
};

export const updateUserProfile = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    // AuthService.updateProfile(USER_PROFILE_UPDATE_PROFILE_REQUEST, dispatch, data);
    // dispatch(updateUserProfileActionState);
    // return updateUserProfileActionState;
  };
};

export const logout = function() {
  return (dispatch: Function, getState: Function) => {
    AuthService.logout(LOGOUT_REQUEST, dispatch);
    dispatch(logoutActionState);
    return logoutActionState;
  };
};


export const socialLoginAction = function(data: any) {
  return (dispatch: Function, getState: Function) => {
    AuthService.login(SOCIAL_LOGIN_REQUEST, dispatch, data);
    dispatch(loginPendingState);
    return loginPendingState;
  };
};

// export const deleteProfileAction = function() {
//   return (dispatch: Function, getState: Function) => {
//     AuthService.delete(DELETE_REQUEST, dispatch);
//     dispatch(deleteActionState);
//     return deleteActionState;
//   };
// };

export const deleteProfileAction = function() {
  return async (dispatch: Function, getState: Function) => {
    try {
      dispatch(deleteActionState);
      await AuthService.delete(DELETE_REQUEST, dispatch);
      logout()(dispatch, getState);
      return deleteActionState;
    } catch (ex) {
      /*do nothing*/
    }
  };
};

export const getSocialIdsAction = function() {
  return async (dispatch: Function, getState: Function) => {
    let response = await AuthService.getSocialIds();
    if (response.error) {
      dispatch({
        type: GET_SOCIAL_IDS_REQUEST + '_REJECTED',
        payload: response.error
      });
    } else {
      dispatch({
        type: GET_SOCIAL_IDS_REQUEST + '_FULFILLED',
        payload: response.data,
      });
    }
  };
};
