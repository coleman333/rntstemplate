import settings from 'src/env.json';


//DEVELOPMENT
const DEV = {
  // GATEWAY_URL:  'https://gateway.dev.omniaz.io/api/',
  GATEWAY_URL:  'https://',

  // ORIGIN:       'omniaz.io',
  ORIGIN:       '',
  APP:          settings,
  PRODUCTION:   false,
};

//PRODUCTION
const PROD = {
  // GATEWAY_URL:  'https://gateway.letsdrnk.com/api/',
  GATEWAY_URL:  'https://',
  // ORIGIN:       'letsdrnk.com',
  ORIGIN:       '',
  APP:          settings,
  PRODUCTION:   true,
};

export const config = settings.env.toLowerCase() === 'dev' ? DEV : PROD;
