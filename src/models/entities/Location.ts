import {LocationType} from '../../enums';

export interface NewLocation {
  address: string;
  country: string;
  location_type: LocationType;
  latitude: number;
  longitude: number;
}

export class Location extends Object {
  public id: number;
  public address: string;
  public country: string;
  public location_type: LocationType;
  public latitude: number;
  public longitude: number;

  constructor(args: Readonly<Partial<Location>>) {
    super();
    this.id = args.id;
    this.address = args.address;
    this.country = args.country;
    this.location_type = args.location_type;
    this.latitude = args.latitude;
    this.longitude = args.longitude;
  }
}

export default Location;
