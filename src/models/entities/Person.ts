export class Person extends Object {
  public id: number;
  public firstName: string;
  public lastName: string;
  public email: string;

  constructor(args: Readonly<Partial<Person>>) {
    super();
    this.id = args.id;
    this.firstName = args.firstName;
    this.lastName = args.lastName;
    this.email = args.email;
  }
}

export default Person;
