import {Reason} from '../../enums';

export class TokenTransaction extends Object {
  public id: number;
  public sender_user_id: number;
  public receiver_user_id: number;
  public amount: number;
  public reason: Reason;
  public create_time: number;
  public extra_data: {[key: string]: string};

  constructor(args: Readonly<Partial<TokenTransaction>>) {
    super();
    this.id = args.id;
    this.sender_user_id = args.sender_user_id;
    this.receiver_user_id = args.receiver_user_id;
    this.amount = args.amount;
    this.reason = args.reason;
    this.create_time = args.create_time;
    this.extra_data = args.extra_data;
  }
}

export default TokenTransaction;
