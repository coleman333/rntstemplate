import {SocialPlatform, SocialActionType} from '../../enums';
import {SocialBountyTranslation} from './SocialBountyTranslation';

export interface NewSocialBounty {
  number_tokens_reward: number;
  platform: SocialPlatform;
  action_type: SocialActionType;
  URL: string;
  verification_timeout: number;
  claims_per_user: number;
  social_bounty_translations: SocialBountyTranslation[];
}

export class SocialBounty extends Object {
  public id: number;
  public number_tokens_reward: number;
  public platform: SocialPlatform;
  public action_type: SocialActionType;
  public URL: string;
  public verification_timeout: number;
  public claims_per_user: number;
  public social_bounty_translations: SocialBountyTranslation[];

  constructor(args: Readonly<Partial<SocialBounty>>) {
    super();
    this.id = args.id;
    this.number_tokens_reward = args.number_tokens_reward;
    this.platform = args.platform;
    this.action_type = args.action_type;
    this.URL = args.URL;
    this.verification_timeout = args.verification_timeout;
    this.claims_per_user = args.claims_per_user;
    this.social_bounty_translations = args.social_bounty_translations;
  }
}

export default SocialBounty;
