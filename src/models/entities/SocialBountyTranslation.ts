export interface NewSocialBountyTranslation {
  language: string;
  instructions: string;
  extras: string;
  image_id: string;
  reward_message: string;
  title: string;
  subtitle: string;
}

export class SocialBountyTranslation extends Object {
  public id: number;
  public language: string;
  public instructions: string;
  public extras: string;
  public image_id: string;
  public reward_message: string;
  public title: string;
  public subtitle: string;

  constructor(args: Readonly<Partial<SocialBountyTranslation>>) {
    super();
    this.id = args.id;
    this.language = args.language;
    this.instructions = args.instructions;
    this.extras = args.extras;
    this.image_id = args.image_id;
    this.reward_message = args.reward_message;
    this.title = args.title;
    this.subtitle = args.subtitle;
  }
}

export default SocialBountyTranslation;
