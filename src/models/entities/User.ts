import { Gender } from 'src/constants/enums';

export class User extends Object {
  public first_name: string;
  public last_name: string;
  public email: string;
  public birth_date: number;
  public country: string;
  public image_id: string;
  public organization_id: number;
  public tokens_amount: number;
  public age_range_from: number;
  public age_range_to: number;
  public gender: Gender;
  public location: UserLocation;

  constructor(args: Readonly<Partial<User>>) {
    super();
    this.first_name = args.first_name;
    this.last_name = args.last_name;
    this.email = args.email;
    this.birth_date = args.birth_date;
    this.country = args.country;
    this.country = args.country;
    this.image_id = args.image_id;
    this.organization_id = args.organization_id;
    this.tokens_amount = args.tokens_amount;
    this.age_range_from = args.age_range_from;
    this.age_range_to = args.age_range_to;
    this.gender = args.gender;
    this.location = args.location;
  }
}

export interface UserLocation {
  city: string;
}

export default User;
