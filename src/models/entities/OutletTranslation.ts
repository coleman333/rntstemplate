export interface NewOutletTranslation {
  language: string;
  display_name: string;
  URL: string;
  title: string;
  subtitle: string;
  facts: {[key: string]: string};
}

export class OutletTranslation extends Object {
  public id: number;
  public language: string;
  public display_name: string;
  public URL: string;

  constructor(args: Readonly<Partial<OutletTranslation>>) {
    super();
    this.id = args.id;
    this.language = args.language;
    this.display_name = args.display_name;
    this.URL = args.URL;
  }
}

export default OutletTranslation;
