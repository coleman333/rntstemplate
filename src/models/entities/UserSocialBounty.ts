import {UserSocialBountyStatus} from 'src/constants/enums';
import {SocialBounty} from './SocialBounty';
import {User} from './User';

export interface NewUserSocialBounty {
  user_id: number;
  social_bounty: SocialBounty;
  user_social_bounty_status: UserSocialBountyStatus;
  user_input: string;
}

export interface UserSocialBountyForConsumer {
  id: number;
  user: User;
  social_bounty: SocialBounty;
  user_social_bounty_status: UserSocialBountyStatus;
  user_input: string;
}

export class UserSocialBounty extends Object {
  public id: number;
  public user_id: number;
  public social_bounty: SocialBounty;
  public user_social_bounty_status: UserSocialBountyStatus;
  public user_input: string;

  constructor(args: Readonly<Partial<UserSocialBounty>>) {
    super();
    this.id = args.id;
    this.user_id = args.user_id;
    this.user_social_bounty_status = args.user_social_bounty_status;
    this.user_input = args.user_input;
  }
}

export default UserSocialBounty;
