export interface NewOrganizationTranslation {
  language: string;
  display_name: string;
  description: string;
  image_id: string;
  URL: string;
  title: string;
  subtitle: string;
  facts: {[key: string]: string};
  vineyard_description: string;
}

export class OrganizationTranslation extends Object {
  public id: number;
  public language: string;
  public display_name: string;
  public description: string;
  public image_id: string;
  public URL: string;
  public reward_message: string;
  public title: string;
  public subtitle: string;
  public facts: {[key: string]: string};
  public vineyard_description: string;

  constructor(args: Readonly<Partial<OrganizationTranslation>>) {
    super();
    this.id = args.id;
    this.language = args.language;
    this.display_name = args.display_name;
    this.description = args.description;
    this.image_id = args.image_id;
    this.URL = args.URL;
    this.reward_message = args.reward_message;
    this.title = args.title;
    this.subtitle = args.subtitle;
    this.facts = args.facts;
    this.vineyard_description = args.vineyard_description;
  }
}

export default OrganizationTranslation;
