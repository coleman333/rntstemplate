import {DocumentType} from '../../enums';

export class Document extends Object {
  public document_type: DocumentType;

  constructor(args: Readonly<Partial<Document>>) {
    super();
    this.document_type = args.document_type;
  }
}

export default Document;
