export interface ScanParams {
  namespace: string;
  identity: string;
  random_number: string;
  access_counter: string;
}

export interface PublicScanParams {
  n: string;
  i: string;
  r: string;
  c: string;
}

export default ScanParams;
