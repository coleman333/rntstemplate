// tslint:disable: max-classes-per-file
import {ReviewerType, RevieweeType} from 'src/constants/enums';

export interface Reviewer {
  reviewer_type: ReviewerType;
}

export interface Reviewee {
  reviewee_type: RevieweeType;
  reviewee_id: number;
}

export interface NewReview {
  reviewer: Reviewer;
  reviewee: Reviewee;
  numerical_rating: number;
  title: string;
  description: string;
  review_date: number;
}

export interface RevieweeRating {
  reviewee: Reviewee;
  rating: number;
  count: number;
}

export class Review extends Object {
  public id: number;
  public reviewer: Reviewer;
  public reviewee: Reviewee;
  public numerical_rating: number;
  public title: string;
  public description: string;
  public review_date: number;

  constructor(args: Readonly<Partial<Review>>) {
    super();
    this.id = args.id;
    this.reviewer = args.reviewer;
    this.reviewee = args.reviewee;
    this.numerical_rating = args.numerical_rating;
    this.title = args.title;
    this.description = args.description;
    this.review_date = args.review_date;
  }
}

export default Review;
