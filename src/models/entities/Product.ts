// tslint:disable: max-classes-per-file
import {Namespace, OwnerType, ProductImageType} from 'src/constants/enums';
import {Organization} from './Organization';

export interface ProductTranslation {
  language: string;
  short_description: string;
  long_description: string;
  image_id: string;
}

export interface ProductImage {
  id: number;
  image_type: ProductImageType;
  image_id: string;
}

export interface ProductOrganization {
  owner_type: OwnerType;
  organization_id: number;
  SKU: string;
  GTIN: string;
}

export interface ProductAward {
  id: number;
  text: string;
  image_id: string;
}

export interface Wine {
  serving_suggestion: string;
  grape_varietal: string;
  vintage: string;
  vineyard: string;
  aroma: string;
}

export interface Beer {
  IBU: number;
  estimated_calories: number;
  aromas: string;
}

export interface Spirits {
  age: number;
  palate: string;
}

export interface Aromas {
  id: number;
  tag_text: string;
  count: number;
}

export interface UserReviews {
  id: number;
  reviewee?: any;
  reviewer?: any;
  description?: string;
  review_date?: number;
  numerical_rating?: number;
}

export interface NewProduct {
  product_organizations: ProductOrganization[];
  product_translations: ProductTranslation[];
  omniaz_sku: string;
  namespace: Namespace;
  category: string;
  subcategory: string;
  average_price: number;
  average_price_currency: string;
  alcohol_content: number;
  year: number;
  volume_in_milliliters: number;
  food_pairings: string[];
  tasting_note: string;
  country: string;
  region: string;
  product_images: ProductImage[];
  awards: ProductAward[];
  user_rating: number;
  reviews_count: number;
  omniazed: boolean;
  featured: boolean;
  name: string;
  variant: number;
  label_notes: string;
  wine?: Wine;
  beer?: Beer;
  spirits?: Spirits;
}

export interface ProductForConsumer {
  id: number;
  organizations: () => Promise<Organization[]>;
  product_translations: ProductTranslation[];
  omniaz_sku: string;
  namespace: Namespace;
  category: string;
  subcategory: string;
  average_price: number;
  average_price_currency: string;
  alcohol_content: number;
  year: number;
  volume_in_milliliters: number;
  food_pairings: string[];
  tasting_note: string;
  country: string;
  region: string;
  product_images: ProductImage[];
  awards: ProductAward[];
  user_rating: number;
  reviews_count: number;
  omniazed: boolean;
  featured: boolean;
  name: string;
  variant: number;
  label_notes: string;
  wine?: Wine;
  beer?: Beer;
  spirits?: Spirits;
}

export class Product extends Object {
  public id: number;
  public product_organizations: ProductOrganization[];
  public product_translations: ProductTranslation[];
  public omniaz_sku: string;
  public namespace: Namespace;
  public category: string;
  public subcategory: string;
  public aromas: Array<Aromas>;
  public average_price: number;
  public average_price_currency: string;
  public alcohol_content: number;
  public year: number;
  public volume_in_milliliters: number;
  public food_pairings: string[];
  public tasting_note: string;
  public country: string;
  public region: string;
  public product_images: ProductImage[];
  public awards: ProductAward[];
  public user_rating: number;
  public reviews_count: number;
  public omniazed: boolean;
  public featured: boolean;
  public name: string;
  public variant: number;
  public label_notes: string;
  public wine?: Wine;
  public beer?: Beer;
  public spirits?: Spirits;
  public user_reviews?: Array<UserReviews>;
  public user_review?: UserReviews;
  public organizations?: Organization[];

  constructor(args: Readonly<Partial<Product>>) {
    super();
    this.id = args.id;
    this.product_organizations = args.product_organizations;
    this.product_translations = args.product_translations;
    this.omniaz_sku = args.omniaz_sku;
    this.namespace = args.namespace;
    this.category = args.category;
    this.subcategory = args.subcategory;
    this.average_price = args.average_price;
    this.average_price_currency = args.average_price_currency;
    this.alcohol_content = args.alcohol_content;
    this.year = args.year;
    this.volume_in_milliliters = args.volume_in_milliliters;
    this.food_pairings = args.food_pairings;
    this.tasting_note = args.tasting_note;
    this.country = args.country;
    this.region = args.region;
    this.product_images = args.product_images;
    this.awards = args.awards;
    this.user_rating = args.user_rating;
    this.reviews_count = args.reviews_count;
    this.omniazed = args.omniazed;
    this.featured = args.featured;
    this.name = args.name;
    this.variant = args.variant;
    this.label_notes = args.label_notes;
    this.wine = args.wine;
    this.beer = args.beer;
    this.spirits = args.spirits;
    this.user_reviews = args.user_reviews;
    this.organizations = args.organizations;
  }
}

export default Product;
