import {OutletType} from '../../enums';
import {OutletTranslation} from './OutletTranslation';
import {Location} from './Location';

export interface PhysicalOutlet {
  location: Location;
  opening_hours: string;
  closing_hours: string;
}

export interface NewOutlet {
  outlet_type: OutletType;
  outlet_translations: OutletTranslation[];
  physical_outlet: PhysicalOutlet;
}

export class Outlet extends Object {
  public id: number;
  public outlet_type: OutletType;
  public outlet_translations: OutletTranslation[];
  public physical_outlet: PhysicalOutlet;

  constructor(args: Readonly<Partial<Outlet>>) {
    super();
    this.id = args.id;
    this.outlet_type = args.outlet_type;
    this.outlet_translations = args.outlet_translations;
    this.physical_outlet = args.physical_outlet;
  }
}

export default Outlet;
