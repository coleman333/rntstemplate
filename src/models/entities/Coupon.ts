import {CouponStatus, CouponType} from 'src/constants/enums';
import {Product} from './Product';
import {Organization} from './Organization';

export interface NewCoupon {
  organization_id: number;
  product_id: number;
  alphanumeric_code: string;
  URL: string;
  title: string;
  image_id: string;
  expiry_date: number;
  coupon_type: CouponType;
  campaign_id: number;
}

export interface CouponForConsumer {
  id: number;
  organization: () => Promise<Organization>;
  product: () => Promise<Product>;
  alphanumeric_code: string;
  URL: string;
  title: string;
  image_id: string;
  expiry_date: number;
  coupon_type: CouponType;
  campaign_id: number;
}

export class Coupon extends Object {
  public id: number;
  public organization_id: number;
  public product_id: number;
  public alphanumeric_code: string;
  public URL: string;
  public title: string;
  public image_id: string;
  public expiry_date: number;
  public coupon_type: CouponType;
  public campaign_id: number;

  constructor(args: Readonly<Partial<Coupon>>) {
    super();
    this.id = args.id;
    this.organization_id = args.organization_id;
    this.product_id = args.product_id;
    this.alphanumeric_code = args.alphanumeric_code;
    this.URL = args.URL;
    this.title = args.title;
    this.image_id = args.image_id;
    this.expiry_date = args.expiry_date;
    this.coupon_type = args.coupon_type;
    this.campaign_id = args.campaign_id;
  }
}

export interface UserCoupon {
  id: number;
  coupon: Coupon;
  coupon_status: CouponStatus;
  expiry_date: number;
}

export default Coupon;
