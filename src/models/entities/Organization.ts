import {BusinessType, RoleType, OrganizationImageType} from 'src/constants/enums';
import {Location} from './Location';
import {OrganizationTranslation} from './OrganizationTranslation';
import {Outlet} from './Outlet';

export interface OrganizationImage {
  id: number;
  image_type: OrganizationImageType;
  image_id: string;
}

export interface NewOrganization {
  organization_translations: OrganizationTranslation[];
  locations: Location[];
  primary_contact: number;
  designation_user_id: number;
  role_type: RoleType;
  business_type: BusinessType;
  email: string;
  images: OrganizationImage[];
  outlets: Outlet[];
  user_rating: number;
  reviews_count: number;
}

export class Organization extends Object {
  public id: number;
  public organization_translations: OrganizationTranslation[];
  public locations: Location[];
  public primary_contact: number;
  public designation_user_id: number;
  public role_type: RoleType;
  public business_type: BusinessType;
  public email: string;
  public images: OrganizationImage[];
  public outlets: Outlet[];
  public user_rating: number;
  public reviews_count: number;

  constructor(args: Readonly<Partial<Organization>>) {
    super();
    this.id = args.id;
    this.organization_translations = args.organization_translations;
    this.locations = args.locations;
    this.primary_contact = args.primary_contact;
    this.designation_user_id = args.designation_user_id;
    this.role_type = args.role_type;
    this.business_type = args.business_type;
    this.email = args.email;
    this.images = args.images;
    this.outlets = args.outlets;
    this.user_rating = args.user_rating;
    this.reviews_count = args.reviews_count;
  }
}

export default Organization;
