export class Locale extends Object {
  public language: string;
  public country: string;

  constructor(args: Readonly<Partial<Locale>>) {
    super();
    this.language = args.language;
    this.country = args.country;
  }
}

export default Locale;
