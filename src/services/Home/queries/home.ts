import gql from 'graphql-tag'

export default gql`
    query homePageQueries($userCoupons: ListUserCouponsRequest!, $productList: ListProductsRequest!) {
        listUserCoupons(input: $userCoupons) {
            user_coupons {
                id
                coupon {
                    id
                    organization_id
                    alphanumeric_code
                    URL
                    title
                    image_id
                    created_on
                    expiry_date
                    coupon_type
                    campaign_id
                }
                coupon_status
                expiry_date
            }
            next_page_token
            prev_page_token
        }
        listProducts(input: $productList) {
            products {
                id
                product_organizations {
                    owner_type
                    organization_id
                    SKU
                    GTIN
                }
                product_translations {
                    language
                    short_description
                    long_description
                    image_id
                }
                omniaz_sku
                namespace
                category
                subcategory
                average_price
                average_price_currency
                alcohol_content
                year
                volume_in_milliliters
                food_pairings {
                    id
                    tag_text
                    count
                }
                tasting_note
                country
                region
                product_images {
                    id
                    image_type
                    image_id
                }
                awards {
                    id
                    text
                    image_id
                }
                user_rating
                reviews_count
                omniazed
                featured
                name
                variant
                label_notes
                aromas {
                    id
                    tag_text
                    count
                }
                wine {
                    serving_suggestion
                    grape_varietal
                    vintage
                    vineyard
                    aroma
                }
                beer {
                    IBU
                    estimated_calories
                    aromas
                }
                spirits {
                    age
                    palate
                }
            }
            next_page_token
            prev_page_token
        }
    }
`