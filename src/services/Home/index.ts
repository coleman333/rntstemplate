import {client} from '../Apollo';
import GET_HOME_SCREEN_DATA from './queries/home';

export class HomeService {
  public static async getData(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<any> {
    try {
      const result: any = await client.query({
        query: GET_HOME_SCREEN_DATA,
        variables: {
          ...query,
        },
      });
      console.log('GQL result', result);
      const data = {
        coupons: result.data.listUserCoupons && result.data.listUserCoupons.user_coupons ? result.data.listUserCoupons.user_coupons : [],
        products: result.data.listProducts && result.data.listProducts.products ? result.data.listProducts.products : [],
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('PagesHomeService->listUserCoupons->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
}
