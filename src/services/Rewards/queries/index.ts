import gql from 'graphql-tag'

export default gql`
    query rewardsPageQueries($userCoupons: ListUserCouponsRequest!) {
        listUserCoupons(input: $userCoupons) {
            user_coupons {
                id
                coupon {
                    id
                    organization_id
                    alphanumeric_code
                    URL
                    title
                    image_id
                    created_on
                    expiry_date
                    coupon_type
                    campaign_id
                }
                coupon_status
                expiry_date
            }
            next_page_token
            prev_page_token
        }
    }
`