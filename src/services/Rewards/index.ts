import { client } from '../Apollo';
import GET_REWARDS_PAGE_DATA from './queries';

export class RewardsService {
  public static async getRewardsData(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<any> {
    try {
      const result: any = await client.query({
        query: GET_REWARDS_PAGE_DATA,
        variables: {
          ...query,
        },
      });
      const data = {
        coupons: (result.data.listUserCoupons && result.data.listUserCoupons.user_coupons) ?
          result.data.listUserCoupons.user_coupons : [],
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('PagesRewardsService->listUserCoupons->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
}
