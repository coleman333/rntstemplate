import gql from 'graphql-tag'

export default gql`
    mutation scan($input: ConsumerScanIdentityRequest!) {
        scan(input: $input) {
            product {
                id
                product_organizations {
                    owner_type
                    organization_id
                    SKU
                    GTIN
                }
                product_translations {
                    language
                    short_description
                    long_description
                    image_id
                }
                omniaz_sku
                namespace
                category
                subcategory
                average_price
                average_price_currency
                alcohol_content
                year
                volume_in_milliliters
                food_pairings {
                    id
                    tag_text
                    count
                }
                tasting_note
                country
                region
                product_images {
                    id
                    image_type
                    image_id
                }
                awards {
                    id
                    text
                    image_id
                }
                user_rating
                reviews_count
                omniazed
                featured
                name
                variant
                label_notes
                aromas {
                    id
                    tag_text
                    count
                }
                wine {
                    serving_suggestion
                    grape_varietal
                    vintage
                    vineyard
                    aroma
                }
                beer {
                    IBU
                    estimated_calories
                    aromas
                }
                spirits {
                    age
                    palate
                }
            }
            scan_parameters {
                n
                i
                r
                c
            }
            text_identifier
            identity_id
            organizations {
                id
                organization_translations {
                    id
                    language
                    display_name
                    description
                    image_id
                    URL
                    facts
                    vineyard_description
                }
                role_type
                business_type
                email
                user_rating
                reviews_count
            }
            user_review {
                id
                reviewee  {
                    reviewee_id
                    reviewee_type
                }
                reviewer {
                    reviewer_type
                }
                numerical_rating
                description
                review_date
            }
        }
    }
`