import gql from 'graphql-tag'

export default gql`
  mutation findByImage($file: Upload!) {
    findByImage(file: $file) {
      product {
        id
        product_organizations {
          owner_type
          organization_id
          SKU
          GTIN
        }
        product_translations {
          language
          short_description
          long_description
          image_id
        }
        omniaz_sku
        namespace
        category
        subcategory
        average_price
        average_price_currency
        alcohol_content
        year
        volume_in_milliliters
        food_pairings {
          id
          tag_text
          count
        }
        tasting_note
        country
        region
        product_images {
          id
          image_type
          image_id
        }
        awards {
          id
          text
          image_id
        }
        user_rating
        reviews_count
        omniazed
        featured
        name
        variant
        label_notes
        aromas {
          id
          tag_text
          count
        }
        wine {
          serving_suggestion
          grape_varietal
          vintage
          vineyard
          aroma
        }
        beer {
          IBU
          estimated_calories
          aromas
        }
        spirits {
          age
          palate
        }
        organizations {
          id
          organization_translations {
            id
            language
            display_name
            description
            image_id
            URL
            facts
            vineyard_description
          }
          locations {
            id
            address
            country
            location_type
            latitude
            longitude
          }
          role_type
          business_type
          email
          images {
            id
            image_type
            image_id
          }
          outlets {
            id
            outlet_type
            outlet_translations {
              id
              language
              display_name
              URL
            }
            physical_outlet {
              location {
                id
                address
                country
                location_type
                latitude
                longitude
              }
              opening_hours
              closing_hours
            }
          }
          user_rating
          reviews_count
        }
        user_review {
          id
          reviewee  {
            reviewee_id
            reviewee_type
          }
          reviewer {
            reviewer_type
          }
          numerical_rating
          description
          review_date
        }
      }
      closest_matches {
        id
        product_organizations {
          owner_type
          organization_id
          SKU
          GTIN
        }
        product_translations {
          language
          short_description
          long_description
          image_id
        }
        omniaz_sku
        namespace
        category
        subcategory
        average_price
        average_price_currency
        alcohol_content
        year
        volume_in_milliliters
        food_pairings {
          id
          tag_text
          count
        }
        tasting_note
        country
        region
        product_images {
          id
          image_type
          image_id
        }
        awards {
          id
          text
          image_id
        }
        user_rating
        reviews_count
        omniazed
        featured
        name
        variant
        label_notes
        aromas {
          id
          tag_text
          count
        }
        wine {
          serving_suggestion
          grape_varietal
          vintage
          vineyard
          aroma
        }
        beer {
          IBU
          estimated_calories
          aromas
        }
        spirits {
          age
          palate
        }
        organizations {
          id
          organization_translations {
            id
            language
            display_name
            description
            image_id
            URL
            facts
            vineyard_description
          }
          locations {
            id
            address
            country
            location_type
            latitude
            longitude
          }
          role_type
          business_type
          email
          images {
            id
            image_type
            image_id
          }
          outlets {
            id
            outlet_type
            outlet_translations {
              id
              language
              display_name
              URL
            }
            physical_outlet {
              location {
                id
                address
                country
                location_type
                latitude
                longitude
              }
              opening_hours
              closing_hours
            }
          }
          user_rating
          reviews_count
        }
        user_review {
          id
          reviewee  {
            reviewee_id
            reviewee_type
          }
          reviewer {
            reviewer_type
          }
          numerical_rating
          description
          review_date
        }
      }
    }
  }  
`
