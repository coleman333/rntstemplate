import { client, uploadClient} from '../Apollo';
// import ADD_REVIEW from './queries/addReview.graphql';
// import UPDATE_REVIEW from './queries/updateReview.graphql';
import GET_PRODUCT from './queries/getProduct';
import UPLOAD_PHOTO from './queries/uploadPhoto';
import SCAN from './queries/scan';
import { ScanResponse } from 'src/redux/reducers/Products/list';
import { Product } from 'src/models/entities/Product';
import { SAVE_SCAN_PARAMS_REQUEST } from 'src/constants/ActionTypes/Auth';

export class ProductService {

  /*
  public static async reviewAction(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<any> {
    try {
      console.info(query);
      const action = query.id ? UPDATE_REVIEW : ADD_REVIEW;
      const typeOfData = query.id ? 'updateReview' : 'addReview';
      const result: any = await client.mutate({
        mutation: action,
        variables: {
          input: {
            ...query,
          },
        },
      });
      const data = {
        user_rating: result.data[typeOfData].reviewee_rating.rating,
        user_reviews: [
          {
            id: result.data[typeOfData].id,
            numerical_rating: result.data[typeOfData].numerical_rating,
          },
        ],
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('PagesProductDetailsService->list->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
  */

  public static async getProduct(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<Product> {
    try {
      const result: any = await client.query({
        query: GET_PRODUCT,
        variables: {
          input: query,
        },
      });

      const data: Product = result.data.getProductForConsumer;

      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('PagesProductDetailsService->getProduct->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }

  public static async getScanProduct(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<ScanResponse> {
    try {
      const result: any = await client.mutate({
        mutation: SCAN,
        variables: {
          input: {scan_parameters: query},
        },
      });
      const data: ScanResponse = result.data.scan;
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      dispatch({
        type: SAVE_SCAN_PARAMS_REQUEST + '_FULFILLED',
        payload: data.scan_parameters || {},
      });
      return data;
    } catch (error) {
      console.error('SCAN->list->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }

  public static async getProductByPhoto(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ) : Promise<Product> {
    try {
      const result: any = await uploadClient.mutate({
        mutation: UPLOAD_PHOTO,
        variables: {
          file: query,
        },
      });
      const product: Product = typeof result.data.findByImage.product === 'undefined' ?
        {} : result.data.findByImage.product;
      const matches: Array<Product> = typeof result.data.findByImage.closest_matches === 'undefined' ?
        [] : result.data.findByImage.closest_matches;
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: { product, matches },
      });
      return product;
    } catch (error) {
      console.error('GQL -> getProductByPhoto -> error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
}
