import {client} from '../Apollo';
import ADD_EVENT from './queries';

export class EventService {
  public static async addEvent(query: any): Promise<any> {
    try {
      await client.mutate({
        mutation: ADD_EVENT,
        variables: {
          input: {
            ...query,
          },
        },
      });
      return true;
    } catch (error) {
      console.error('EventService->addEvent->error', error);
      return null;
    }
  }
}
