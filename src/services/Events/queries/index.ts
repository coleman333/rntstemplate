import gql from 'graphql-tag'

export default gql`
    mutation addEvent($input: AddEventRequest!) {
        addEvent(input: $input)
    }
`