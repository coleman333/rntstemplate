import {
  NavigationBackActionPayload,
  NavigationContainerComponent,
  NavigationNavigateActionPayload,
} from 'react-navigation';

export interface NavigationService {
  init(reference: NavigationContainerComponent): void;
  navigateTo(payload: string | NavigationNavigateActionPayload): void;
  navigateBack(payload?: NavigationBackActionPayload): void;
  toggleDrawer(): void;
}
