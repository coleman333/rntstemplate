import {
  NavigationActions,
  NavigationBackActionPayload,
  NavigationContainerComponent,
  NavigationNavigateActionPayload,
} from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { NavigationService } from './types';

export const Navigation = ((): NavigationService => {
  let navigator: NavigationContainerComponent;

  const init = (reference: NavigationContainerComponent): void => {
    navigator = reference;
  };

  const navigateTo = (payload: string | NavigationNavigateActionPayload): void => {
    navigator.dispatch(
      NavigationActions.navigate(typeof payload === 'string' ? { routeName: payload } : payload),
    );
  };

  const navigateBack = (payload: NavigationBackActionPayload): void => {
    navigator.dispatch(NavigationActions.back(payload || {}));
  };

  const toggleDrawer= (): void => {
    navigator.dispatch(DrawerActions.toggleDrawer());
  };

  return { init, navigateTo, navigateBack, toggleDrawer };
})();
