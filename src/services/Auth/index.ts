import {client} from '../Apollo';
import GET_USER_PROFILE from './queries/getUserProfile';
import DELETE_ACCOUNT from './queries/deleteAccount';
//import UPDATE_USER_PROFILE from './queries/updateUserProfile.graphql';
import LOGOUT_USER from './queries/logout';
import SOCIAL_LOGIN_FACEBOOK from './queries/loginWithFacebook';
import SOCIAL_LOGIN_GOOGLE from './queries/loginWithGoogle';
import GET_SOCIAL_IDS_REQUEST from './queries/userGetSocialAppIds';
import {IGetUser, IGetSocialApps, IUpdateProfile, ILogin} from '../../interfaces/Auth';

export class AuthService {

  public static async delete(ACTION_TYPE: string, dispatch: Function): Promise<any> {
    try {
      // TODO: add interface for received data
      const result: any = await client.mutate({
        mutation: DELETE_ACCOUNT,
      });

      if (result.data.deleteAccount) {
        dispatch({
          type: ACTION_TYPE + '_FULFILLED',
        });
      } else {
        dispatch({
          type: ACTION_TYPE + '_REJECTED',
          error: result.data.error,
        });

        throw 'Not deleted.';
      }
    } catch (error) {
      console.error('Auth->deleteAcc->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      throw error;
    }
  }

  public static async getProfile(ACTION_TYPE: string, dispatch: Function): Promise<any> {
    try {
      const result: any = await client.query({
        query: GET_USER_PROFILE,
      });
      const data: IGetUser = {
        ...result.data.getProfile,
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('Auth->getData->list->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }

  /*
  public static async updateProfile(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<any> {
    try {
      const result: any = await client.mutate({
        mutation: UPDATE_USER_PROFILE,
        variables: {
          input: {
            ...query,
          },
        },
      });
      const data: IUpdateProfile = {
        ...result.data.updateProfile,
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('Auth->update->list->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
*/
  public static async logout(ACTION_TYPE: string, dispatch: Function): Promise<any> {
    try {
      // TODO: add interface for received data
      const result: any = await client.mutate({
        mutation: LOGOUT_USER,
      });
      const data = {
        logout: result.data.logout,
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        // TODO: use interface for dispatched data
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('Auth->logout->list->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }

  public static async login(ACTION_TYPE: string, dispatch: Function, query: any): Promise<any> {
    try {
      const type = query.platform === 'FACEBOOK' ? SOCIAL_LOGIN_FACEBOOK : SOCIAL_LOGIN_GOOGLE;
      const result: any = await client.mutate({
        mutation: type,
        variables: {
          input: {
            ...query,
          },
        },
      });
      const data: ILogin = {
        auth_token: result.data.login.auth_token
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('PagesSignUpService->login->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }

  public static async getSocialIds(): Promise<any> {
    try {
      const result = await client.query({
        query: GET_SOCIAL_IDS_REQUEST,
      });
      const data = result.data as IGetSocialApps;
      const socialIds = {
        socialGoogleId: data.getSocialApps.google,
        socialFacebookId: data.getSocialApps.facebook,
      };
      return {data: socialIds, error: null};
    } catch (error) {
      return {data: {}, error: error.message};
    }
  }
}
