import gql from 'graphql-tag'

export default gql`
{
    getSocialApps {
        facebook
        google
    }
}
`;
