import gql from 'graphql-tag'

export default gql`
    mutation loginWithGoogle($input: LoginRequest!){
        login(input: $input) {
            auth_token
        }
    }
`