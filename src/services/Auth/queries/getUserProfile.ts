import gql from 'graphql-tag'

export default gql`
    {
        getProfile {
            first_name
            last_name
            email
            birth_date
            country
            image_id
            tokens_amount
        }
    }
`