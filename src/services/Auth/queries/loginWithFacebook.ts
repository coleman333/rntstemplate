import gql from 'graphql-tag'

export default gql`
  mutation loginWithFacebook($input: LoginRequest!){
    login(input: $input) {
      auth_token
    }
  }
`
