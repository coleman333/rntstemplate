import { ApolloClient } from 'apollo-client';
import { setContext } from "apollo-link-context";
import { BatchHttpLink } from 'apollo-link-batch-http';
import { ApolloLink, from } from 'apollo-link';
import { createUploadLink } from 'apollo-upload-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { getStore } from 'src/redux/store';
import { config } from 'src/config';
import { LOGOUT_REQUEST } from 'src/constants/ActionTypes/Auth';

console.log('APP', config.APP);

const beforeMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      Origin: config.ORIGIN,
    },
  });

  return forward(operation);
});

const afterMiddleware = new ApolloLink((operation, forward) => {
  const store = getStore();
  return forward(operation).map(response => {
    let tokenIsExpired = false;
    response.errors &&
    response.errors.length > 0 &&
    response.errors.map((i: any) => {
      if (i.message === '16 UNAUTHENTICATED: Token is expired') {
        tokenIsExpired = true;
      }
      if (i.message === '16 UNAUTHENTICATED: unauthorized') {
        tokenIsExpired = true;
      }
      if (i.message === '14 UNAVAILABLE: Connect Failed') {
        tokenIsExpired = true;
      }
    });
    if (tokenIsExpired) {
      let state = store.getState();
      if (state.auth.authorized) {
        store.dispatch({
          type: LOGOUT_REQUEST + '_FULFILLED',
        });
      }
    }
    return response;
  });
});

const batchHttpLink = new BatchHttpLink({
  uri: config.GATEWAY_URL,
  credentials: 'include'
});

const uploadLink = createUploadLink({
  uri: config.GATEWAY_URL,
  credentials: 'include'
});


export const uploadClient = new ApolloClient({
  link: from([
    beforeMiddleware,
    afterMiddleware,
    uploadLink
  ]),
  cache: new InMemoryCache(),
});

export const client = new ApolloClient({
  link: from([
    beforeMiddleware,
    afterMiddleware,
    batchHttpLink
  ]),
  cache: new InMemoryCache(),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all',
    }
  },
});

export default client;
