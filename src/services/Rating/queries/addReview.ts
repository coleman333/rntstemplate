import gql from 'graphql-tag'

export default gql`
  mutation addReview($input: AddReviewRequest!) {
    addReview(input: $input) {
      id
      reviewee {
        reviewee_id
        reviewee_type
      }
      reviewer {
        reviewer_type
      }
      numerical_rating
      description
      review_date
      reviewee_rating {
        reviewee  {
          reviewee_id
          reviewee_type
        }
        rating
        count
      }
    }
  }
`
