import gql from 'graphql-tag'

export default gql`
  mutation updateReview($input: UpdateReviewRequest!) {
    updateReview(input: $input) {
      id
      reviewee {
        reviewee_id
        reviewee_type
      }
      reviewer {
        reviewer_type
      }
      numerical_rating
      description
      review_date
      reviewee_rating {
        reviewee  {
          reviewee_id
          reviewee_type
        }
        rating
        count
      }
    }
  }
`
