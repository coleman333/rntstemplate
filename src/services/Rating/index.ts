import {client} from '../Apollo';
import UPDATE_REVIEW from './queries/updateReview';
import ADD_REVIEW from './queries/addReview';


export class ProductModalRatingService {
  public static async reviewAction(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<any> {
    try {
      console.info(query);
      const action = query.id ? UPDATE_REVIEW : ADD_REVIEW;
      const result: any = await client.mutate({
        mutation: action,
        variables: {
          input: {
            ...query,
          },
        },
      });

      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: result.data,
      });
      return result.data;
    } catch (error) {
      console.error('PagesProductDetailsService->list->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
}
