import gql from 'graphql-tag'

export default gql`
  query balanceTokenTransactions($balanceInput: BalanceTokenTransactionsRequest!, $currencyInput: Currency!) {
    balanceTokenTransactions(input: $balanceInput) {
      amount
    }
    getConversionRate(currency: $currencyInput) {
      conversion_rate
    }
  }
`
