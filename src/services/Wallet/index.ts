import {client} from '../Apollo';
import GET_BALANCE from './queries/getPriceAndBalance';
import { IWallet } from '../../interfaces/Wallet';


export class WalletService {

  public static async getBalance(ACTION_TYPE:string, dispatch:Function, query: any):Promise<any> {
    try {
      const result:any = await client.query({
        query: GET_BALANCE,
        variables: {
          ...query,
        },
      });

      const OMZ_amount = parseFloat(result.data.balanceTokenTransactions.amount) / 1000000;
      const OMZ_rate = parseFloat(result.data.getConversionRate.conversion_rate);
      const data:IWallet = {
        balanceOMZ: OMZ_amount,
        balanceCurrency: OMZ_amount * OMZ_rate,
        conversationRate: OMZ_rate,
      };
      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
}
