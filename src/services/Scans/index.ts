import { client } from '../Apollo';
import GET_SCAN_HISTORY_DATA from './queries';
import { productsDateSort } from 'src/utils/productsDateSort';

export class RewardsService {
  public static async getScanHistory(
    ACTION_TYPE: string,
    dispatch: Function,
    query: any,
  ): Promise<any> {
    try {
      const result: any = await client.query({
        query: GET_SCAN_HISTORY_DATA,
        variables: {
            input: {
                ...query,
            }
        },
      });
      const data = {
        history: productsDateSort(result?.data?.getScanHistory?.items),
      };

      dispatch({
        type: ACTION_TYPE + '_FULFILLED',
        payload: data,
      });
      return data;
    } catch (error) {
      console.error('PagesProfile->getScanHistory->error', error);
      dispatch({
        type: ACTION_TYPE + '_REJECTED',
        error,
      });
      return null;
    }
  }
}
