import gql from 'graphql-tag'

export default gql`
    query getScanHistory($input: ScanHistoryRequest!) {
        getScanHistory(input: $input) {
            items {
                event {
                    target_name
                    action
                    request_extra_data {
                        datetime
                    }
                }
                product {
                    category
                    # year
                    name
                    user_rating
                    product_images {
                        image_id
                    }
                    average_price
                    average_price_currency
                }
            }
            next_page_token
            prev_page_token
        }
    }
`