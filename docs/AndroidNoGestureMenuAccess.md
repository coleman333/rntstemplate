* Go to `System Settings` -> `Keyboard` -> `Shortcuts` -> `Custom Shortcuts`
* Click the `+` button
* Specify the desired name (for example `React Native - Application reload`)
* Copy the next code to the command line:

To reload the application
```terminal
gnome-terminal -e "bash -c 'adb shell input keyevent KEYCODE_MENU && adb shell input keyevent 66 && adb shell input keyevent 66 '"
```
To open application developer menu
```terminal
gnome-terminal -e "bash -c 'adb shell input keyevent KEYCODE_MENU'"
```
* Choose a shortcut keybind by clicking on the `disabled` text
