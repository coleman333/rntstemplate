import { AppRegistry } from 'react-native';
import { App } from './src/App';
import { name } from './app.json';

if (__DEV__) {
  console.disableYellowBox = true;
}

AppRegistry.registerComponent(name, () => App);
